<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Insurance_model extends CI_Model {

    var $table_name = 'INS_PROCESS';
    var $primary_key = 'insID';
    var $fields_insert = array(
        'insID', 'branchNo', 'active', 'typeCustomer', 'financeName', 'brandCar', 'carName', 'colorCar', 'regNo', 'regProvinve', 'frameNo', 'engineNo', 'accessories1', 'accessories2', 'typeInsurance', 'insNo', 'insName', 'typeProtec', 'typeRepair', 'customerNo', 'customerName', 'details', 'towncity', 'district', 'provinve', 'postalCode', 'tel', 'barcode', 'dateAct', 'dateCancelIns', 'dateCancelInsAct', 'paymentType', 'directPayment', 'dateMail', 'dateInsNoti', 'dateGetPolicy', 'dateProtec1', 'dateProtec2', 'dateInsMatured', 'dateSendAccount', 'dateSendSale', 'dateWarning', 'typeNew', 'discountGood', 'discountFOC', 'insCapital', 'insPremium', 'insAct', 'insSum', 'insDeductible', 'specialCustomerRecord', 'remarkIns', 'remarkInsSelect', 'nameAdmin', 'staffIns', 'CREATE_USER', 'LAST_USER', 'countFollow', 'statusImport','detailsNew','towncityNew','districtNew','provinveNew','postalCodeNew','statusAddressNew','dateProtec1Remove','dateProtec2Remove','StatusStart'
    );
    var $fields_update = array(
        'LAST_USER',
        'remarkIns',
        'remarkInsSelect',
        'insID',
        'countFollow'
    );
    var $fields_updateAll = array(
        'insID', 'branchNo', 'active', 'typeCustomer', 'financeName', 'brandCar', 'carName', 'colorCar', 'regNo', 'regProvinve', 'frameNo', 'engineNo', 'accessories1', 'accessories2', 'typeInsurance', 'insNo', 'insName', 'typeProtec', 'typeRepair', 'customerNo', 'customerName', 'details', 'towncity', 'district', 'provinve', 'postalCode', 'tel', 'barcode', 'dateAct', 'dateCancelIns', 'dateCancelInsAct', 'paymentType', 'directPayment', 'dateMail', 'dateInsNoti', 'dateGetPolicy', 'dateProtec1', 'dateProtec2', 'dateInsMatured', 'dateSendAccount', 'dateSendSale', 'dateWarning', 'typeNew', 'discountGood', 'discountFOC', 'insCapital', 'insPremium', 'insAct', 'insSum', 'insDeductible', 'specialCustomerRecord', 'nameAdmin', 'staffIns', 'LAST_USER', 'statusImport','detailsNew','towncityNew','districtNew','provinveNew','postalCodeNew','dateProtec1Remove','dateProtec2Remove','statusAddressNew','StatusStart'
    );
    var $db_date;
    var $db_user;

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        $this->db_date = date("Y-m-d H:i:s");
        $this->db_user = $_SESSION['vgroup']['info']['username'];
    }

    function count_by_filter($filter = array()) {
        $rs = array();
        $sql = "SELECT COUNT(*) AS COUNT ";
        $sql .= "FROM {$this->table_name} ";
        $sql .= "WHERE RECORD_STATUS = 'N'  ";
        $sql .= ($filter['IDNumem'] == "") ? "" : " AND (IDNumem ='{$filter['IDNumem']}')";
        $sql .= ";";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }

    function select_by_filterNewCar($filter = array()) {
        $rs = array();
        $sql = "SELECT [ID_NUM]
        ,[Login_NM]
        ,[STATUS_INS]
        ,[D_1]
        ,[FROM_1]
        ,[COMPANY_NM]
        ,[RECEIPT_FROM]
        ,[NOTIFIED_NO]
        ,[PRICE_DRIFF]
        ,[NOTIFIED_USER]
        ,[RC_DRIFF]
        ,[AGENT_NO]
        ,[INS_TYPE]
        ,[BOOK_NO]
        ,[NO]
        ,[CUSTOMER_NM]
        ,[CARD_NO]
        ,[DETAILS]
        ,REPLACE(REPLACE([TOWNCITY], 'ตำบล', ''), 'แขวง', '') AS 'TOWNCITY'
        ,REPLACE(REPLACE([DISTRICT], 'อำเภอ', ''), 'เขต', '') AS 'DISTRICT'
        ,REPLACE([PROVINVE], 'จังหวัด', '') AS 'PROVINVE'
        ,[ZIP_CD]
        ,[TEL_NO]
        ,[COMPANY_TEL_NO]
        ,[MTO_NM]
        ,[MODEL_YEAR]
        ,[FRAME_NO]
        ,[ENGINE_NO]
        ,[COLOR_NM]
        ,[TOTAL_CAR]
        ,[DISPLACEMENT]
        ,[LECENCE]
        ,[PROVINCE2]
        ,[ACCESSORIES]
        ,[ACC1]
        ,[INS_NM]
        ,[PROTECTYPE]
        ,[INS_NO]
        ,[FNC_NM]
        ,[TYPE_NM]
        ,[BARCODE_NM]
        ,[TYPE]
        ,[DRIVER1_NM]
        ,[BD1]
        ,[CARDID1]
        ,[DRVLC_NM1]
        ,[DRIVER2_NM]
        ,[BD2]
        ,[CARDID2]
        ,[DRVLC_NM2]
        ,[INSCOST]
        ,[INSCHIP]
        ,[START_DATE1]
        ,[START_DATE2]
        ,[INSNO_OLE]
        ,[INSNM_OLE]
        ,[Discount]
        ,[SALE_NM]
        ,[Remark]
        ,[BR_CD]
        ,[RC_FROM2]
        FROM Ids_Ins_Nw_Inform
        WHERE STATUS_INS ='5เสร็จสิ้น'";
        // $sql .= " WHERE STATUS_INS ='5เสร็จสิ้น'";
        // $sql .= ($filter['User_NM'] == "") ? "" : " AND (User_NM ='{$filter['User_NM']}')";
        $sql .= ($filter['START_DATE2'] == "") ? "" : " AND (START_DATE2 like '%{$filter['START_DATE2']}')";
        $sql .= ($filter['FRAME_NO'] == "") ? "" : " AND (FRAME_NO like '%{$filter['FRAME_NO']}%')";
        $sql .= ($filter['BR_CD'] == "") ? "" : " AND (BR_CD ='{$filter['BR_CD']}')";
        $sql .= ($filter['MTO_NM'] == "") ? "" : " AND (MTO_NM like '%{$filter['MTO_NM']}%')";
        $sql .= ($filter['LECENCE'] == "") ? "" : " AND (LECENCE like '%{$filter['LECENCE']}%')";
        $sql .= ($filter['CUSTOMER_NM'] == "") ? "" : " AND (CUSTOMER_NM like '%{$filter['CUSTOMER_NM']}%')";
        $sql .= ($filter['INS_NM'] == "") ? "" : " AND (INS_NM like '%{$filter['INS_NM']}%')";
        $sql .= ($filter['INS_NO'] == "") ? "" : " AND (INS_NO like '%{$filter['INS_NO']}%')";
        $sql .= ";";
        // echo $sql ; 
        // $query = $this->db->query($sql);
        $query = $this->load->database('vgroupdb', TRUE)->query($sql);
        foreach ($query->result_array() as $value) {
            $rs[] = $value;
        }
        return $rs;
    }

    function select_by_filter($filter = array()) {
        $rs = array();
        $sql = "SELECT * ";
        $sql .= "FROM {$this->table_name} ";
        $sql .= "WHERE RECORD_STATUS = 'N'  ";
        $sql .= ($filter['User_NM'] == "") ? "" : " AND (User_NM ='{$filter['User_NM']}')";
        $sql .= ";";
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $rs[] = $value;
        }

        return $rs;
    }


    function select_by_id($id = NULL) {
        $result = array();
        if (!empty($id)) {
            $sql = "SELECT *  ";
            $sql .= "FROM {$this->table_name} ";
            $sql .= "WHERE RECORD_STATUS = 'N'  ";
            $sql .= "AND (User_NM = '{$id}') ";
            $sql .= ";";
        }
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }

    function select_by_login($filter = array()) {
        $result = array();
        if (!empty($filter['user'])) {
            $sql = "SELECT *  ";
            $sql .= "FROM MAS_EMPLOYEE ";
            $sql .= "WHERE RECORD_STATUS = 'N'  ";
            $sql .= "AND (User_NM = '{$filter['user']}') ";
            $sql .= "AND (Password = '{$filter['pass']}') ";
            $sql .= ";";
        }
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }

    function insert($arrayObj = array()) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        $sql_fields = '';
        $sql_value = '';
        if (is_array($arrayObj)) {
            foreach ($this->fields_insert as $key) {
                $arrayObj[$key] = str_replace("'", "''", trim($arrayObj[$key]));
                if (!empty($arrayObj[$key]) && !is_array($arrayObj[$key])) {
                    $sql_fields .= "{$key}, ";
                    $sql_value .= "'" . trim($arrayObj[$key]) . "', ";
                }
            }
            $sql_fields .= "RECORD_STATUS, LAST_DATE ";
            $sql_value .= "'N', '{$this->db_date}' ";

            $sql = "INSERT INTO {$this->table_name} ({$sql_fields}) VALUES ({$sql_value}) ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function update($arrayObj = array()) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        $arrayObj[$this->primary_key] = trim($arrayObj[$this->primary_key]);
        if (is_array($arrayObj) && !empty($arrayObj[$this->primary_key])) {
            $sql = "UPDATE {$this->table_name} SET ";
            foreach ($this->fields_update as $key) {
                $arrayObj[$key] = trim($arrayObj[$key]);
                $data = "NULL";
                if (!empty($arrayObj[$key]) && !is_array($arrayObj[$key])) {
                    $data = "'" . $arrayObj[$key] . "'";
                }
                $sql .= "{$key}={$data}, ";
            }
            $sql .= "LAST_DATE='{$this->db_date}'  ";
            $sql .= "WHERE RECORD_STATUS = 'N' ";
            $sql .= "AND ({$this->primary_key} = '{$arrayObj[$this->primary_key]}') ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id = NULL) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        if (!empty($id)) {
            $sql = "UPDATE {$this->table_name} SET ";
            $sql .= "RECORD_STATUS = 'D', ";
            $sql .= "LAST_DATE = '{$this->db_date}' ";
            $sql .= "WHERE (RECORD_STATUS = 'N') ";
            $sql .= "AND ({$this->primary_key} IN ('{$id})') ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }


}