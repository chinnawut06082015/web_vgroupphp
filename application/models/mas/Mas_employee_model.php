<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mas_employee_model extends CI_Model {

    var $table_name = 'MAS_EMPLOYEE';
    var $primary_key = 'IDNumem';
    var $fields_insert = array(
      'IDNumem',
      'IDNumpro',
      'User_NM'
    );
    var $fields_update = array(
        'IDNumem',
        'IDNumpro',
       'User_NM'
    );
    var $db_date;
    var $db_user;

    function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Bangkok');
        $this->db_date = date("Y-m-d H:i:s");
        $this->db_user = $_SESSION['vgroup']['info']['username'];
    }

    function count_by_filter($filter = array()) {
        $rs = array();
        $sql = "SELECT COUNT(*) AS COUNT ";
        $sql .= "FROM {$this->table_name} ";
        $sql .= "WHERE RECORD_STATUS = 'N'  ";
        $sql .= ($filter['IDNumem'] == "") ? "" : " AND (IDNumem ='{$filter['IDNumem']}')";
        $sql .= ";";
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }


    function select_by_filter($filter = array()) {
        $rs = array();
        $sql = "SELECT * ";
        $sql .= "FROM {$this->table_name} ";
        $sql .= "WHERE RECORD_STATUS = 'N'  ";
        $sql .= ($filter['User_NM'] == "") ? "" : " AND (User_NM ='{$filter['User_NM']}')";
        $sql .= ";";
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $rs[] = $value;
        }

        return $rs;
    }


    function select_by_id($id = NULL) {
        $result = array();
        if (!empty($id)) {
            $sql = "SELECT *  ";
            $sql .= "FROM {$this->table_name} ";
            $sql .= "WHERE RECORD_STATUS = 'N'  ";
            $sql .= "AND (User_NM = '{$id}') ";
            $sql .= ";";
        }
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }

    function select_by_login($filter = array()) {
        $result = array();
        if (!empty($filter['user'])) {
            $sql = "SELECT *  ";
            $sql .= "FROM {$this->table_name} ";
            $sql .= "WHERE RECORD_STATUS = 'N'  ";
            $sql .= "AND (User_NM = '{$filter['user']}') ";
            $sql .= "AND (Password = '{$filter['pass']}') ";
            $sql .= ";";
        }
        // echo $sql;
        $query = $this->db->query($sql);
        foreach ($query->result_array() as $value) {
            $result = $value;
        }

        return $result;
    }

    function insert($arrayObj = array()) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        $sql_fields = '';
        $sql_value = '';
        if (is_array($arrayObj)) {
            foreach ($this->fields_insert as $key) {
                $arrayObj[$key] = str_replace("'", "''", trim($arrayObj[$key]));
                if (!empty($arrayObj[$key]) && !is_array($arrayObj[$key])) {
                    $sql_fields .= "{$key}, ";
                    $sql_value .= "'" . trim($arrayObj[$key]) . "', ";
                }
            }
            $sql_fields .= "RECORD_STATUS, LAST_DATE ";
            $sql_value .= "'N', '{$this->db_date}' ";

            $sql = "INSERT INTO {$this->table_name} ({$sql_fields}) VALUES ({$sql_value}) ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function update($arrayObj = array()) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        $arrayObj[$this->primary_key] = trim($arrayObj[$this->primary_key]);
        if (is_array($arrayObj) && !empty($arrayObj[$this->primary_key])) {
            $sql = "UPDATE {$this->table_name} SET ";
            foreach ($this->fields_update as $key) {
                $arrayObj[$key] = trim($arrayObj[$key]);
                $data = "NULL";
                if (!empty($arrayObj[$key]) && !is_array($arrayObj[$key])) {
                    $data = "'" . $arrayObj[$key] . "'";
                }
                $sql .= "{$key}={$data}, ";
            }
            $sql .= "LAST_DATE='{$this->db_date}'  ";
            $sql .= "WHERE RECORD_STATUS = 'N' ";
            $sql .= "AND ({$this->primary_key} = '{$arrayObj[$this->primary_key]}') ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    function delete($id = NULL) {
        date_default_timezone_set('Asia/Bangkok');
        $result = 0;
        if (!empty($id)) {
            $sql = "UPDATE {$this->table_name} SET ";
            $sql .= "RECORD_STATUS = 'D', ";
            $sql .= "LAST_DATE = '{$this->db_date}' ";
            $sql .= "WHERE (RECORD_STATUS = 'N') ";
            $sql .= "AND ({$this->primary_key} IN ('{$id})') ";
            $sql .= ";";
            // echo $sql;
            $result = $this->db->query($sql);
        }

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }


}
