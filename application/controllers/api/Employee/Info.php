<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 *
 * @package         CodeIgniter
 * @author          thepnatee
 */
class Info extends REST_Controller {


    function __construct() {
        parent::__construct();
        $this->methods['profile_get']['limit'] = 100000;
    }

## Function get data จาก header

    function getallheaders() {
        $headers = '';

        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }

    function profile_get() {
        $obj = $this->getallheaders();
        $value = str_replace(chr(9), '', file_get_contents('php://input'));
        $obj_body = json_decode($value);
        $data = (array) $obj_body;
        if ($obj['Application-Key'] === $this->token) {
            $filter['IDNumem'] = $this->get('IDNumem');

            if($filter){
                $this->load->model('mas/Mas_employee_model');
                $rs = $this->Mas_employee_model->select_by_filter($filter);
            if ($rs) {
                $this->response($rs,
                        REST_Controller::HTTP_OK);
            } else {
                $this->response($rs,400);
            }
        } else {

            //กรณีไม่ส่งค่า userName

            $this->response([
                'status' => 'FALSE',
                'message' => 'Request Query String a GET operation (HTTP method)!,"?userLineuserId={Username}"'
                    ], 400);
        }
            
        } else {
            //กรณีไม่ส่ง Token มา

            $this->response([
                'status' => 'FALSE',
                'message' => 'Application-Key header required. Must follow the scheme, "Application-Key: <ACCESS TOKEN>"'
                    ], 401);
        } 
    }

}
