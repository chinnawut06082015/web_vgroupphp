<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");
        $this->load->library('Check');
        $this->check->checkpermisionn();
    }

    public function index() {
        $_SESSION['page'] = 'Process';
        // $data['form'] = hash('sha512', md5(rand(100, 99999)));
        // $data['user'] = $this->get_data();

        // print_r($_SESSION['search']);
        //         echo '<pre>';
        //         print_r($_SESSION);
        //         echo '</pre>';

        $this->load->view('Sales/Insurance/Process');
        // $this->load->view('Sales/Insurance/Process', $data);
    }

    public function get_data() {

        $filter = $_SESSION['search'];

        $this->load->model('Sales/Insurance/Insurance_model');
        $rs = $this->Insurance_model->select_by_filterNewCar($filter);
        // $this->load->model('Sales/Insurance/Insurance_model');
        // $rs = $this->Insurance_model->select_by_filter($filter);
        return $rs;
    }

    public function search_data() {
        if ($_POST) {
            // echo '<pre>';
                // print_r($dt);
                // echo '</pre>';
            $_SESSION['search'] = $_POST;
            $rs['status'] = TRUE;
            echo json_encode($rs);
        } else {
            $rs['status'] = FALSE;
            echo json_encode($rs);
        }
    }
    
    function get_data_edit($id = NULL) {
        $this->load->model('Sales/Insurance/Insurance_model');
        $rs = $this->Insurance_model->select_by_id($id);
        echo json_encode($rs);
    }

    function set_data() {

        $dt = $_POST;

        switch ($dt['action']) {
            case 'add':

                $this->load->model('Sales/Insurance/Insurance_model');
                $rs = $this->Insurance_model->insert($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'เพิ่มข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
            case 'edit':

                // echo '<pre>';
                // print_r($dt);
                // echo '</pre>';

                $this->load->model('Sales/Insurance/Insurance_model');
                $rs = $this->Insurance_model->update($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ปรับปรุงข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
            
            case 'edit':


                $this->load->model('Sales/Insurance/Insurance_model');
                $rs = $this->Insurance_model->update($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ปรับปรุงข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
            case 'delete':

                $this->load->model('Sales/Insurance/Insurance_model');
                $rs = $this->Insurance_model->delete($dt['IDNumem']);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ลบเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
        }
    }

}