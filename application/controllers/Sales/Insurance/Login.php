<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");
    }

    public function index() {

        $data['form'] = hash('sha384', md5(rand(100, 99999)));

        $_SESSION['vgroup']['base'] = base64_encode($data['form']);

        /*
         * Clear Session
         *          */

        unset($_SESSION['vgroup']["LOGIN"]);
        unset($_SESSION['vgroup']["info"]);
        $this->load->view('Sales/Insurance/Login', $data);
    }

    function logoff() {
        $_SESSION['vgroup']['LOGIN'] = 'Logoff';
        session_start();
        session_destroy();
        header("location: ./Login");
        exit(0);
    }

    function get_data() {

        // echo '<pre>';
        // print_r($_SESSION);
        // echo '</pre>';
        // echo '<pre>';
        // print_r($_POST);
        // echo '</pre>';

        #ดัก SESSION ชั้นที่ 1 

                    #filter username สำหรับค้นหา Database Sys_user_model
                    $obj = $this->get_data_user_from_db($_POST);
                    // echo '<pre>';
                    // print_r($obj);
                    // echo '</pre>';  
                    #Step 1 กรณีพบใน DB 
                    if (!empty($obj)) {

                            #สถานะของผู้ใช้งานต้องเป็นปัจจุบันเท่านั้น

                                $_SESSION['vgroup']['LOGIN'] = 'Login';

                                #User Information จาก Database Sys_user_model
                                $_SESSION['vgroup']['info'] = $obj;


                                $result['status'] = TRUE;
                                $result['message_header'] = 'Success';
                                $result['message_error'] = 'Login Success';
                                echo json_encode($result);

                    } else {

                        $result['status'] = FALSE;
                        $result['message_header'] = 'Error';
                        $result['message_error'] = 'Permission is not found!';
                        echo json_encode($result);
                    }
    }

    function get_data_user_from_db($filter = array()) {
        $this->load->model('Sales/Insurance/Insurance_model');
        $rs = $this->Insurance_model->select_by_login($filter);
        return $rs;
    }

}