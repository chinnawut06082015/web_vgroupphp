<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");

        #ตรวจสอบ SESSION สำหรับ Login 
        $this->load->library('Check');
        $this->check->checkpermisionn();
    }

    #function สำหรับเริ่มต้น
    public function index() {
        
        #ชื่อหน้าของ Page
        $_SESSION['page'] = 'User';
        
        #เข้ารหัสเพื่อป้องการ hack form
        $data['form'] = hash('sha512', md5(rand(100, 99999)));
        
        # get data เริ่มต้นของหน้านั้นๆ
        $data['user'] = $this->get_data();

        #ตรวจสอบค่าที่ส่งไป สำหรับ debug ใน network
        // echo '<pre>';
        // print_r($data);
        // echo '</pre>';

        #นำ array['data'] ส่งไปหน้า view
        $this->load->view('User', $data);
    }

    # GET Data สำหรับเริ่มหน้า
    public function get_data() {

        #$_SESSION[''search] ค่าเริ่มต้นเป็นค่าว่าง

        $filter = $_SESSION['search'];

        #ชื่อไฟล์ - > function ของไฟล์ 

        $this->load->model('mas/Mas_employee_model');
        $rs = $this->Mas_employee_model->select_by_filter($filter);
        
        #ตรวจสอบค่าที่ส่งไป สำหรับ debug ใน network
        // echo '<pre>';
        // print_r($rs);
        // echo '</pre>';

        # ใช้ retur สำหรับทำ oop คือให้ controller ด้วยกัน
        return $rs;


    }

    # สำหรับค้นหา 
    public function search_data() {
        if ($_POST) {

            echo '<pre>';
            print_r($_POST);
            echo '</pre>';

            # ยัดข้อมูลที่ POST เข้า SESSION search
            $_SESSION['search'] = $_POST;
            
            $rs['status'] = TRUE;

            #ใช้ echo สำหรับ หน้า view ajax response ข้อมูล
            echo json_encode($rs);
        } else {

            $rs['status'] = FALSE;
            echo json_encode($rs);


        }
    }
    
    #function สำหรับ get by id เพื่อ  แก้ไข
    function get_data_edit($id = NULL) {
        $this->load->model('mas/Mas_employee_model');
        $rs = $this->Mas_employee_model->select_by_id($id);
        echo json_encode($rs);
    }

    # function สำหรับ เพิ่ม แก้ไข ลบข้อมูล
    function set_data() {

        $dt = $_POST;

        $this->load->model('mas/Mas_employee_model');

        switch ($dt['action']) {

            case 'add':
                
                $rs = $this->Mas_employee_model->insert($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'เพิ่มข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;

            case 'edit':

                $rs = $this->Mas_employee_model->update($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ปรับปรุงข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
            
            case 'edit':

                $rs = $this->Mas_employee_model->update($dt);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ปรับปรุงข้อมูลเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
            case 'delete':

                $rs = $this->Mas_employee_model->delete($dt['IDNumem']);
                if ($rs === true) {
                    $result['status'] = TRUE;
                    $result['message'] = 'ลบเรียบร้อย';
                } else {
                    $result['status'] = FALSE;
                    $result['message'] = 'ข้อมูลไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
                }
                echo json_encode($result);

                break;
        }
    }

}
