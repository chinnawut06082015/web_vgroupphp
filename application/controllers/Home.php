<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");
        $this->load->library('Check');
        $this->check->checkpermisionn();
    }

    public function index() {
        $_SESSION['page'] = 'Home';


        $this->load->view('Home', $data);
    }

    public function get_data() {

    }
}
