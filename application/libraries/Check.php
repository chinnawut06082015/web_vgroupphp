<?PHP

/*

 * Sys_token_model->select_permission
 * Sys_user_model->select_by_login_block
 * 
 *  */

class Check {

    private $CI;

    function __construct() {
        $this->CI = & get_instance();
    }

    function checkpermisionn() {
        //ตรวจสอบสถานะของผู่ใช้งาน
        if ($_SESSION['vgroup']['LOGIN'] !== 'Login' || empty($_SESSION['vgroup']['info'])) {

            $_SESSION['vgroup']['LOGIN'] = 'Logoff';
            session_start();
            session_destroy();
            header("location: ./");
            exit(0);
        }
    }

    function SQLCheck($str = NULL) {
        $str = str_replace("'", "''", $str);
        $str = str_replace("--", "", $str);
        return $str;
    }

}
