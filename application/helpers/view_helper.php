<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of view_helper
 *
 * @author Administrator
 */
if (!function_exists('view_id_card')) {

//put your code here
     function view_id_card($cid) {
          if (strlen($cid) == 13) {
               $cid = substr($cid, 0, 1) . "-" . substr($cid, 1, 4) . "-" . substr($cid, 5, 5) . "-" . substr($cid, 10, 2) . "-" . substr($cid, 12, 1);
          }
          return $cid;
     }

}

/**
 * Description of view_helper
 *
 * @author Administrator
 */
if (!function_exists('view_money_text')) {

//put your code here
     function view_money_text($num) {

          //$num = intval($num);
          // สร้าง Array สำหรับเปลี่ยนเลขเป็นตัวอักษร เรียงจากน้อยไปมาก เพราะเราจะทำการไล่จากตัวท้ายไปหาตัวหน้า
          $number = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");

          // และสำหรับหลักเลข 1 ชุด เรียงจากน้อยไปมาก เพราะเราจะทำการไล่จากตัวท้ายไปหาตัวหน้าเหมือนกัน
          $number2 = array("", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน");


          $str = ""; // สร้าง String เก็บชุดอักษรที่คำนวณได้
          $lennum = strlen($num); // นับจำนวนอักษรที่ผู้ใช้ใส่เข้ามา
          $tmp = 0; // ค่าเริ่มต้นของค่าหลักเลข
          $count = 0; // ค่าเริ่มต้นตัวนับที่จะไล่ไปตามตำแหน่งต่างๆ
          // วนค่าจากตำแหน่งท้ายของตัวเลขขึ้นมา เนื่องจากเรารู้ว่าตัวท้ายสุดจะเป็นหลักหน่วยเสมอ
          for ($i = $lennum - 1; $i > -1; --$i) {
               // เพิ่มค่าตัวนับในตำแหน่งที่วนลูป
               $count++;
               // เมื่อตำแหน่งตัวเลขมากกว่าหลักล้าน ให้ย้อนกลับเป็นหลักสิบ (ทำไมไม่เป็นหลักหน่วย? เนื่องจากเลยจากหลักล้านไปแล้ว ตัวต่อไปจะเหมือนกับหลักสิบ เช่น 10,000,000 เราจะเห็นว่าเลข 1 คือหลักสิบ)
               if ($tmp == 7) {
                    $tmp = 1;
               }
               // ดึงตัวเลขของตำแหน่งที่วนมาถึง
               $ch = substr($num, $i, "1");
               // เปลี่ยนตัวเลขเป็นตัวหนังสือ ของตัวนั้นๆ
               $digit = $number[$ch];
               // เก็บค่าของหลักลงตัวแปร(เริ่มจาก 1)
               $pos = $tmp + 1;
               // หากเป็นหลักสิบ และตัวเลขที่เจอเป็น 1 ไม่ให้แสดงตัวอักษร คำว่า หนึ่ง เนื่องจากเราจะไม่อ่านว่า หนึ่งสิบ
               if ($pos == 2 && $ch == "1") {
                    $digit = "";
                    // หากเป็นหลักสิบ และตัวเลขที่เจอเป็น 2 ให้แสดงตัวอักษร คำว่า ยี่ เนื่องจากเราจะไม่อ่านว่า สองสิบ
               } else if ($pos == 2 && $ch == "2") {
                    $digit = "ยี่";
                    // หากเป็นหลักหน่วย หรือหลักล้าน และตัวเลขที่พบคือ 1 และยังมีหลักที่มากกว่าหลักหน่วยปัจจุบัน ให้แสดงเป็น เอ็ด แทน หนึ่ง
               } else if (( $pos == 1 || $pos == 7 ) && $ch == "1" && $lennum > $count) {
                    $digit = "เอ็ด";
               }

               // สร้างหลักจากตำแหน่งของหลักที่พบ
               $last = $number2[$tmp];
               // ถ้าตัวเลขที่พบเป็น 0 และไม่ใช่หลักล้าน ไม่ให้แสดงอักษรของหลัก
               if ($ch == "0" && $pos != 7)
                    $last = "";
               // เอาค่าที่หาได้มาต่อกับ str โดยต่อจากข้างหน้าไปเรื่อยๆ
               $str = $digit . "" . $last . "" . $str;
               // เพิ่มค่าหลักเลขที่เริ่มจากหน่วย(0) ถึง ล้าน(6)
               $tmp++;
          }//loop for
          // แสดงผลลัพธ์

          return (empty($num)?'ศูนย์':$str);
     }

}

if (!function_exists('view_money_text_bank')) {

     function view_money_text_bank($num) {
          $num = floatval($num);
          if (strstr($num, '.')) {
               $num.='00';
          }else{
               $num.='.00';
          }
          $temp = explode('.', $num);
          $int = $temp[0]; 
          $dec = intval(substr($temp[1], 0, 2));
          $text = '';
          if ($dec == 0) {
               $text = view_money_text($int) . 'บาทถ้วน';
          } else {
               $text = view_money_text($int) . 'บาท' . view_money_text($dec) . 'สตางค์';
          }
          return $text;
     }

}


/**
 * Description of view_helper
 *
 * @author Administrator
 */
if (!function_exists('rs2html')) {

//put your code here
     function rs2html(&$rs, $ztabhtml=false, $zheaderarray=false, $htmlspecialchars=true, $echo = true) {
          $s = '';
          $rows = 0;
          $docnt = false;
          GLOBAL $gSQLMaxRows, $gSQLBlockRows, $ADODB_ROUND;

          if (!$rs) {
               printf(ADODB_BAD_RS, 'rs2html');
               return false;
          }

          if (!$ztabhtml)
               $ztabhtml = "BORDER='1' WIDTH='98%'";
          //else $docnt = true;
          $typearr = array();
          $ncols = $rs->FieldCount();
          $hdr = "<TABLE COLS=$ncols $ztabhtml><tr>\n\n";
          for ($i = 0; $i < $ncols; $i++) {
               $field = $rs->FetchField($i);
               if ($field) {
                    if ($zheaderarray)
                         $fname = $zheaderarray[$i];
                    else
                         $fname = htmlspecialchars($field->name);
                    $typearr[$i] = $rs->MetaType($field->type, $field->max_length);
                    //print " $field->name $field->type $typearr[$i] ";
               } else {
                    $fname = 'Field ' . ($i + 1);
                    $typearr[$i] = 'C';
               }
               if (strlen($fname) == 0)
                    $fname = '&nbsp;';
               $hdr .= "<TH>$fname</TH>";
          }
          $hdr .= "\n</tr>";
          if ($echo)
               print $hdr . "\n\n";
          else
               $html = $hdr;

          // smart algorithm - handles ADODB_FETCH_MODE's correctly by probing...
          $numoffset = isset($rs->fields[0]) || isset($rs->fields[1]) || isset($rs->fields[2]);
          while (!$rs->EOF) {

               $s .= "<TR valign=top>\n";

               for ($i = 0; $i < $ncols; $i++) {
                    if ($i === 0)
                         $v = ($numoffset) ? $rs->fields[0] : reset($rs->fields);
                    else
                         $v = ($numoffset) ? $rs->fields[$i] : next($rs->fields);

                    $type = $typearr[$i];
                    switch ($type) {
                         case 'D':
                              if (strpos($v, ':') !== false)
                                   ;
                              else {
                                   if (empty($v)) {
                                        $s .= "<TD> &nbsp; </TD>\n";
                                   } else {
                                        $s .= "	<TD>" . $rs->UserDate($v, "D d, M Y") . "</TD>\n";
                                   }
                                   break;
                              }
                         case 'T':
                              if (empty($v))
                                   $s .= "<TD> &nbsp; </TD>\n";
                              else
                                   $s .= "	<TD>" . $rs->UserTimeStamp($v, "D d, M Y, H:i:s") . "</TD>\n";
                              break;

                         case 'N':
                              if (abs(abs($v) - round($v, 0)) < 0.00000001)
                                   $v = round($v);
                              else
                                   $v = round($v, $ADODB_ROUND);
                         case 'I':
                              $vv = stripslashes((trim($v)));
                              if (strlen($vv) == 0)
                                   $vv .= '&nbsp;';
                              $s .= "	<TD align=right>" . $vv . "</TD>\n";

                              break;
                         /*
                           case 'B':
                           if (substr($v,8,2)=="BM" ) $v = substr($v,8);
                           $mtime = substr(str_replace(' ','_',microtime()),2);
                           $tmpname = "tmp/".uniqid($mtime).getmypid();
                           $fd = @fopen($tmpname,'a');
                           @ftruncate($fd,0);
                           @fwrite($fd,$v);
                           @fclose($fd);
                           if (!function_exists ("mime_content_type")) {
                           function mime_content_type ($file) {
                           return exec("file -bi ".escapeshellarg($file));
                           }
                           }
                           $t = mime_content_type($tmpname);
                           $s .= (substr($t,0,5)=="image") ? " <td><img src='$tmpname' alt='$t'></td>\\n" : " <td><a
                           href='$tmpname'>$t</a></td>\\n";
                           break;
                          */

                         default:
                              if ($htmlspecialchars)
                                   $v = htmlspecialchars(trim($v));
                              $v = trim($v);
                              if (strlen($v) == 0)
                                   $v = '&nbsp;';
                              $s .= "	<TD>" . str_replace("\n", '<br>', stripslashes($v)) . "</TD>\n";
                    }
               } // for
               $s .= "</TR>\n\n";

               $rows += 1;
               if ($rows >= $gSQLMaxRows) {
                    $rows = "<p>Truncated at $gSQLMaxRows</p>";
                    break;
               } // switch

               $rs->MoveNext();

               // additional EOF check to prevent a widow header
               if (!$rs->EOF && $rows % $gSQLBlockRows == 0) {

                    //if (connection_aborted()) break;// not needed as PHP aborts script, unlike ASP
                    if ($echo)
                         print $s . "</TABLE>\n\n";
                    else
                         $html .= $s . "</TABLE>\n\n";
                    $s = $hdr;
               }
          } // while

          if ($echo)
               print $s . "</TABLE>\n\n";
          else
               $html .= $s . "</TABLE>\n\n";

          if ($docnt)
               if ($echo)
                    print "<H2>" . $rows . " Rows</H2>";

          return ($echo) ? $rows : $html;
     }

}

if (!function_exists('array_dropdown')) {

//function สร้างDropDown จาก array่[value]=text...
     function array_dropdown($arr, $selected='') {
          $result = "";
          foreach ($arr as $value => $txt) {
               if ($value != 'LABEL')
                    $result .= "<option value='" . $value . "'" . ($selected == $value ? " selected" : "") . ">" . $txt . "</option>";
          }
          return $result;
     }

}

if (!function_exists('view_thai_number')) {

//function แปลงเลขอารบิคเป็นเลขไทย
     function view_thai_number($num) {
          $num = str_replace("0", "๐", $num);
          $num = str_replace("1", "๑", $num);
          $num = str_replace("2", "๒", $num);
          $num = str_replace("3", "๓", $num);
          $num = str_replace("4", "๔", $num);
          $num = str_replace("5", "๕", $num);
          $num = str_replace("6", "๖", $num);
          $num = str_replace("7", "๗", $num);
          $num = str_replace("8", "๘", $num);
          $num = str_replace("9", "๙", $num);
          return $num;
     }

}

if (!function_exists('gen_control_code')) {

//function สร้างชุดรหัสสำหรับการตรวจสอบ
     function gen_control_code($data) {
          return abs(crc32(sha1($data)));
     }

}

if (!function_exists('gen_check_digi')) {

//function สร้างรหัสสำหรับการตรวจสอบ
     function gen_check_digi($data) {
          $data = strval($data);
          $len = strlen($data);
          $result = 0;
          for ($i = 0; $i < $len; $i++) {
               $result+= $data{$i};
          }
          if (strlen($result) > 1)
               $result = substr($result, strlen($result) - 1, 1);
          return $result;
     }

}

if (!function_exists('gen_check_digi_barcode')) {

//function สร้างรหัสสำหรับการตรวจสอบ
     function gen_check_digi_barcode($data) {
          $data = strval($data);
          $len = strlen($data);
          $result = 0;
          $index = 0;
          $totalOdd = $totalEven = $tempOdd = 0;
          for ($i = 0; $i < $len; $i++) {
               $index = $i + 1;
               if (($index % 2) == 0)
                    $totalEven+=$data{$i};
               else {
                    $tempOdd = strval($data{$i} * 2);
                    if (strlen($tempOdd) > 1) {
                         $totalOdd+= $tempOdd{0} + $tempOdd{1};
                    } else {
                         $totalOdd+= $tempOdd{0};
                    }
               }
          }
          $result = 10 - (($totalOdd + $totalEven) % 10);

          return ($result % 10);
     }

}

if (!function_exists('msg_box')) {

     /**
      * function สร้าง box สำหรับแจ้ง message ในรูปแบบของ jquery ui theme
      * 
      * @param string $state css ui-state-<p>(active, default, disabled, error, error-text, focus, highlight, hover, processing, success)</p>
      * @param string $icon <p>ui-icon-</p>
      * @param string $alert <p>text highlight</p>
      * @param string $text <p>text message</p>
      * @return HTML
      */
     function msg_box($state, $icon, $alert, $text) {
          $response = '<div class="ui-widget">';
          $response.= '<div class="ui-state-' . $state . ' ui-corner-all" style="padding: 0 .7em;">';
          $response.= '<p><span class="ui-icon ui-icon-' . $icon . '" style="float: left; margin-right: .3em;"></span>';
          $response.= '<strong>' . $alert . ':</strong> ' . $text . '</p>';
          $response.= '</div>';
          $response.= '</div>';
          return $response;
     }

}