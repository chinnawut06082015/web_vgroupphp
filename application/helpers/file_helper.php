<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of file_helper
 *
 * resize image
 * @author Administrator
 */
if (!function_exists('resize_image')) {

//put your code here
    function resize_image($newWidth, $originalFile) {

        if (empty($newWidth) || empty($originalFile)) {
            return FALSE;
        } else {
            $temp_info = get_mime_type($originalFile);
            if ($temp_info[0] == 'image') {
                list($width, $height) = getimagesize($originalFile);
                $newHeight = ($height / $width) * $newWidth;
                $tmp = imagecreatetruecolor($newWidth, $newHeight);

                //if ($width > $newWidth) {
                switch ($temp_info[1]) {
                    case 'jpeg' :
                        $src = imagecreatefromjpeg($originalFile);
                        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                        imagejpeg($tmp, $originalFile, 95);
                        imagedestroy($tmp);
                        break;

                    case 'png' :
                        imagealphablending($tmp, false);
                        imagesavealpha($tmp, true);
                        $src = imagecreatefrompng($originalFile);
                        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                        imagepng($tmp, $originalFile, 9);
                        imagedestroy($tmp);
                        break;
                    case 'gif' : break;
                    default : break;
                }
                //}
            }
        }
    }

}

if (!function_exists('get_mime_type')) {

//put your code here
    function get_mime_type($file_name) {
        // Version 4.x supported
        if (function_exists('mime_content_type')) {//
            //$f_info = mime_content_type($file_name);
            return explode('/', get_manual_mime_type($file_name));
        } else if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $f_info = finfo_file($finfo, $file_name);
            finfo_close($finfo);
            return explode('/', $f_info);
        } else {
            return explode('/', get_manual_mime_type($file_name));
        }
    }

}

if (!function_exists('get_manual_mime_type')) {

    function get_manual_mime_type($file_name) {
        $ext = strtolower(end(explode('.', $file_name)));
        $mime_types = array(
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
            // ms office
            'doc' => 'application/msword',
            'docx' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        $return = $mime_types[$ext];
        return (!empty($return) ? $return : 'application/unknow');
    }

}

if (!function_exists('url_exists')) {

//put your code here
    function url_exists($url) {
        // Version 4.x supported
        $handle = curl_init($url);
        if (false === $handle) {
            return false;
        }
        curl_setopt($handle, CURLOPT_HEADER, false);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
        curl_setopt($handle, CURLOPT_NOBODY, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
        $connectable = curl_exec($handle);
        curl_close($handle);
        return $connectable;
    }

    if (!function_exists('file_type')) {

        function file_type($file_name) {
            $obj_map = end(explode('.', $file_name));
            $file_type = '';
            if ($obj_map == 'jpg' or $obj_map == 'jpeg') {
                $file_type = base_url() . "/resources/images/file_type/jpg.png";
            } else if ($obj_map == 'png') {
                $file_type = base_url() . "/resources/images/file_type/png.png";
            } else if ($obj_map == 'csv') {
                $file_type = base_url() . "/resources/images/file_type/csv.png";
            } else if ($obj_map == 'doc' or $obj_map == 'docx') {
                $file_type = base_url() . "/resources/images/file_type/doc.png";
            } else if ($obj_map == 'pdf') {
                $file_type = base_url() . "/resources/images/file_type/pdf.png";
            } else if ($obj_map == 'ppt' or $obj_map == 'pptx') {
                $file_type = base_url() . "/resources/images/file_type/ppt.png";
            } else if ($obj_map == 'xls' or $obj_map == 'xlsx') {
                $file_type = base_url() . "/resources/images/file_type/xls.png";
            }else if ($obj_map == 'mp4' or $obj_map == 'mp4') {
                $file_type = base_url() . "/resources/images/file_type/mp4.png";
            }  else {
                $file_type = base_url() . "/resources/images/file_type/file.png";
            }
            return $file_type;
        }

    }
}