<?php

if (!function_exists('get_sql_filter')) {

    /**
     * To Prepare an SQL query by field config and filter array
     * 
     * @param array $prepare 
     * <p><b>$key</b> => array(<b>$comparision</b>, <b>$field</b>, <b>$logical</b>, <b>$value</b>)</p>
     * <p><b>key</b> = array key in $filter</p>
     * <p><b>comparision</b> = sql operator comparision [WHERE, AND, OR]</p>
     * <p><b>logical</b> = sql operator logical [=, <>, >, <, =>, =<, BETWEEN, LIKE, IN]</p>
     * <p><b>value</b> = value fixed for filter by model</p>
     * @param array $filter <p><b>$key</b> => <b>$field</b></p>
     * <p><b>key</b> = key in filter</p>
     * <p><b>value</b> = value filter</p>
     * @return array sql => text, filter => array()
     */
    function get_sql_filter($prepare = array(), $filter = array()) {
        $response = array('sql' => '', 'filter' => array());
        if (is_array($prepare) && count($prepare)) {
            foreach ($prepare as $key => $operators) {
                list($comparision, $field, $logical, $value) = $operators;
                $filter[$key] = ($filter[$key] == "" && $operators['3'] != '') ? $operators['3'] : $filter[$key];
                if ($filter[$key] != "") {
                    $field = empty($field) ? $key : $field;
                    switch ($logical) {
                        case 'BETWEEN':
                            //case 1 field 2 value
                            if (is_array($filter[$key]) && count($filter[$key]) == 2) {
                                $response['sql'].= "{$comparision} ({$field} {$logical} CONVERT(datetime,?,120) AND CONVERT(datetime,?,120)) ";
                                array_push($response['filter'], $filter[$key][0]);
                                array_push($response['filter'], $filter[$key][1]);
                            }

                            //case 1 value 2 field
                            if (is_array($field) && count($field) == 2) {
                                $response['sql'].= "{$comparision} (? {$logical} {$field['0']} AND {$field['1']}) ";
                                array_push($response['filter'], $filter[$key]);
                            }
                            break;

                        case 'BETWEEN_DATE':
                            //case 1 field 1 value 00:00:00 - 23:59:59
                            if (!is_array($filter[$key]) && !is_array($field)) {
                                $response['sql'].= "{$comparision} ({$field} BETWEEN CONVERT(datetime,?+' 00:00:00',120) AND CONVERT(datetime,?+' 23:59:59',120)) ";
                                array_push($response['filter'], $filter[$key]);
                                array_push($response['filter'], $filter[$key]);
                            }

                            //case 1 field 2 value
                            if (is_array($filter[$key]) && count($filter[$key]) == 2) {
                                $response['sql'].= "{$comparision} ({$field} BETWEEN CONVERT(datetime,?,120) AND CONVERT(datetime,?,120)) ";
                                array_push($response['filter'], $filter[$key][0]);
                                array_push($response['filter'], $filter[$key][1]);
                            }

                            //case 1 value 2 field
                            if (is_array($field) && count($field) == 2) {
                                $response['sql'].= "{$comparision} (CONVERT(datetime,?,120) {$logical} {$field['0']} AND {$field['1']}) ";
                                array_push($response['filter'], $filter[$key]);
                            }


                            break;
                        case 'IN':
                        case 'NOT_IN':
                            $array = (is_array($filter[$key])) ? $filter[$key] : @array_filter(explode(',', $filter[$key]));
                            foreach ($array as $value) {
                                $param[] = '?';
                                array_push($response['filter'], $value);
                            }
                            $params = implode(',', $param);
                            $response['sql'].= str_replace('_', ' ', $comparision) . " ({$field} {$logical} ({$params})) ";
                            break;

                        case 'LIKE':
                            $response['sql'].= "{$comparision} ({$field} {$logical} ?) ";
                            $filter[$key] = "%{$filter[$key]}%";
                            array_push($response['filter'], $filter[$key]);
                            break;

                        default:
                            $response['sql'].= "{$comparision} ({$field} {$logical} ?) ";
                            array_push($response['filter'], $filter[$key]);
                            break;
                    }
                }
            }
        }
        return $response;
    }

}


if (!function_exists('return_sql_date')) {

    function return_sql_date($date_time1, $date_time2 = '', $local_en = FALSE) {
        $response = FALSE;
        if (!empty($date_time1) && is_string(strstr($date_time1, '/'))) {
            $temp = explode('/', $date_time1);
            if (is_array($temp) && count($temp) == 3) {
                $yy = ($local_en == FALSE) ? $temp[2] - 543 : $temp[2];
                $mm = str_pad($temp[1], 2, '0', STR_PAD_LEFT);
                $dd = str_pad($temp[0], 2, '0', STR_PAD_LEFT);
                $response = $yy . '-' . $mm . '-' . $dd;
                if (!empty($date_time2) && is_string(strstr($date_time2, '/'))) {
                    $temp = explode('/', $date_time2);
                    if (is_array($temp) && count($temp) == 3) {
                        $yy = ($local_en == FALSE) ? $temp[2] - 543 : $temp[2];
                        $mm = str_pad($temp[1], 2, '0', STR_PAD_LEFT);
                        $dd = str_pad($temp[0], 2, '0', STR_PAD_LEFT);
                        $date2 = $yy . '-' . $mm . '-' . $dd;
                        $response = array($response, $date2);
                    }
                }
            }
        }
        return $response;
    }

    function GetBetween($var1 = "", $var2 = "", $pool) {
        $temp1 = strpos($pool, $var1) + strlen($var1);
        $result = substr($pool, $temp1, strlen($pool));
        $dd = strpos($result, $var2);
        if ($dd == 0) {
            $dd = strlen($result);
        }

        return substr($result, 0, $dd);
    }

}