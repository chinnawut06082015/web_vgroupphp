<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Session Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Language CI
 * @author    Suranart Sriyeesun  (oomzabb@hotmail.com)
 * @version   0.1
 * Copyright (c) 2010 Suranart Sriyeesun  (http://www.facebook.com/TanOhmZ)
 */
if (!function_exists('ses_data')) {

    /**
     * ถอดรหัสและดึงข้อมูลจาก PHP session 
     * @param string $name <p>
     * ชื่อของ session ที่ต้องการ
     * </p>
     * @return string
     * ข้อมูล session ที่ต้องการ
     *
     * <b>Input</b><br>
     * ses_data('username')
     *
     * <b>Output</b><br>
     * Admin
     */
    function ses_data($name) {
        $CI = & get_instance();
        if (!empty($_SESSION[$CI->config->item('app_code')])) {
            $result = $_SESSION[$CI->config->item('app_code')][$name];
        } else {
            $result = '';
        }
        return $result;
    }

}

if (!function_exists('ses_add')) {

    /**
     * เข้ารหัสและเพิ่มข้อมูลเข้าสู่ session
     * @param array $data <p>
     * ข้อมูลของ session ในรูปแบบ array เช่น <br>
     *     $newdata = array(<br>
     * 'username' => 'Administrator',<br>
     * 'name' => 'System developer',<br>
     * );
     * </p>
     * @return string
     * ข้อมูล session ที่ต้องการ
     *
     */
    function ses_add($data = array()) {
        $CI = & get_instance();
        $temp = $_SESSION[$CI->config->item('app_code')];
        if (is_array($temp))
            $data = $data + $temp;
        $_SESSION[$CI->config->item('app_code')] = $data;
    }

}

if (!function_exists('ses_path')) {

    /**
     * ค้นหาพาทหลังสุดของ session ในรูปแบบของ url path
     * @param string $data <p>
     * ข้อมูลของ session ในรูปแบบ path เช่น <br>
     *    master/mas_campus_model
     * </p>
     * @return string
     * ข้อมูล ของชื่อไฟล์ ที่ต้องการ
     *
     */
    function ses_path($data) {

        $temp = explode('/', $data);

        $result = $temp[count($temp) - 1];
        return $result;
    }

}

if (!function_exists('get_ip')) {

    function get_ip() {
        return (getenv(HTTP_X_FORWARDED_FOR)) ? getenv(HTTP_X_FORWARDED_FOR) : getenv(REMOTE_ADDR);
    }

}

if (!function_exists('getbrowser')) {

    function getbrowser() {
        return $_SERVER['HTTP_USER_AGENT'];
    }

}

if (!function_exists('get_browser_info')) {

    function get_browser_info($agent = NULL) {
        // Declare known browsers to look for
        $browsers = array("firefox", "msie", "opera", "chrome", "safari",
            "mozilla", "seamonkey", "konqueror", "netscape",
            "gecko", "navigator", "mosaic", "lynx", "amaya",
            "omniweb", "avant", "camino", "flock", "aol");

        $agent = strtolower($agent ? $agent : $_SERVER['HTTP_USER_AGENT']);

        // Find all phrases (or return empty array if none found)
        foreach ($browsers as $browser) {
            if (preg_match("#($browser)[/ ]?([0-9.]*)#", $agent, $match)) {
                $info['name'] = $match[1];
                $info['version'] = $match[2];
                break;
            }
        }
        return "{$info['name']} ({$info['version']})";
    }

}
