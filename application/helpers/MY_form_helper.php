<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Form Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Language CI
 * @author    Suranart Sriyeesun  (oomzabb@hotmail.com)
 * @version   0.1
 * Copyright (c) 2010 Suranart Sriyeesun  (http://www.facebook.com/TanOhmZ)
 */
if (!function_exists('set_form')) {

    /**
     * สร้าง Form สำหรับจัดการข้อมูลจากพารามิเตอร์ของฟอร์มที่ต้องการ
     * @param string $field <p>
     * id ของฟิลด์
     * </p>
     * @param array $params  <p>
     * พารามิเตอร์ของฟิลด์นั้น
     * </p>
     * @param string $item [optional] <p>
     * id ของฟิลด์หลังจากแยก Multilanguage แล้ว
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_form($field, $params = array(), $item = NULL, $id = NULL, $value = NULL, $grid_name = NULL) {

        $CI = & get_instance();

        if (!$item) {
            $item = $field;
        }
        $label = lang($field);
        if (is_array($label)) {
            $label = $label['LABEL'];
        } else {
            $label = trim($label);
            $temp_label = $label;
            if (check_lang($field)) {
                $sel_lang = check_select_lang($item);
                if ($sel_lang != label('SUFFIX')) {
                    $label .= " (" . ucfirst($sel_lang) . ")";
                } else {
                    $label .= " (" . ucfirst(lang('SUFFIX')) . ")";
                }
            }
            if (empty($temp_label)) {
                $label = substr($field, 0, strlen($field) - 2) . 'ML';
                $label_2 = label($label);
                if ($label != $label_2) {
                    $label = $label_2 . ' (' . ucfirst(check_select_lang($field)) . ')';
                } else {
                    $label = $item;
                }
            }
        }
        $label = ($label == '') ? $field : $label; //echo $label.'<hr/>';

        $default = '';
        $validator = '';
        $v = array();
        $chk_onlyFloat = FALSE;
        $onSelect = '';
        $scriptOnSelect = '';
        $scriptOnRemove = '';
        $loadOnChange = TRUE;
        $loadAjax = FALSE;

        $beforeElement = '';
        $afterElement = '';

        foreach ($params as $param => $paramval) {
            switch ($param) {
                case 'required' :
                case 'optional' :
                case 'custom' :
                case 'length' :
                case 'ajax' :
                case 'minCheckbox' :
                case 'confirm' :
                    if ($param == 'required' || $param == 'optional') {
                        if ($paramval == 'true') {
                            $v[] = $param;
                            $required = 'true';
                        }
                    } else {
                        if ($param != 'ajax') {
                            $v[] = "{$param}[{$paramval}]";
                        }
                        if ($paramval == 'onlyFloat') {
                            $chk_onlyFloat = TRUE;
                        }
                    }
                    break;
                case 'colspan' :
                    if ($paramval == 'true') {
                        $colspan = 'true';
                    }
                    break;
                default :
                    $$param = $paramval;
                    if ($param != 'html' && $param != 'search' && $param != 'default' && $paramval != 'dropdown' && $paramval != 'dbdropdown' && $param != 'label') {
                        $atb.= "{$param}='{$paramval}' ";
                    }
                    break;
            }
        }

        $form_data = $beforeElement;

        if (!empty($v)) {
            $bt = 'form-control';
            if ($type == 'radio') {
                $bt = 'control-primary';
            } else if ($type == 'file') {
                $bt = 'file-styled-primary';
            }

            if (!empty($html) || $chk_onlyFloat === TRUE || $type == 'datepicker' || $type == 'datetime' || $type == 'timepicker') {
                $bt.= ' form-inline';
                if ($chk_onlyFloat === TRUE) {
                    $bt.= ' onlyFloat';
                }
            }
            $validator = "class='validate[";
            $validator .= implode(',', $v);
            $validator .= "] {$bt}'";
        }
        if ($visible != 'false') {
            $value = ($value != '') ? $value : $default;
            $select_by = ($select_by != '') ? $select_by : $item;
            $select_by = ($db_table != '') ? $db_table : $select_by;

            $show_label = ($type == 'hidden' || $type == 'checkbox') ? '' : $label; //echo $show_label.'<hr/>';

            if (empty($valign)) {
                switch ($type) {
                    case 'editor' :
                    case 'textarea' :
                    case 'autocomplete' :
                    case 'autocomplete_filter' :
                    case 'checkbox-multi' :
                    case 'controller' :
                        $valign = 'top';
                        break;
                    default :
                        $valign = 'center';
                        break;
                }
                $valign = (!empty($html) ? 'top' : $valign);
            }

            $long_column = FALSE;
            if ($colspan != 'true') {
                switch ($type) {
                    case 'controller' :
                    case 'editor' :
                    case 'textarea' :
                    case 'radio' :
                        $long_column = TRUE;
                        break;
                    default :
                        break;
                }
            }

            if ($long_column === TRUE) {
                $label_width = 2;
                $form_data.= " 
<div class='col-sm-12'>
    <div class='form-group'>
        <label class='col-md-125 control-label'>{$show_label}" . ($required == 'true' ? "<div class='ui-icon-req'></div>" : '&nbsp;') . "</label>";
                $form_data.= " 
        <div class='col-md-875'>";
            } else if ($type != 'hidden') {
                //$form_data.="<th align='right' valign='{$valign}' style='width:150px'>{$show_label}&nbsp;";
                $label_width = 3;
                $form_data.= " 
<div class='col-sm-6'>
    <div class='form-group'>
        <label class='col-md-{$label_width} control-label'>{$show_label}" . ($required == 'true' ? "<div class='ui-icon-req'></div>" : '&nbsp;') . "</label>";
//                if ($required == 'true') {
//                    $form_data.="<div class='ui-icon-req'></div>";
//                }
                //$form_data.='</th>';
                //$form_data.='<td  valign="top"' . ($colspan != 'true' ? ' colspan="3"' : '') . '>';
                $form_data.= " 
        <div class='col-md-" . (12 - $label_width) . "'>";
            }
            $replace_id = array('[', ']');
            switch ($type) {

                case 'textarea' :
                    if (empty($cols)) {
                        $cols = 50;
                    }
                    if (empty($rows)) {
                        $rows = 6;
                    }
                    if (!empty($atb)) {
                        $temp = array_filter(explode(' ', $atb));
                        if (is_array($temp) && !empty($temp)) {
                            $a = array();
                            foreach ($temp as $row) {
                                $temp_2 = explode('=', $row);
                                switch ($temp_2[0]) {
                                    case 'visible' :
                                    case 'type' :
                                    case 'search' :
                                    case 'value' :
                                    case 'required' :
                                    case 'default' :
                                    case 'cols' :
                                    case 'rows' : break;
                                    default : $a[] = $row;
                                        break;
                                }
                            }
                            $atb = implode(' ', $a);
                        }
                    }
                    $form_data.="<textarea cols='{$cols}' rows='{$rows}' {$atb} {$validator} name='{$item}' id='" . str_replace($replace_id, '', $item) . "' class='form-control'>{$value}</textarea>";
                    break;


                //case 'file' :
                case 'uploadify_file' :
                    $fileType = '';
                    if (!empty($fileTypeExts)) {
                        $fileType = "<font color='#ff0000'> *Please Choose File formats: <b>{$fileTypeExts}</b></font><br/>";
                    }
                    $fileSize = '<br/>';
                    if (!empty($fileSizeLimit)) {
                        $fileSize = "<font color='#ff0000'>Max file size: <b>{$fileSizeLimit}</b></font><br/>";
                    }

                    $fileSizeLimit = (!empty($fileSizeLimit) ? "'fileSizeLimit' : '{$fileSizeLimit}'," : '');
                    $fileTypeExts = (!empty($fileTypeExts) ? "'fileTypeExts' : '{$fileTypeExts}'," : '');
                    $fileTypeDesc = (!empty($fileTypeDesc) ? "'fileTypeDesc' : '{$fileTypeDesc}'," : '');
                    $multi = (!empty($multi) ? $multi : 'false');
                    $form_data.="
                        <input name='{$item}' id='" . str_replace($replace_id, '', $item) . "{$grid_name}' type='file'/> 
                         <div id='queue" . str_replace($replace_id, '', $item) . "{$grid_name}'></div>    
                         <script type='text/javascript'>
                              $(document).ready(function() {
                                   $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').uploadify({
                                        {$fileSizeLimit}
                                        {$fileTypeExts}  
                                        {$fileTypeDesc}    
                                        'auto'              : true,
                                        'queueID'           : 'queue" . str_replace($replace_id, '', $item) . "{$grid_name}',
                                        'removeCompleted'   : false,
                                        'multi'             : {$multi},
                                        'buttonText'        : '" . label(SELECT_FILES) . "',    
                                        'onComplete'        : function(event, queueID, fileObj, response, data) {
                                             //if(response!=1)alert(response);
                                        },
                                        'onError'           : function(event, queueID, fileObj, errorObj) {
                                             alert('Whoops! There was an error uploading your file.=='+errorObj);
                                        }
                                   });
                              });
                         </script>
                         ";
                    if (!empty($path)) {
                        $form_data.=" 
                        <input name='{$item}_PATH' id='" . str_replace($replace_id, '', $item) . "_PATH{$grid_name}' type='hidden' value='{$path}'/>
                         ";
                    }
                    $form_data.= $fileType . $fileSize;
                    if (!empty($value)) {
                        $file = $path . $value;
                        if (is_file($file)) {
                            $form_data.= "<font color='#ff0000' size='2em'><b>***When you upload a new file into system the old one will be replaced.***</b></font><br/>";
                            $form_data.= "<a target='_blank' href='{$file}' class='btn btn-link' role='button'>";
                            $form_data.= "<image src='" . file_type($file) . "'/>{$value}";
                            $form_data.= "</a><br/>";
                        }
                    }
                    $form_data.='<br/>';
                    break;

                case 'uploadifive_file' :
                    $timestamp = time();
                    $folder = (!empty($folder) ? ",folder : '{$folder}'" : '');
                    $size = (!empty($size) ? ",size : '{$size}'" : ",size : ''");
                    $fileTypes = (!empty($fileTypes) ? ",fileTypes : '{$fileTypes}'" : ",fileTypes : ''");
                    $filename2 = (!empty($filename2) ? ",filename2 : '{$filename2}'" : ",filename2 : ''");
                    $form_data.="
                        <input name='{$item}' id='" . str_replace($replace_id, '', $item) . "{$grid_name}' type='file' multiple='true' />";
//                            <button id='btn_upload_{$item}{$grid_name}'>".label(BTN_UPLOAD)."</button>
                    $form_data.="       <div id='queue" . str_replace($replace_id, '', $item) . "{$grid_name}'></div>
                        <script type='text/javascript'>
                            $(document).ready(function() {
                                $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').uploadifive({
                                    'auto'             : true,
                                    'checkScript'      : base_url+'resources/js/plugin/uploadifive/check-exists.php',
                                    'formData'         : {
                                                            'timestamp' : '{$timestamp}',
                                                            'token'     : '" . md5('unique_salt' . $timestamp) . "'
                                                            {$folder} {$size} {$fileTypes} {$filename2}
                                                         },
                                    'queueID'          : 'queue" . str_replace($replace_id, '', $item) . "{$grid_name}',
                                    'uploadScript'     : base_url+'resources/js/plugin/uploadifive/uploadifive.php',
                                    'onUploadComplete' : function(file, data) { console.log(data); }
                                });
                                $('#btn_upload_" . str_replace($replace_id, '', $item) . "{$grid_name}').button({
                                    icons: {primary: 'ui-icon-disk'}
                                }).click(function(){
                                    $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').uploadifive('upload');
                                    return false;
                                });
                            });
                        </script>
                        ";
                    break;

                case 'editor' :
                    if (empty($height)) {
                        $height = 300;
                    }
                    if (empty($width)) {
                        $width = 800;
                    }
                    if (!empty($atb)) {
                        $temp = array_filter(explode(' ', $atb));
                        if (is_array($temp) && !empty($temp)) {
                            $a = array();
                            foreach ($temp as $row) {
                                $temp_2 = explode('=', $row);
                                switch ($temp_2[0]) {
                                    case 'upload' :
                                    case 'toolbar' :
                                    case 'filter' :
                                    case 'visible' :
                                    case 'type' :
                                    case 'search' :
                                    case 'value' :
                                    case 'required' :
                                    case 'default' :
                                    case 'height' :
                                    case 'width' : break;
                                    default : $a[] = $row;
                                        break;
                                }
                            }
                            $atb = implode(' ', $a);
                        }
                    }
                    $form_data.="<textarea {$atb} name='{$item}' id='" . str_replace($replace_id, '', $item) . "{$grid_name}' style='width:{$width}px; height:{$height}px'>{$value}</textarea>";

                    $form_data.= "
                         <script type='text/javascript'>
                              $(function() {
                                    if ($.type($('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').tinymce()) === 'object') {
                                        $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').tinymce().remove(); 
                                    }
                                   $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').tinymce({
                                             plugins: [
                                                'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                                                'searchreplace wordcount visualblocks visualchars code fullscreen',
                                                'insertdatetime media nonbreaking save table contextmenu directionality',
                                                'emoticons template paste textcolor colorpicker textpattern imagetools'
                                              ],
                                              image_advtab: true,
                                             //statusbar : false,
                                             menubar: false,
                                             //toolbar_items_size: 'small',
                                             ";

                    if ($upload === TRUE) {
                        $form_data.= "
                                                file_browser_callback: function(field, url, type, win) {
                                                      tinyMCE.activeEditor.windowManager.open({
                                                          file: base_url+'resources/js/plugin/KCFinder3.20/browse.php?opener=tinymce4&field=' + field + '&type=' + type,
                                                          title: 'KCFinder',
                                                          width: 700,
                                                          height: 500,
                                                          inline: true,
                                                          close_previous: false
                                                      }, {
                                                          window: win,
                                                          input: field
                                                      });
                                                      return false;
                                                  },
                             ";
                    }
                    if ($toolbar == 'basic') {//$editor_type
                        $form_data.= "
                                             // Theme options
                                             //theme_advanced_buttons1 : 'bold,italic,underline,strikethrough,|,emotions,|,forecolor,backcolor,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,fullscreen',                                             
                                             //v4.0.2
                                             toolbar1: 'bold italic underline strikethrough | forecolor backcolor | fontsizeselect | alignleft aligncenter alignright alignjustify | link image ',
                                   ";
                    } else {
                        $form_data.= "
                                              toolbar1: 'undo redo | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                                              toolbar2: 'forecolor backcolor fontsizeselect | table',"; //fullscreen code | 
                    }
                    $form_data.= "
                                   });
                              });
                         </script>
                                   ";
                    break;

                case 'editor_fck' :
                    if (empty($toolbar)) {
                        $toolbar = 'User';
                    }
                    $form_data.="<textarea {$atb} name='{$item}' id='" . str_replace($replace_id, '', $item) . "{$grid_name}' style='width:800px; height:450px'>{$value}</textarea>";
                    $form_data.= "
                         <script type='text/javascript'>
                              $(function() {
                                   $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').fck({ 
                                       path : base_url+'resources/js/plugin/fckeditor/',
                                       toolbar: '{$toolbar}',
                                   });
                              });
                         </script>
                                   ";
                    break;

                case 'timepicker_old' :

                    $t_value = explode('.', calHour($value));

                    $form_data.="<select {$atb} {$validator} name='F_{$item}' id='F_" . str_replace($replace_id, '', $item) . "'>";
                    for ($i = 7; $i <= 21; $i++) {
                        $optionval = ($i >= 10) ? $i : '0' . $i;

                        $form_data.="<option value='{$i}' " . ($t_value[0] == $i ? 'selected' : '') . ">{$optionval}</option>";
                    }
                    $form_data.="</select>";

                    $form_data.=":";

                    $form_data.="<select {$atb} {$validator} name='L_{$item}' id='L_" . str_replace($replace_id, '', $item) . "'>";
                    for ($i = 0; $i <= 55; $i+=5) {
                        $optionval = ($i >= 10) ? $i : '0' . $i;
                        $form_data.="<option value='{$optionval}' " . ($t_value[1] == $optionval ? 'selected' : '') . ">{$optionval}</option>";
                    }
                    $form_data.="</select>";
                    break;

                case 'timepicker' :
                case 'timepicker_minute' :

                    if ($type === 'timepicker_minute') {
                        $value_time = empty($value) ? '' : calHour($value);
                        if (empty($timeFormat)) {
                            $timeFormat = 'H.i';
                            $showDefault = '00.00';
                        }

                        $step = empty($step) ? '60' : $step;
                        //$timeFormat = empty($timeFormat) ? 'H.i' : $timeFormat;
                    } else {
                        $value_time = view_time($value);
                    }
                    $timeFormat = empty($timeFormat) ? 'H:i' : $timeFormat;
                    $showDefault = empty($showDefault) ? '00:00' : $showDefault;
                    $step = empty($step) ? '1' : $step;

                    $minTime = empty($minTime) ? '00.00' : $minTime;
                    $maxTime = empty($maxTime) ? '23.59' : $maxTime;

                    if (empty($validator)) {
                        $validator = "class='form-inline form-control'";
                    }

                    $form_data.="<input name='{$item}' {$validator} id='" . str_replace($replace_id, '', $item) . "{$grid_name}' size='5' maxlength='5' value='{$value_time}' style='text-align:center;'/> ({$showDefault})";
                    $form_data.= "
                         <script>
                              $(function() {
                                   $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').timepicker({
                                        //'scrollDefaultNow': true, 
                                        'step': {$step}, 
                                        'timeFormat': '{$timeFormat}',                                       
                                        'minTime': '{$minTime}',
                                        'maxTime': '{$maxTime}',    
                                        'disableTextInput': true,
                                        'forceRoundTime':true, 
                                        'selectOnBlur': true
                                    });
                              });
                         </script>
                                   ";
                    break;

                case 'datepicker' :
                case 'datetime' :

                    if (empty($validator)) {
                        $validator = 'class="form-inline form-control"';
                    }
                    $value_date = view_date($value);

                    $form_data.="<input name='{$item}' {$validator} id='" . str_replace($replace_id, '', $item) . "{$grid_name}' value='{$value_date}' size='10' maxlength='10' style='text-align:center;'/> (dd/mm/yyyy)";
                    $form_data.= "
                         <script>
                              $(function() {
                                   $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}').datepicker({dateFormat: 'dd/mm/yy'});
                              });
                         </script>
                                   ";
                    break;

                case 'toggle' :

                    $select_by = 'TRUE_FALSE';

                case 'radio' :
                    if (isset($db_table)) {
                        $temp = get_data_model($db_table, NULL, 'array', $filter_db);
                        $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                    } else if (isset($select_by)) {
                        $temp = lang($select_by);
                        if (isset($temp['LABEL'])) {
                            unset($temp['LABEL']);
                        }
                    }

                    $ran = md5(time() . rand());
                    $i = 0;

                    if (is_array($temp)) {
                        $validator = (!empty($validator) ? $validator : "class='control-primary'");
                        foreach ($temp as $option => $optionval) {
                            if (is_array($optionval)) {
                                $optionval = $optionval[$language];
                            }
                            if ($value == '' && $i == 0) {
                                $value = $option;
                            }
                            $form_data.="<label class='radio-inline' for='re{$i}{$ran}'><input {$validator} type='radio' name='{$item}' id='re{$i}{$ran}' value='{$option}' " . ($option == $value ? 'checked="checked" ' : '') . ($disabled == 'disabled' ? 'disabled="disabled" ' : '') . " />{$optionval}</label>";
                            $i++;
                        }
                    }

                    break;

                case 'checkbox' :

                    $form_data.="<label for='{$item}'><input  type='checkbox' {$atb} {$validator} name='{$item}' id='" . str_replace($replace_id, '', $item) . "' value='T'  " . ($value == 'T' ? 'checked' : '') . " />{$label}</label>";
                    break;

                case 'dropdown' :
                case 'dbdropdown' :
                    if (!empty($atb)) {
                        $temp = array_filter(explode(' ', $atb));
                        if (is_array($temp) && !empty($temp)) {
                            $a = array();
                            foreach ($temp as $row) {
                                $temp_2 = explode('=', $row);
                                switch ($temp_2[0]) {
                                    case 'db_table_filter' :
                                    case 'db_table' :
                                    case 'maxitems' :
                                    case 'filter' :
                                    case 'search' :
                                    case 'type' :
                                    case 'visible' :
                                    case 'type' :
                                    case 'search' :
                                    case 'maxlength' :
                                    case 'required' :
                                    case 'default' :
                                    case 'search_default' :
                                    case 'select_by' :
                                    case 'parent_filter' :
                                    case 'sql_select' :
                                    case 'filter_db' : break;
                                    default : $a[] = $row;
                                        break;
                                }
                            }
                            $atb = implode(' ', $a);
                        }
                    }

                    if (empty($db_table)) {
                        $form_data.="<select {$atb} {$validator} name='{$item}' id='" . str_replace($replace_id, '', $item) . "' class='form-control'>";
                        $tmp_dropdown = lang($select_by);
                        if (is_array($tmp_dropdown)) {
                            if (isset($tmp_dropdown['LABEL'])) {
                                unset($tmp_dropdown['LABEL']);
                            }
                            if ($required != 'true') {
                                $form_data.="<option value=''>" . label('MSG_SELECT') . "</option>";
                            }
                            foreach ($tmp_dropdown as $option => $optionval) {
                                $form_data.="<option value='{$option}' " . ($option == $value ? 'selected' : '') . ">{$optionval}</option>";
                            }
                        }
                        $form_data.="</select>";
                    } else {

                        $dbtable = $CI->config->item($select_by);

//                        if (!empty($dbtable['db_filter']) || $loadAjax !== TRUE) {                            
//                            $CI->load->model($dbtable['db_model']);
//                            $table = ses_path($dbtable['db_model']);
//                            $nrow = $CI->$table->count_by_filter($filter_db);
//                            if ($nrow > 100) {
//                                $loadAjax = TRUE;
//                                if(!empty($value)){
//                                    $filter_db['PK_ID'] = $value;
//                                }
//                            }
//                        }
                        if ($loadAjax === TRUE) {
                            if (!empty($value)) {
                                $filter_db['PK_ID'] = $value;
                            }
                        }

                        $tmp_dropdown = array();
                        if ($loadAjax !== TRUE || $filter_db['PK_ID'] != '') {
                            $tmp_dropdown = get_data_model($select_by, NULL, 'array', $filter_db);
                        }

                        $form_data.="<select {$atb} {$validator} name='{$item}' id='" . str_replace($replace_id, '', $item) . "' class='form-control'>";
                        if ($required != 'true' || !empty($onChange) || !empty($filter)) {
                            $form_data.="<option value=''>" . label('MSG_SELECT') . "</option>";
                        }
                        $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');

                        $temp = array();
                        if (is_array($tmp_dropdown)) {
                            reset($tmp_dropdown);

                            //$tmp_script = " var temp{$item} = new Array(); ";

                            $i = 0;
                            $temp = array();

                            foreach ($tmp_dropdown as $key => $row) {

                                $tmp_val = '';
                                $temp[$i] = new stdClass();
                                if (is_array($row['filter'])) {

                                    foreach ($row['filter'] as $field => $filed_val) {
                                        $filed_val = ($filed_val == '') ? 0 : $filed_val;
                                        $tmp_val.='_' . $filed_val;

                                        $field = trim($field);
                                        if (!empty($parent_filter[$field])) {
                                            $field = $parent_filter[$field];
                                        }
                                        $temp[$i]->$field = $filed_val;
                                    }
                                }
                                $temp[$i]->id = $key;
                                $temp[$i]->value = $row[$language];
                                if ($loadOnChange === FALSE || !is_array($row['filter']) || $key == $value) {
                                    $form_data.="<option value='{$key}' " . ($key == $value ? 'selected' : '') . ">{$row[$language]}</option>";
                                }

                                $i++;
                            }
                        }
                        $form_data.="</select>";

                        $db_filter = explode(',', $dbtable['db_filter']);

                        $i = 0;
                        $tempFilter = array();
                        $check_filter = FALSE;
                        foreach ($db_filter as $field) {
                            $tempFilter[$i] = new stdClass();
                            if (!empty($parent_filter[$field])) {
                                $field = $parent_filter[$field];
                            }
                            $tempFilter[$i]->field = trim($field);
                            if (!empty($tempFilter[$i]->field)) {
                                $check_filter = TRUE;
                            }
                            $i++;
                        }

                        $temp = json_encode($temp);
                        $tempFilter = json_encode($tempFilter);

                        $form_script_data = '';
                        if ($check_filter === TRUE) {
                            $form_script_data.= "
                                       var temp{$item}= {$temp};
                                       var filter{$item}={$tempFilter};
 
                                                 ";
                        }
                        if ($loadAjax === TRUE) {
                            $form_script_data.= "
                                        function get_data{$grid_name}_{$item}(ele){
                                            $(function(){
                                                var dt = {table:'{$select_by}', loadOnChange:{$loadOnChange}, parent_filter:'" . json_encode($parent_filter) . "'};
                                                    
                                                for(j=0;j<filter{$item}.length;j++){
                                                    element = filter{$item}[j].field;
                                                    element =$.trim(element);
                                                    if($('#form_edit{$grid_name}{$id} #'+element).val()){
                                                        dt[element] = $('#form_edit{$grid_name}{$id} #'+element).val();
                                                    }
                                                } 
                                                $.ajax({
                                                    type: 'POST',
                                                    url: base_url+'ajax/dropdown/',
                                                    data : dt,
                                                    dataType : 'json',
                                                    error: function(response, textStatus, errorThrown){ 
                                                        switch (textStatus) {
                                                            case 'error':
                                                            case 'timeout':                                                                                                        
                                                                alert(response.status +' '+ errorThrown);
                                                                break; 
                                                            case 'parsererror':
                                                                //alert(strip_tags(response.responseText));
                                                                alert(response.status +' Parser error');
                                                                break; 
                                                            default: 
                                                                console.log('response=',response);
                                                                console.log('textStatus=',textStatus);
                                                                console.log('errorThrown=',errorThrown);
                                                                break;
                                                        }
                                                    },
                                                    success: function(response){  
                                                        if($.type(response) === 'array'){
                                                            var text = '';
                                                            var child_value = $('#form_edit{$grid_name}{$id} #{$item}').val();
                                                            $('#form_edit{$grid_name}{$id} #{$item}').html('<option value=\"\">" . label('MSG_SELECT') . "</option>');
                                                            for(i=0;i<response.length;i++){
                                                                text = '<option value=\"'+ response[i].id+'\"'+(response[i].id==child_value?\" selected\":\"\")+'>'+response[i].value+'</option>';
                                                                $('#form_edit{$grid_name}{$id} #{$item}').append(text); 
                                                            }
                                                            //$('#form_edit{$grid_name}{$id} #{$item}').change();
                                                        }
                                                    }
                                                });   
                                            });
                                        }
                                       ";
                        }

                        $arrFilter = explode(',', $filter);
                        if (!empty($arrFilter) && !empty($filter)) {
                            $form_script_data.= " 
                                            $(function() {
                                               $('#form_edit{$grid_name}{$id} #{$item}').change(function() {
                                         ";
                            foreach ($arrFilter as $f) {
                                if (!empty($f)) {
                                    $form_script_data.= "                   
                                                if ($.type(temp{$f}) != 'undefined') {
                                                    size{$f}=temp{$f}.length;
                                                    ";
                                    //if ($loadAjax === TRUE) {
                                    $form_script_data.= "  
                                                    if(size{$f} <= 1){ 
                                                        if (typeof get_data{$grid_name}_{$f} !== 'undefined' && $.isFunction(get_data{$grid_name}_{$f})) {    
                                                            get_data{$grid_name}_{$f}('{$item}');
                                                        }
                                                    }
                                                    ";
                                    //}
                                    $form_script_data.= "  
                                                    sizeFilter{$f}=filter{$f}.length; 
                                                    select = '';
                                                    add =true;
                                                    text = '';
                                                    var child_value = $('#form_edit{$grid_name}{$id} #{$f}').val();
                                                    $('#form_edit{$grid_name}{$id} #{$f}').html('<option value=\"\">" . label('MSG_SELECT') . "</option>');    
                                                           
                                                    for(i=0;i<size{$f};i++){
                                                        for(j=0;j<sizeFilter{$f};j++){

                                                            element = filter{$f}[j].field;
                                                            element =$.trim(element);		
                                                            if($('#form_edit{$grid_name}{$id} #'+element).length){
                                                                if($('#form_edit{$grid_name}{$id} #'+element).val()){
                                                                    select = $('#form_edit{$grid_name}{$id} #'+element).val();
                                                                    if(temp{$f}[i] && select!=''){
                                                                        var ele = temp{$f}[i];
                                                                        $.each(ele, function(index, value) {
                                                                            if(index==element){
                                                                                if(value!=select){
                                                                                    add=false;
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                }else{
                                                                    add=false; 
                                                                }
                                                            } 
                                                        }
                                                        if(add==true){ 
                                                            text = '<option value=\"'+ temp{$f}[i].id+'\"'+(temp{$f}[i].id==child_value?\" selected\":\"\")+'>'+temp{$f}[i].value+'</option>';
                                                            $('#form_edit{$grid_name}{$id} #{$f}').append(text);

                                                        }
                                                        add=true;
                                                    }
                                                }
                                                                    ";
                                }
                            }
                            $form_script_data.= "

                                                return false;
                                            });
                                        }); ";
                        }
                        if (!empty($form_script_data)) {
                            $form_data.= "
                                       <script>
                                       ";
                            $form_data.= $form_script_data;
                            $form_data.= "
                                   </script>
                                             ";
                        }
                    }

                    break;

                case 'controller':
                    $value_text = '';
                    if (!empty($value)) {
                        $value_text = ", {'value':'{$value}'}";
                    }
                    $form_data.= "
                         <script>
                              $(function() {
                                   $('#" . str_replace($replace_id, '', $item) . "filter').load(base_url+'{$url}/index/{$parent_id}/{$gridname}'{$value_text});
                              });
                         </script>
                         <div id='" . str_replace($replace_id, '', $item) . "filter'></div>
                         <input name='{$item}' id='" . str_replace($replace_id, '', $item) . "' type='hidden' value=''/>    
                               ";

                    break;
                case 'autocomplete':

                    $trigger = ($value != '') ? $value : $trigger;
                    if ($select_by_id == 'true') {
                        $trigger = ($trigger == '') ? $id : $trigger;
                    }

                    if ($db_table != '') {
                        $dbtable = $CI->config->item($db_table);

                        if ($dbtable['db_cache'] == 'true') {
                            get_data_model($db_table, NULL, 'array');
                        }
                    }

                    if ($id != '') {
                        if ($link_table != '') {

                            $linktable = $CI->config->item($link_table);
                            $table = ses_path($linktable['db_model']);
                            $CI->load->model($linktable['db_model']);
                            $filter[$linktable['db_filter']] = $id;
                            $result = $CI->$table->select_by_filter(array($dbtable['db_id']), $filter, $dbtable['db_order']);

                            $tmp = '';
                            if (is_object($result)) {
                                while ($rs = $result->FetchRow()) {
                                    $tmp[] = $rs[$dbtable['db_id']];
                                }
                            }
                            $trigger = $tmp;
                        }
                    }
                    $model = ($dbtable['db_model'] != '') ? $dbtable['db_model'] : $model;
                    $url = $model;

                    if ($trigger != '') {
                        $result = get_data_autocomplete($model, $trigger, $filter, $db_table);
                    }

                    if ($onSelect != '') {
                        $onSelect = "onselect: function() {
                                             var dt = $('#form_edit{$grid_name}{$id}').serialize();
                                             $.ajax({
                                                   type: 'POST',
                                                   url: base_url+'{$onSelect}/index/',
                                                   data : dt ,
                                                   dataType : 'html',
                                                   success: function(data){
                                                        $('#" . str_replace($replace_id, '', $item) . "filter').html(data);
                                                    }
                                             });
                                        },
                                        onremove: function() {
                                             $('#" . str_replace($replace_id, '', $item) . "filter').html('');
                                        }
                                          ";
                    } else {
                        $onSelect = '';
                        if (!empty($scriptOnSelect)) {

                            $onSelect.= "onselect: function(){
                                        {$scriptOnSelect}
                                     },
                                     ";
                        }
                        if (!empty($scriptOnRemove)) {

                            $onSelect.= "onremove: function(){
                                        {$scriptOnRemove}
                                     }
                                     ";
                        }
                    }
                    $maxitems = ($maxitems == '') ? '' : 'maxitems :' . $maxitems . ',';
                    $default_data = '';
                    if (is_array($result) && sizeof($result) > 0) {
                        $d = array();
                        foreach ($result as $index => $addItem) {
                            $default_data.= ".trigger('addItem'," . json_encode(array($addItem)) . ")";
                        }
                    }
                    $width = empty($width) ? '' : "width:{$width},";

                    if (!empty($filter) && is_array($filter)) {
                        $filter_data = "filter_data:'" . json_encode(array_filter($filter)) . "',";
                    }
                    $filter_ele = '';
                    if (!empty($filter_element)) {
                        $filter_ele = "filter_element:'{$filter_element}',";
                    }

                    $form_data.= "
                         <select id='" . str_replace($replace_id, '', $item) . "{$grid_name}{$id}' name='{$item}'><option value=''>" . label(MSG_SELECT) . "</option></select>
                         <script>
                              $(function() {
                                    $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}{$id}').fcbkcomplete({
                                        json_url: base_url+'ajax/autocomplete/',
                                        addontab: true,
                                        firstselected:true,
                                        complete_text: '" . label(MSG_SELECT) . "',
                                        {$maxitems}
                                        {$width}
                                        height: 15,
                                        form_data : 'form_edit{$grid_name}{$id}',
                                        config_table : '{$db_table}',
                                        {$filter_data}
                                        {$filter_ele}
                                        {$onSelect}
                                    });
                                    ";
                    if (!empty($default_data)) {
                        $form_data.= "$('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "{$grid_name}{$id}'){$default_data}; ";
                    }
                    $form_data.= "
                              });
                         </script>
                         <div id='" . str_replace($replace_id, '', $item) . "filter'></div>
						<br/>
						<br/>
                               ";

                    break;



                case 'spinner' :
                    $step = (!empty($step) ? $step : 1);
                    $min = (!empty($min) ? $min : 0);
                    $max = (!empty($max) ? $max : 99);
                    $value = (!empty($value) ? $value : $min);
                    $form_data.= "
                         <input name='{$item}' id='" . str_replace($replace_id, '', $item) . "' value='{$value}'/>
                         <script>
                              $(function() {
                                    $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').spinner({
                                        step:{$step},
                                        min:{$min},
                                        max:{$max}
                                    });
                              });
                         </script>
                                    ";
                    break;

                case 'checkbox-multi' :
                    $select_by = trim($select_by);
                    $db_table = trim($db_table);

                    $label = label($item);
                    $label = trim(str_replace(' ', '&nbsp;', $label));

                    $t_g = "<input type='hidden' name='{$item}' id='" . str_replace($replace_id, '', $item) . "' value='' class='hide'>";
                    $t_g.= "<span id='span{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "'></span>";
                    $t_g.= "
                        <table id='table_to_grid{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "'>";
                    $t_g.= "<tr><thead><th>select</th><th>{$label}</th></thead></tr>";
                    $t_g.= "<tbody>";

                    if (!empty($value)) {
                        $default = $value;
                    }
                    $chk_default = strstr($default, ',');
                    $arrDefault = array();
                    if ($chk_default) {
                        $arrDefault_temp = explode(',', $default);
                        if (!empty($arrDefault_temp)) {
                            foreach ($arrDefault_temp as $default_value) {
                                $default_value = trim($default_value);
                                $arrDefault[$default_value] = $default_value;
                            }
                        }
                    } else {
                        $default = trim($default);
                        $arrDefault[$default] = $default;
                    }

                    $rowNum = 1;

                    if (!empty($db_table)) {
                        $tmp_dropdown = get_data_model($db_table, NULL, 'array', $filter_db);
                        $dbtable = $CI->config->item($db_table);
                        $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                        if (is_array($tmp_dropdown)) {
                            reset($tmp_dropdown);

                            foreach ($tmp_dropdown as $option => $optionval) {
                                $t_g.= "<tr>";
                                $t_g.= "<td><input type='checkbox' name='" . str_replace($replace_id, '', $item) . "[]' id='" . str_replace($replace_id, '', $item) . "_{$option}' value='{$option}' " . (!empty($arrDefault[$option]) ? 'checked' : '') . " /></td>";
                                $t_g.= "<td>{$optionval[$language]}</td>";
                                $t_g.= "</tr>";
                                $rowNum++;
                            }
                        }
                    } else if (!empty($select_by)) {
                        $temp = lang($select_by);
                        if (isset($temp['LABEL'])) {
                            unset($temp['LABEL']);
                        }
                        if (is_array($temp)) {
                            foreach ($temp as $option => $optionval) {

                                $t_g.= "
                                    <tr>";
                                $t_g.= "<td><input type='checkbox' name='" . str_replace($replace_id, '', $item) . "[]' id='" . str_replace($replace_id, '', $item) . "}_{$option}' value='{$option}' " . (!empty($arrDefault[$option]) ? 'checked' : '') . " /></td>";
                                $t_g.= "<td>{$optionval}</td>";
                                $t_g.= "</tr>";
                                $rowNum++;
                            }
                        }
                    }
                    $multi = 'true';
                    if ($multiselect == FALSE) {
                        $multi = 'false';
                    }

                    $t_g.= "</tbody>";
                    $t_g.= "</table>";
                    //$t_g.= "<div id='pg_to_grid{$grid_name}{$id}_{$item}'></div>";
                    $t_g .="<div style='width:100%;display:inline-block;' id='div_width_form{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "'></div>";
                    $height = empty($height) ? 115 : $height;
                    $t_g.= "
                             <script type='text/javascript'>
                                  $(function(){
                                       tableToGrid('#form_edit{$grid_name}{$id} #table_to_grid{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "', {
                                            width:$('#div_width_form{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "').width(),
                                            multiselect:{$multi},
                                            height: '{$height}',
                                            //altRows : true,
                                            scrollOffset:'25',
                                            //pager: '#form_edit{$grid_name}{$id} #pg_to_grid{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "',
                                            pgbuttons:false,
                                            pginput:false,
                                            rowNum:{$rowNum},
                                            viewrecords: true,
                                            colModel:[
                                                 {name:'select', sortable:false,hidden:true,width:10},
                                                 {name:'{$label}', sortable:false, width:350}
                                            ],
                                            //caption:'" . label($item) . "'
                                       });
                                       /*
                                       $('#form_edit{$grid_name}{$id} #table_to_grid{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "').jqGrid('navGrid', '#pg_to_grid{$grid_name}{$id}_" . str_replace($replace_id, '', $item) . "',{
                                             view:false,
                                             edit:false,
                                             add:false,
                                             del:false,
                                             search:false,
                                             refresh:false
                                       });
                                       */
                                  });
                             </script>";

                    $form_data.=$t_g;

                    break;

                case 'color-picker' :
                    if (empty($value)) {
                        $value = 'ffffff';
                    }
                    $form_data.= "
                         #<input name='{$item}' id='" . str_replace($replace_id, '', $item) . "' value='{$value}' maxlength='6' readonly='true' class='color-picker'/>
                         <script>
                              $(function() {
                                    $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').colpick({
                                        layout:'hex',
                                        submit:0,
                                        color:'{$value}',
                                        onChange:function(hsb,hex,rgb,el,bySetColor) {
                                            $(el).css('border-color','#'+hex);
                                            // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
                                            if(!bySetColor) $(el).val(hex);
                                        }
                                    }).keyup(function(){
                                        $(this).colpickSetColor(this.value);
                                    });
                                    $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').css('border-color','#{$value}');
                              });
                         </script> 
                                    ";
                    break;

                default :
                    $autocomplete = '';
                    $inline = '';
                    $control = 'form-control';
                    if ($html != '') {
                        $inline = 'form-inline ';
                    }
                    if ($type == 'file') {
                        $control = 'file-styled-primary';
                    }
                    if (empty($validator)) {
                        $validator = "class='{$inline}{$control}'";
                    }
                    
                    if($view_year === TRUE){
                        $value = view_year($value);
                    }

                    if ($chk_onlyFloat == TRUE) {
                        if (!empty($atb)) {
                            $temp = array_filter(explode(' ', $atb));
                            if (is_array($temp) && !empty($temp)) {
                                $a = array();
                                foreach ($temp as $row) {
                                    $temp_2 = explode('=', $row);
                                    switch ($temp_2[0]) {
                                        case 'db_table_filter' :
                                        case 'db_table' :
                                        case 'maxitems' :
                                        case 'filter' :
                                        case 'search' :
                                        //case 'type' :
                                        case 'visible' :
                                        case 'search' :
                                        //case 'maxlength' :
                                        case 'required' :
                                        case 'default' :
                                        case 'search_default' :
                                        case 'select_by' :
                                        case 'parent_filter' :
                                        case 'size' :
                                        case 'sql_select' :
                                        case 'path' :
                                        case 'filter_db' : break;
                                        default : $a[] = $row;
                                            break;
                                    }
                                }
                                $atb = implode(' ', $a);
                            }
                        }
                        $autocomplete = "autocomplete='off'";
                        $temp = explode('.', $value);
                        if ($temp[0] == '') {
                            $temp[0] = 0;
                        }
                        if ($temp[1] == '') {
                            $temp[1] = '00';
                        }
                        if (strlen($temp[1]) > 2) {
                            $temp[1] = substr($temp[1], 0, 2);
                        }
                        $temp[1] = str_pad($temp[1], 2, '0');
                        $value = $temp[0] . '.' . $temp[1];
                        $size_onlyFloat = 'size="15" style="text-align:right;"';
                        $inline = 'form-inline ';
                    } else {
                        if (!empty($atb)) {
                            $temp = array_filter(explode(' ', $atb));

                            if (is_array($temp) && !empty($temp)) {
                                $a = array();
                                foreach ($temp as $row) {
                                    $temp_2 = explode('=', $row);
                                    switch ($temp_2[0]) {
                                        case 'db_table_filter' :
                                        case 'db_table' :
                                        case 'maxitems' :
                                        case 'filter' :
                                        case 'search' :
                                        //case 'type' :
                                        case 'visible' :
                                        case 'search' :
                                        //case 'maxlength' :
                                        case 'required' :
                                        case 'default' :
                                        case 'search_default' :
                                        case 'select_by' :
                                        case 'parent_filter' :
                                        //case 'size' :
                                        case 'sql_select' :
                                        case 'path' :
                                        case 'filter_db' : break;
                                        default : $a[] = $row;
                                            break;
                                    }
                                }
                                $atb = implode(' ', $a);
                            }
                        }
                    }

                    $form_data.="<input {$atb} {$validator} name='{$item}' id='" . str_replace($replace_id, '', $item) . "' value=\"" . htmlspecialchars($value) . "\" {$size_onlyFloat} {$autocomplete}/> ";

                    if ($type == 'file') {
                        if (!empty($path)) {
                            $form_data.=" 
                        <input name='{$item}_PATH' id='" . str_replace($replace_id, '', $item) . "_PATH{$grid_name}' type='hidden' value='{$path}'/>
                         ";
                            if (!empty($value)) {
                                $file = $path . $value;
                                if (is_file($file)) {
                                    $form_data.= "<br/><font color='#ff0000' size='2em'><b>***When you upload a new file into system the old one will be replaced.***</b></font><br/>";
                                    $form_data.= "<a target='_blank' href='{$file}' class='btn btn-link' role='button'>";
                                    $form_data.= "<image src='" . file_type($file) . "'/>{$value}";
                                    $form_data.= "</a><br/>";
                                }
                            }
                        }
                    }

                    break;
            }
            if ($html != '') {
                $form_data.=$html;
            }
            if ($chk_onlyFloat == TRUE) {
                if (empty($value)) {
                    $value = 0;
                }
                $form_data.= "
                         <div id='show_number_" . str_replace($replace_id, '', $item) . "' style='width:115px;text-align:right;font-style:italic;color:green;font-weight:bold;font-size:15px;'>" . number_format($value, 2) . "</div>
                         <script>
                              $(function() {
                                $('#form_edit{$grid_name}{$id} #{$item}').keyup(function() {
                                      $('#form_edit{$grid_name}{$id} #show_number_" . str_replace($replace_id, '', $item) . "').html('');
                                      $('#form_edit{$grid_name}{$id} #show_number_" . str_replace($replace_id, '', $item) . "').html($.number($('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').val(),2));
                                });
                                $('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').change(function() {
                                      $('#form_edit{$grid_name}{$id} #show_number_" . str_replace($replace_id, '', $item) . "').html('');
                                      $('#form_edit{$grid_name}{$id} #show_number_" . str_replace($replace_id, '', $item) . "').html($.number($('#form_edit{$grid_name}{$id} #" . str_replace($replace_id, '', $item) . "').val(),2));
                                });
                              });
                         </script>
                                    ";
            }

            if ($type != 'hidden') {
                //$form_data.='</td>';
                $form_data.=' 
        </div>'; //col-lg-9
//                if ($colspan != 'true') {
//                    $form_data.="</tr>";
//                    $form_data.="<tr>";
//                }
                $form_data.=' 
    </div> 
</div>'; //col-md
                if ($colspan != 'true') {
                    $form_data.="<div class='new_line'></div>";
                }
            }
        }//visible
        //$form_data.='<label>';
        //$form_data.= $label;
        //$form_data.= "<{$ftb} {$atb} {$validator} name={$item} id={$item}  />" . "<br/>";
        //$form_data.='</label>';

        $form_data.= $afterElement;

        return $form_data;
    }

}

if (!function_exists('confirm_form')) {

    /**
     * สร้าง Form สำหรับจัดการข้อมูลจากพารามิเตอร์ของฟอร์มที่ต้องการ
     * @param string $field <p>
     * id ของฟิลด์
     * </p>
     * @param array $params  <p>
     * พารามิเตอร์ของฟิลด์นั้น
     * </p>
     * @param string $item [optional] <p>
     * id ของฟิลด์หลังจากแยก Multilanguage แล้ว
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function confirm_form($field, $params = array(), $item = NULL, $id = NULL, $value = NULL, $irow, $grid_name = NULL) {

        $CI = & get_instance();

        if (!$item)
            $item = $field;

//        $label = lang($field);
//        if (is_array($label)) {
//            $label = $label['LABEL'];
//        } else {
//            if (check_lang($field)) {
//                $sel_lang = check_select_lang($item);
//                if ($sel_lang != lang('SUFFIX')) {
//                    $label .= " (" . ucfirst($sel_lang) . ")";
//                } else {
//                    $label .= " (" . ucfirst(lang('SUFFIX')) . ")";
//                }
//            }
//        }
//
//        $label = ($label == '') ? $item : $label;

        $label = lang($field);
        if (is_array($label)) {
            $label = $label['LABEL'];
        } else {
            $label = trim($label);
            $temp_label = $label;
            if (check_lang($field)) {
                $sel_lang = check_select_lang($item);
                if ($sel_lang != label('SUFFIX')) {
                    $label .= " (" . ucfirst($sel_lang) . ")";
                } else {
                    $label .= " (" . ucfirst(lang('SUFFIX')) . ")";
                }
            }
            if (empty($temp_label)) {
                $label = substr($field, 0, strlen($field) - 2) . 'ML';
                $label_2 = label($label);
                if ($label != $label_2) {
                    $label = $label_2 . ' (' . ucfirst(check_select_lang($field)) . ')';
                } else {
                    $label = $item;
                }
            }
        }
        $label = ($label == '') ? $field : $label; //echo $label.'<hr/>';

        $default = '';
        $validator = 'class=validate[';
        foreach ($params as $param => $paramval) {
            if ($param == 'required' || $param == 'optional' || $param == 'custom' || $param == 'length' || $param == 'ajax' ||
                    $param == 'minCheckbox' || $param == 'confirm'
            ) {
                if ($param == 'required' || $param == 'optional') {
                    if ($paramval == 'true') {
                        $validator .="{$param},";
                        $required = 'true';
                    }
                } else {
                    $validator .="{$param}[$paramval],";
                }
            } else if ($param == 'colspan') {

                if ($paramval == 'true')
                    $colspan = 'true';
            }else {
                $$param = $paramval;

                $atb.= "{$param}='{$paramval}' ";
            }
        }
        $validator .= ']';


        if ($visible != 'false') {

            $value = ($value != '') ? $value : $default;

            $select_by = ($select_by != '') ? $select_by : $item;
            $select_by = ($db_table != '') ? $db_table : $select_by;

            $show_label = ($type == 'hidden' || $type == 'checkbox') ? '' : $label;

            $form_data.="<th  align='right'" . ($type == "textarea" || $type == "autocomplete" || $type == "autocomplete_filter" ? " valign='top'" : '') . " width='150'>{$show_label}&nbsp;";
            if ($required == 'true') {
                $form_data.="<div class='ui-icon-req'></div>";
            }
            $form_data.='</th>';

            $form_data.="<td width='350'>";

            //$type = ($type == 'radio') ? 'dropdown' : $type;
//            $type = ($type == 'radio' || $type == 'dbdropdown_filter'  || $type == "autocomplete_filter" ) ? 'dropdown' : $type;

            switch ($type) {

                case 'textarea' :
                    $form_data.=$value;
                    break;

                case 'datepicker' :

                    $value_date = view_date($value);

                    $form_data.=$value_date;
                    break;
                case 'timepicker' :

                    $value_time = view_time($value);

                    $form_data.=$value_time;
                    break;

                case 'dropdown' :
                    $temp = lang($select_by);
                    if (is_array($temp)) {
                        foreach ($temp as $option => $optionval) {
                            if ($option != 'LABEL') {
                                $form_data.= ( $option == $value ? $optionval : '');
                            } else {
                                unset($temp['LABEL']);
                            }
                        }
                    }
                    break;

                case 'radio' :

                    if (isset($db_table)) {
                        $temp = get_data_model($db_table, NULL, 'array');
                        $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                    } else if (isset($select_by)) {
                        $temp = lang($select_by);
                        if (isset($temp['LABEL'])) {
                            unset($temp['LABEL']);
                        }
                    }

                    if (is_array($temp)) {
                        foreach ($temp as $option => $optionval) {
                            if (is_array($optionval)) {
                                $optionval = $optionval[$language];
                            }
                            $form_data.= ( $option == $value ? $optionval : '');
                        }
                    }
                    break;


                case 'dbdropdown' :

                    $form_data.=get_data_model($select_by, $value) . ' ';

                    break;

                case 'autocomplete':
                    $form_data.=get_data_model($select_by, $value) . ' ';

                    break;

                case 'hidden' :
                    $form_data.='';
                    break;

                default :
                    $form_data.=$value;
                    break;
            }

            if (!isset($show_html_after_confirm)) {
                $show_html_after_confirm = TRUE;
            }

            if ($html != '' && $show_html_after_confirm != FALSE) {
                $form_data.=$html;
            }


            $form_data.='</td>';
            if ($irow % 2) {
                $form_data.="</tr>";
                $form_data.="<tr>";
            }
        }//visible



        return $form_data;
    }

}

if (!function_exists('set_search_form')) {

    /**
     * สร้าง Form สำหรับค้นหาข้อมูลจากพารามิเตอร์ของฟอร์มที่ต้องการ
     * @param string $field <p>
     * id ของฟิลด์
     * </p>
     * @param array $params  <p>
     * พารามิเตอร์ของฟิลด์นั้น
     * </p>
     * @param string $item [optional] <p>
     * id ของฟิลด์หลังจากแยก Multilanguage แล้ว
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_search_form($field, $params = array(), $item = NULL, $col_index = NULL, $grid_name = NULL) {

        $ck_auto = FALSE;

        //$col_index = $value;
        $CI = & get_instance();
        if (!$item) {
            $item = $field;
        }
        $label = lang($field);
        if (is_array($label)) {
            $label = $label['LABEL'];
        } else {
            $label = trim($label);
            if (check_lang($field)) {
                $sel_lang = check_select_lang($item);
                if ($sel_lang != lang('SUFFIX')) {
                    $label .= " (" . ucfirst($sel_lang) . ")";
                } else {
                    $label .= " (" . ucfirst(lang('SUFFIX')) . ")";
                }
            } else if (empty($label)) {
                $label = substr($field, 0, strlen($field) - 2) . 'ML';
                $label_2 = label($label);
                if ($label != $label_2) {
                    $label = $label_2 . ' (' . ucfirst(check_select_lang($item)) . ')';
                } else {
                    $label = $field;
                }
            }
        }
        $label = ($label == '') ? $field : $label;

        $loadOnChange = TRUE;
        foreach ($params as $param => $paramval) {
            if ($param != 'html') {
                $$param = $paramval;
                $atb.= "{$param}='{$paramval}' ";
            }
        }

        $value = $default_search; //($value != '') ? $value : $default;

        $select_by = ($select_by != '') ? $select_by : $item;
        $select_by = ($db_table != '') ? $db_table : $select_by;


        $type = ($type == 'radio') ? 'dropdown' : $type;
        //$type = ($type == 'autocomplete') ? 'dropdown' : $type;
        $type = ($type == 'autocomplete_search') ? 'autocomplete' : $type;


        $valign = (!empty($valign) ? $valign : ($type == "autocomplete" || $type == "checkbox-multi" || !empty($html) ? "top" : "center"));

        $temp_label = $label;
        $label = ($type == 'hidden' || $type == 'checkbox') ? '' : $label;
        //$form_data = '';
//        $form_data.="<th align='right' valign='{$valign}'>{$label}&nbsp;&nbsp;";
//
//        $form_data.=($search_required == 'true') ? "<div class='ui-icon-req'></div>" : '';
//
//        $form_data.='</th>';
//
//        $label = ($type == 'checkbox') ? $temp_label : $label;
//
//        $form_data.='<td>';
        //echo $col_index.'=='.$label.'==>'.($col_index % 2).'<br/>';
        $form_data = $beforeElementSearch;
        $form_data.= '  
    <div class="col-sm-6">
            ';

        $form_data.= " 
        <div class='form-group'>
            <label class='col-md-3 control-label'>{$label} &nbsp; " . ($search_required == 'true' ? "<div class='ui-icon-req'></div>" : '') . "</label>
            <div class='col-md-9'>
            ";

        switch ($type) {

            case 'datepicker' :
            case 'datetime' :

                $value_date = view_date($value);

                $form_data.="<input name='{$item}' id='{$item}{$grid_name}' class='form-inline form-control' size='10' maxlength='10' style='text-align:center;' value='{$value_date}'/> (dd/mm/yyyy)";
                $form_data.= "
                              <script>
                                   $(function() {
                                        $('#form_search{$grid_name} #{$item}{$grid_name}').datepicker({dateFormat: 'dd/mm/yy'});
                                   });
                              </script>
                                        ";
                break;


            case 'autocomplete':

                $trigger = ($value != '') ? $value : $trigger;
                if ($select_by_id == 'true') {
                    $trigger = ($trigger == '') ? $id : $trigger;
                }

                if ($db_table != '') {

                    $dbtable = $CI->config->item($db_table);

                    if ($dbtable['db_cache'] == 'true') {
                        get_data_model($db_table, NULL, 'array');
                    }
                }
//echo $value;

                $model = ($dbtable['db_model'] != '') ? $dbtable['db_model'] : $model;
                $url = $model;


                if ($onSelect != '') {
                    $onSelect = "onselect: function() {
                                             var dt = $('#form_search{$grid_name}{$id}').serialize();

                                             $.ajax({
                                                       type: 'POST',
                                                       url: base_url+'{$onSelect}/index/',
                                                       data : dt ,
                                                       dataType : 'html',
                                                       success: function(data){
                                                            $('#div_search{$grid_name}').html(data);
                                                        }

                                             });
                                        },
                                        onremove: function() {
                                              $('#div_search{$grid_name}').html('');
                                        }
                                          ";
                }

                $maxitems = ($maxitems == '') ? '' : 'maxitems :' . $maxitems . ',';
                $width = empty($width) ? '' : "width:{$width},";

                if (!empty($filter) && is_array($filter)) {
                    $filter_data = "filter_data:'" . json_encode(array_filter($filter)) . "',";
                }
                $filter_ele = '';
                if (!empty($filter_element)) {
                    $filter_ele = "filter_element:'{$filter_element}',";
                }
                $form_data.= "
                         <select id='{$item}{$grid_name}' name='{$item}'>
                                       <option value=''>" . label(MSG_SELECT) . "</option>
                         </select>
                         <script>
                              $(function() {
                                    $('#form_search{$grid_name} #{$item}{$grid_name}').fcbkcomplete({
                                        json_url: base_url+'ajax/autocomplete/',
                                        addontab: true,
                                        firstselected:true,
                                        complete_text: '" . label(MSG_SELECT) . "',
                                        {$maxitems}
                                        {$width}
                                        height: 15,
                                        form_data : 'form_search{$grid_name}',
                                        config_table : '{$db_table}',
                                        {$filter_data}
                                        {$filter_ele}
                                        {$onSelect}
                                    });
                              });
                         </script>                                    
						<br/>
						<br/>
                               ";

                $ck_auto = TRUE;
                break;

            case 'dropdown' :
            case 'dbdropdown' :
                if (!empty($atb)) {
                    $temp = array_filter(explode(' ', $atb));
                    if (is_array($temp) && !empty($temp)) {
                        $a = array();
                        foreach ($temp as $row) {
                            $temp_2 = explode('=', $row);
                            switch ($temp_2[0]) {
                                case 'db_table_filter' :
                                case 'db_table' :
                                case 'maxitems' :
                                case 'filter' :
                                case 'search' :
                                case 'type' :
                                case 'visible' :
                                case 'type' :
                                case 'search' :
                                case 'maxlength' :
                                case 'required' :
                                case 'default' :
                                case 'search_default' :
                                case 'select_by' :
                                case 'parent_filter' :
                                case 'search_required' :
                                case 'sql_select' :
                                case 'filter_db' : break;
                                default : $a[] = $row;
                                    break;
                            }
                        }
                        $atb = implode(' ', $a);
                    }
                }
                if (empty($db_table)) {
                    $form_data.="<select {$atb} {$validator} name='{$item}' id='{$item}' class='form-control'>";
                    $tmp_dropdown = lang($select_by);
                    if (is_array($tmp_dropdown)) {
                        if (isset($tmp_dropdown['LABEL'])) {
                            unset($tmp_dropdown['LABEL']);
                        }
                        $count_item = count($tmp_dropdown);
                        if ($count_item > 1) {
                            $form_data.="<option value=''>" . label('MSG_SELECT') . "</option>";
                        }
                        foreach ($tmp_dropdown as $option => $optionval) {
                            $form_data.="<option value='{$option}' " . ($option === $value ? 'selected' : '') . ">{$optionval}</option>";
                        }
                    }
                    $form_data.="</select>";
                } else {
////////////////////////////////////////////////////////////////////////                    
                    $dbtable = $CI->config->item($select_by);
                    $tmp_dropdown = array();
                    if ($loadAjax !== TRUE) {
                        $tmp_dropdown = get_data_model($select_by, NULL, 'array', $filter_db);
                    }

                    $count_item = count($tmp_dropdown);
                    $form_data.="<select {$atb} {$validator} name='{$item}' id='{$item}' class='form-control'>";
                    if ($count_item != 1 || $loadAjax === TRUE) {
                        $form_data.="<option value=''>" . label('MSG_SELECT') . "</option>";
                    }
                    $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
//////////////////////////////////////////////////////////////////

                    $temp = array();
                    if (is_array($tmp_dropdown)) {
                        reset($tmp_dropdown);

                        //$tmp_script = " var temp{$item} = new Array(); ";

                        $i = 0;
                        $temp = array();

                        foreach ($tmp_dropdown as $key => $row) {

                            $tmp_val = '';
                            $temp[$i] = new stdClass();
                            if (is_array($row['filter'])) {

                                foreach ($row['filter'] as $field => $filed_val) {
                                    $filed_val = ($filed_val == '') ? 0 : $filed_val;
                                    $tmp_val.='_' . $filed_val;

                                    $field = trim($field);
                                    if (!empty($parent_filter[$field])) {
                                        $field = $parent_filter[$field];
                                    }
                                    $temp[$i]->$field = $filed_val;
                                }
                            }
                            $temp[$i]->id = $key;
                            $temp[$i]->value = $row[$language];
                            if ($loadOnChange === FALSE || !is_array($row['filter']) || $key == $value) {
                                $form_data.="<option value='{$key}' " . ($key === $value ? 'selected' : '') . ">{$row[$language]}</option>";
                            }

                            $i++;
                        }
                    }
                    $form_data.="</select>";

                    $db_filter = explode(',', $dbtable['db_filter']);

                    $i = 0;
                    $tempFilter = array();
                    $check_filter = FALSE;
                    foreach ($db_filter as $field) {
                        $tempFilter[$i] = new stdClass();
                        if (!empty($parent_filter[$field])) {
                            $field = $parent_filter[$field];
                        }
                        $tempFilter[$i]->field = trim($field);
                        if (!empty($tempFilter[$i]->field)) {
                            $check_filter = TRUE;
                        }
                        $i++;
                    }

                    $temp = json_encode($temp);
                    $tempFilter = json_encode($tempFilter);

                    $form_script_data = '';
                    if ($check_filter === TRUE) {
                        $form_script_data.= "
                                       var temp{$item}= {$temp};
                                       var filter{$item}={$tempFilter};
 
                                                 ";
                    }
                    if ($loadAjax === TRUE) {
                        $form_script_data.= "
                                        function get_data{$grid_name}_{$item}(ele){
                                            $(function(){
                                                var dt = {table:'{$select_by}', loadOnChange:{$loadOnChange}, parent_filter:'" . json_encode($parent_filter) . "'};
                                                    
                                                for(j=0;j<filter{$item}.length;j++){
                                                    element = filter{$item}[j].field;
                                                    element =$.trim(element);
                                                    if($('#form_search{$grid_name}{$id} #'+element).val()){
                                                        dt[element] = $('#form_search{$grid_name}{$id} #'+element).val();
                                                    }
                                                } 
                                                $.ajax({
                                                    type: 'POST',
                                                    url: base_url+'ajax/dropdown/',
                                                    data : dt,
                                                    dataType : 'json',
                                                    error: function(response, textStatus, errorThrown){ 
                                                        switch (textStatus) {
                                                            case 'error':
                                                            case 'timeout':                                                                                                        
                                                                alert(response.status +' '+ errorThrown);
                                                                break; 
                                                            case 'parsererror':
                                                                //alert(strip_tags(response.responseText));
                                                                alert(response.status +' Parser error');
                                                                break; 
                                                            default: 
                                                                console.log('response=',response);
                                                                console.log('textStatus=',textStatus);
                                                                console.log('errorThrown=',errorThrown);
                                                                break;
                                                        }
                                                    },
                                                    success: function(response){  
                                                        if($.type(response) === 'array'){
                                                            var text = '';
                                                            var child_value = $('#form_search{$grid_name}{$id} #{$item}').val();
                                                            $('#form_search{$grid_name}{$id} #{$item}').html('<option value=\"\">" . label('MSG_SELECT') . "</option>');
                                                            for(i=0;i<response.length;i++){
                                                                text = '<option value=\"'+ response[i].id+'\"'+(response[i].id==child_value?\" selected\":\"\")+'>'+response[i].value+'</option>';
                                                                $('#form_search{$grid_name}{$id} #{$item}').append(text); 
                                                            }
                                                            //$('#form_search{$grid_name}{$id} #{$item}').change();
                                                        }
                                                    }
                                                });   
                                            });
                                        }
                                       ";
                    }

                    $arrFilter = explode(',', $filter);
                    if (!empty($arrFilter) && !empty($filter)) {
                        $form_script_data.= " 
                                            $(function() {
                                               $('#form_search{$grid_name}{$id} #{$item}').change(function() {
                                         ";
                        foreach ($arrFilter as $f) {
                            if (!empty($f)) {
                                $form_script_data.= "   
                                                    if ($.type(temp{$f}) != 'undefined') {
                                                    size{$f}=temp{$f}.length;
                                                    ";
                                //if ($loadAjax === TRUE) {
                                $form_script_data.= " 
                                                    if(size{$f} <= 1){
                                                        if (typeof get_data{$grid_name}_{$f} !== 'undefined' && $.isFunction(get_data{$grid_name}_{$f})) {
                                                            get_data{$grid_name}_{$f}('{$item}');
                                                        }
                                                    }  
                                                    ";
                                //}
                                $form_script_data.= " 
                                                    sizeFilter{$f}=filter{$f}.length; 
                                                        select = '';
                                                        add =true;
                                                        text = '';
                                                        var child_value = $('#form_search{$grid_name}{$id} #{$f}').val();
                                                        $('#form_search{$grid_name}{$id} #{$f}').html('<option value=\"\">" . label('MSG_SELECT') . "</option>');    

                                                    for(i=0;i<size{$f};i++){
                                                        for(j=0;j<sizeFilter{$f};j++){

                                                                element = filter{$f}[j].field;
                                                                element =$.trim(element); 
                                                                if($('#form_search{$grid_name}{$id} #'+element).length){
                                                                    if($('#form_search{$grid_name}{$id} #'+element).val()){  
                                                                        select = $('#form_search{$grid_name}{$id} #'+element).val();
                                                                        if(temp{$f}[i] && select!=''){
                                                                                var ele = temp{$f}[i];
                                                                                $.each(ele, function(index, value) {
                                                                                    if(index==element){ 
                                                                                        if(value!=select){ 
                                                                                            add=false;
                                                                                        }
                                                                                    }
                                                                                });
                                                                            }
                                                                        }else{
                                                                            //add=false; 
                                                                        }
                                                                    } 
                                                                }
                                                                if(add==true){  
                                                                    text = '<option value=\"'+ temp{$f}[i].id+'\"'+(temp{$f}[i].id==child_value?\" selected\":\"\")+'>'+temp{$f}[i].value+'</option>';
                                                                    $('#form_search{$grid_name}{$id} #{$f}').append(text);

                                                                }
                                                                add=true;
                                                        }
                                                    }
                                                                    ";
                            }
                        }
                        $form_script_data.= "

                                                      return false;
                                                 });
                                            }); ";
                    }
                    if (!empty($form_script_data)) {
                        $form_data.= "
                                       <script>
                                       ";
                        $form_data.= $form_script_data;
                        $form_data.= "
                                   </script>
                                             ";
                    }
                }

                break;
            //2013-07-31 
            case 'checkbox' :

                $form_data.="<label for='{$item}'><input  type='checkbox' {$atb} {$validator} name='{$item}' id='{$item}' value='T' />{$label}</label>";
                break;

            case 'checkbox-multi' :
                $select_by = trim($select_by);
                $db_table = trim($db_table);

                $label = label($item);
                $label = trim(str_replace(' ', '&nbsp;', $label));

                $t_g = "
                    <table id='table_to_grid{$grid_name}_{$item}'>";
                $t_g.= "<tr><thead><th>select</th><th>{$label}</th></thead></tr>";
                $t_g.= "<tbody>";

                $chk_default = strstr($default, ',');
                $arrDefault = array();
                if ($chk_default) {
                    $arrDefault_temp = explode(',', $default);
                    if (!empty($arrDefault_temp)) {
                        foreach ($arrDefault_temp as $default_value) {
                            $default_value = trim($default_value);
                            $arrDefault[$default_value] = $default_value;
                        }
                    }
                } else {
                    $default = trim($default);
                    $arrDefault[$default] = $default;
                }

                $rowNum = 1;

                if (!empty($db_table)) {
                    $tmp_dropdown = get_data_model($db_table, NULL, 'array');
                    $dbtable = $CI->config->item($db_table);
                    $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                    if (is_array($tmp_dropdown)) {
                        reset($tmp_dropdown);

                        foreach ($tmp_dropdown as $option => $optionval) {
                            $t_g.= "<tr>";
                            $t_g.= "<td><input type='checkbox' name='{$item}[]' id='{$item}_{$option}' value='{$option}' " . (!empty($arrDefault[$option]) ? 'checked' : '') . " /></td>";
                            $t_g.= "<td>{$optionval[$language]}</td>";
                            $t_g.= "</tr>";
                            $rowNum++;
                        }
                    }
                } else if (!empty($select_by)) {
                    $temp = lang($select_by);
                    if (isset($temp['LABEL'])) {
                        unset($temp['LABEL']);
                    }
                    if (is_array($temp)) {
                        foreach ($temp as $option => $optionval) {

                            $t_g.= "
                                <tr>";
                            $t_g.= "<td><input type='checkbox' name='{$item}[]' id='{$item}_{$option}' value='{$option}' " . (!empty($arrDefault[$option]) ? 'checked' : '') . " /></td>";
                            $t_g.= "<td>{$optionval}</td>";
                            $t_g.= "</tr>";
                            $rowNum++;
                        }
                    }
                }

                $t_g.= "</tbody>";
                $t_g.= "</table>";
                //$t_g.= "<div id='pg_to_grid{$grid_name{$grid_name}_{$item}'></div>";
                $t_g.= "<span id='span{$grid_name}_{$item}'></span>";
                $height = empty($height) ? 115 : $height;
                $t_g.= "
                         <script type='text/javascript'>
                              $(function(){
                                   tableToGrid('#form_search{$grid_name}{$id} #table_to_grid{$grid_name}_{$item}', {
                                        width:'400',
                                        multiselect:true,
                                        height: '{$height}',
                                        //altRows : true,
                                        scrollOffset:'25',
                                        //pager: '#form_search{$grid_name}{$id} #pg_to_grid{$grid_name}_{$item}',
                                        pgbuttons:false,
                                        pginput:false,
                                        rowNum:{$rowNum},
                                        viewrecords: true,
                                        colModel:[
                                             {name:'select', sortable:false,hidden:true,width:'10'},
                                             {name:'{$label}', sortable:false, width:350}
                                        ],
                                        //caption:'" . label($item) . "'
                                   });
                                   /*
                                   $('#form_search{$grid_name}{$id} #table_to_grid{$grid_name}_{$item}').jqGrid('navGrid', '#pg_to_grid{$grid_name}_{$item}',{
                                         view:false,
                                         edit:false,
                                         add:false,
                                         del:false,
                                         search:false,
                                         refresh:false
                                   });
                                   */
                              });
                         </script>";

                $form_data.=$t_g;

                break;

            default :
                if (!empty($atb)) {
                    $temp = array_filter(explode(' ', $atb));
                    if (is_array($temp) && !empty($temp)) {
                        $a = array();
                        foreach ($temp as $row) {
                            $temp_2 = explode('=', $row);
                            switch ($temp_2[0]) {
                                case 'db_table_filter' :
                                case 'db_table' :
                                case 'maxitems' :
                                case 'filter' :
                                case 'search' :
                                case 'type' :
                                case 'visible' :
                                case 'type' :
                                case 'search' :
                                case 'custom' :
                                case 'required' :
                                case 'default' :
                                case 'search_default' :
                                case 'filter_db' : break;
                                default : $a[] = $row;
                                    break;
                            }
                        }
                        $atb = implode(' ', $a);
                    }
                }
                $form_data.="<input {$atb} name='{$item}' id='{$item}' class='form-control' value='{$value}' />";
                break;
        }


        if ($html_search != '') {
            //$form_data.=$html_search;
        }

//        $form_data.= '</td>';
        $form_data.= '</div>'; //col-lg-9

        $form_data.= ' 
        </div>'; //form-group                 
//
        //if ($col_index % 2 || $ck_auto == TRUE) {
//            $form_data.="</tr>";
//            $form_data.="<tr>";

        $form_data.= ' 
    </div>
    '; //col-md-6
        //}
        $form_data.= $afterElementSearch;


        return $form_data;
    }

}

if (!function_exists('get_form_param')) {

    /**
     * รับค่า parameter ของฟอร์มที่ต้องการ
     * @param array $params  <p>
     * พารามิเตอร์ของฟิลด์นั้น
     * </p>
     * @param array $params  <p>
     * พารามิเตอร์ของฟิลด์นั้น
     * </p>*
     * @return string
     * ข้อมูลพารามิเตอร์ของฟิลด์ที่ต้องการ
     *
     */
    function get_form_param($params = array(), $param_check = NULL) {

        //var_dump($params);
        $form_data = '';
        if (is_array($params)) {
            foreach ($params as $param => $paramval) {
                if (!is_array($paramval)) {
                    $$param = trim($paramval);
                    if ($param == $param_check) {
                        $form_data = $paramval;
                    }
                }
            }
        }
        if ($param_check == 'type' && $type == 'checkbox-multi') {
            if (!empty($db_table)) {
                $form_data = 'dbdropdown';
            } else if (!empty($db_table_filter)) {
                $form_data = 'dbdropdown_filter';
            } else if (!empty($select_by)) {
                $form_data = 'dropdown';
            }
        }
        return $form_data;
    }

}

if (!function_exists('check_click_permission')) {

    function check_click_permission($func = NULL, $permission = array()) {
        $return = FALSE;
        if (is_array($permission) && !empty($permission)) {
            switch ($func) {
                case 'add' :
                case 'ADD' : if ($permission['ACTION_ADD'] == 'T') {
                        $return = TRUE;
                    }
                    break;
                case 'edit' :
                case 'EDIT' : if ($permission['ACTION_EDIT'] == 'T') {
                        $return = TRUE;
                    }
                    break;
                case 'delete' :
                case 'DEL' :
                case 'DELETE' : if ($permission['ACTION_DEL'] == 'T') {
                        $return = TRUE;
                    }
                    break;
                default : $return = TRUE;
                    break;
            }
        }

        return $return;
    }

}

if (!function_exists('set_toolbar')) {

    /**
     * สร้างปุ่มบน toolbar ในรูปแบบของ html
     * @param string $grid_name <p>
     * ชื่อตาราง
     * </p>
     * @param string $multiselect  <p>
     * ประเภทปุ่มที่ต้องการ
     * </p>*
     * @param array $button  <p>
     * ปุ่มที่ต้องการ
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_toolbar($grid_name, $multiselect, $form_label, $button = array(), $url_put = NULL, $parent_id = NULL, $tab_id = NULL, $permission_page = array()) {

        if ($button != '') {
            $form_data = "$('#t_grid{$grid_name}').append( \"";
            foreach ($button as $params) {

                $red = 'false';
                $onTop = 'true';
                $onGrid = 'true';
                $selEnable = 'true';
                $line = 'false';
                $clickFunction = '';
                $label = '';
                $loadURL = '';
                $send_id = 'true';
                $num_form = 0;
                $select_id = 'select_id';
                $set_param = '';
                $after_success = '';
                $select_field = '';
                $permission = '';
                $cellattr = '';
                $show_tab_name = TRUE;
                $script_before_submit = '';
                $url = '';
                $success_function = '';
                $checkOpenForm = TRUE;
                $icon = '';

                if ($params['name'] == 'ADD' || $params['clickFunction'] == 'ADD') {
                    $send_id = 'false';
                }

                foreach ($params as $param => $paramval) {
                    $$param = $paramval;
                }

                if ($clickFunction == '') {
                    $clickFunction = $name;
                }

                if (empty($permission)) {
                    $permission = $clickFunction;
                }

                if (check_click_permission($permission, $permission_page)) {

                    $bt_class = ($onTop != 'true') ? "class ='hide' " : '';

                    $tmp_red = ".addClass('ui-state-error').hover(function(){
                                                           $(this).removeClass('ui-state-error').addClass('ui-state-hover');
                                                      }).mouseout(function(){
                                                            $(this).addClass('ui-state-error');
                                                      })";

                    $btn_id = 'BTN_' . $name . $grid_name;
                    $temp_name = trim($name);
                    //$name = ($label != '') ? $label : $name;
                    if ($label != '') {
                        $btn_name = $label;
                    } else {
                        $btn_name = label('BTN_' . $name);
                    }
                    $btn_name = ($btn_name == '') ? $name : $btn_name;
                    if ($btn_name == $name) {
                        $btn_name = label($name);
                    }
                    $form_data .="<button id='{$btn_id}' {$bt_class} >{$btn_name}</button>";
                    $form_data .= ( $line == 'true' ? ' | ' : '');

                    $form_script .="$('#{$btn_id}').button({
                                                           icons: {primary: '{$icon}'}
                                                      })";
                    if ($red == 'true') {
                        $form_script .=$tmp_red;
                    }

                    $form_script .=";";

                    if (!$parent_id) {
                        $parent_id = 'null';
                    }

                    $set_param = ($set_param != '') ? $set_param . '/' : '';

                    switch ($clickFunction) {
                        case 'ADD' :
                            $bt_label = ($label != '') ? lang('BTN_' . $label) : lang('BTN_ADD');
                            $form_function .="
                                            $('#{$btn_id}').click(function() { 

                                                      var st = '#{$tab_id}tab_add';
                                                      var check_select_tab = false;

                                                      if($('div[id*=\"tab_edit\"]').size()>0) {                                                        
                                                           alert('" . label('MSG_CANNOT_SAVE_TAB') . "');
                                                           check_select_tab = false;
                                                           $('div[id*=\"tab_edit\"]').each(function(){
                                                                if(check_select_tab === false){                                                                    
                                                                    select_tabs($(this));
                                                                    check_select_tab = true; 
                                                                }
                                                           });
                                                      }else if($('div[id*=\"tab_add\"]').size() >0 ) {
                                                           alert('" . label('MSG_CANNOT_SAVE_TAB') . "');
                                                           check_select_tab = false;
                                                           $('div[id*=\"tab_add\"]').each(function(){
                                                                if(check_select_tab === false){ 
                                                                    select_tabs($(this));
                                                                    check_select_tab = true;
                                                                }
                                                           });
                                                      } else {
                                                                $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                var select_id = " . ($send_id == 'false' ? 'null' : "$('#grid{$grid_name}').jqGrid('getGridParam','selrow')") . ";
                                                                var parent_id = $('#parent_id{$grid_name}').val();

                                                                $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',st,'{$bt_label}');
                                                                $(st,'#{$tab_id}tabs').load('{$url_put}/get_form/{$set_param}add/'+select_id+'/'+parent_id+'/{$grid_name}/{$tab_id}', function(){ $.unblockUI();});
                                                       }

                                                 return false;
                                            });

                                       ";
                            break;
                        case 'EDIT' :
                            //echo $label;
                            //$bt_label = ($label != '') ? label('BTN_' . $label) : (label('BTN_' . $name) != '') ? label('BTN_' . $name) : label('BTN_EDIT');
                            if ($label != '') {
                                $bt_label = label('BTN_' . $label);
                            } else if (label('BTN_' . $name) != '') {
                                $bt_label = label('BTN_' . $name);
                            } else {
                                $bt_label = label('BTN_EDIT');
                            }
                            $form_function.=" 
                                            $('#{$btn_id}').click(function() {
                                                      var st = '#{$tab_id}tab_edit';
                                                      var check_select_tab = false;
                                                      if($('div[id*=\"tab_add\"]').size() >0) {
                                                           alert('" . label('MSG_CANNOT_SAVE_TAB') . "');
                                                           check_select_tab = false;
                                                           $('div[id*=\"tab_add\"]').each(function(){
                                                                if(check_select_tab === false){
                                                                    select_tabs($(this));
                                                                    check_select_tab = true;
                                                                }
                                                           });
//                                                      }else if($('div[id*=\"tab_edit\"]').size() >0 ) {
//                                                           alert('" . label('MSG_CANNOT_SAVE_TAB') . "');
//                                                           check_select_tab = false;
//                                                           $('div[id*=\"tab_edit\"]').each(function(){
//                                                                if(check_select_tab === false){
//                                                                    select_tabs($(this));
//                                                                    check_select_tab = true; 
//                                                                }
//                                                           });
                                                      }else{
                                       ";
                            if ($multiselect != 'false') {
                                $form_function .="
                                                                          if(bt){
                                                                               var select_id = Array();
                                                                               select_id[0]=bt;
                                                                               bt=0;
                                                                          }else{
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');
                                                                          }
                                                                          $.each(select_id, function(i) {

                                                                                    st +=select_id[i];
                                                                                    if($(st).size() >0 ) {
                                                                                          $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',st);
                                                                                    }else{
                                                                                          $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                                          var ret = $('#grid{$grid_name}').jqGrid('getRowData',select_id[i]);
    ";
                                $show_label = '';
                                $temp_label = explode(',', $form_label);
                                foreach ($temp_label as $label) {
                                    $label = trim($label);
                                    $show_label.= ( empty($show_label) ? '' : "+' '+") . 'ret.' . build_select_field($label);
                                }
                                $form_function .="
                                                                                         var parent_id = $('#parent_id{$grid_name}').val();
                                                                                         $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',st,'{$bt_label} ['+" . $show_label . "+']');
                                                                                         $(st,'#{$tab_id}tabs').load('{$url_put}/get_form/{$set_param}edit/'+select_id[i]+'/'+parent_id+'/{$grid_name}/{$tab_id}', function(){ $.unblockUI();});
                                                                                   }

                                                                          });

                                                 ";
                            } else {
                                $form_function .="
                                                                          if(bt){
                                                                               select_id=bt;
                                                                               bt=0;
                                                                          }else{
                                                                                var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                                          }

                                                                          //st +=select_id;
                                                                          if($(st).size() >0 ) {
                                                                                alert('" . label('MSG_CANNOT_SAVE_TAB') . "');
                                                                                $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',st);
                                                                          }else{
                                                                                $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                                var ret = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
    ";
                                $show_label = '';
                                $temp_label = explode(',', $form_label);
                                foreach ($temp_label as $label) {
                                    $label = trim($label);
                                    $show_label.= ( empty($show_label) ? '' : "+' '+") . 'ret.' . build_select_field($label);
                                }
                                $form_function .="
                                                                               var parent_id = $('#parent_id{$grid_name}').val();
                                                                               $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',st,'{$bt_label} ['+" . $show_label . "+']');
                                                                               $(st,'#{$tab_id}tabs').load('{$url_put}/get_form/{$set_param}edit/'+" . (!empty($select_field) ? ($select_field == 'parent_id' ? 'parent_id' : 'ret.' . $select_field) : 'select_id') . "+'/'+parent_id+'/{$grid_name}/{$tab_id}', function(){ $.unblockUI();});
                                                                         }

                                                 ";
                            }

                            $form_function .="
                                                                  }

                                                           return false;
                                                      });
                                       ";

                            break;
                        case 'COPY' :
                            $form_function.="
                                            $(\"#{$btn_id}\").click(function() {
                                                      var st = '#{$tab_id}tab_edit';
                                                      if($('div[id*=\"{$tab_id}tab_add\"]').size() >0) {
                                                           alert('" . lang('MSG_CANNOT_SAVE_TAB') . "');
                                                      }else{
                                       ";
                            if ($multiselect != 'false') {
                                $form_function .="
                                                                          if(bt){
                                                                               var select_id = Array();
                                                                               select_id[0]=bt;
                                                                               bt=0;
                                                                          }else{
                                                                               var select_id = $(\"#grid{$grid_name}\").jqGrid('getGridParam','selarrrow');
                                                                          }
                                                                          $.each(select_id, function(i) {

                                                                                    st +=select_id[i];
                                                                                    if($(st).size() >0 ) {
                                                                                          $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',st);
                                                                                    }else{
                                                                                          $.blockUI({ message: '<br/>" . lang(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                                          var ret = $(\"#grid{$grid_name}\").jqGrid('getRowData',select_id[i]);
    ";
                                $show_label = '';
                                $temp_label = explode(',', $form_label);
                                foreach ($temp_label as $label) {
                                    $label = trim($label);
                                    $show_label.= ( empty($show_label) ? '' : "+' '+") . 'ret.' . build_select_field($label);
                                }
                                $form_function .="
                                                                                         var parent_id = $('#parent_id{$grid_name}').val();
                                                                                         $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',st,'" . lang('BTN_EDIT') . " ['+" . $show_label . "+']');
                                                                                         $(st,\"#{$tab_id}tabs\").load('{$url_put}/get_form/copy/'+select_id[i]+'/'+parent_id+'/{$grid_name}/{$tab_id}', function(){ $.unblockUI();});
                                                                                   }

                                                                          });

                                                 ";
                            } else {
                                $form_function .="
                                                                          if(bt){
                                                                               select_id=bt;
                                                                               bt=0;
                                                                          }else{
                                                                                var select_id = $(\"#grid{$grid_name}\").jqGrid('getGridParam','selrow');
                                                                          }

                                                                          st +=select_id;
                                                                          if($(st).size() >0 ) {
                                                                                $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',st);
                                                                          }else{
                                                                                $.blockUI({ message: '<br/>" . lang(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                                var ret = $(\"#grid{$grid_name}\").jqGrid('getRowData',select_id);
    ";
                                $show_label = '';
                                $temp_label = explode(',', $form_label);
                                foreach ($temp_label as $label) {
                                    $label = trim($label);
                                    $show_label.= ( empty($show_label) ? '' : "+' '+") . 'ret.' . build_select_field($label);
                                }
                                $form_function .="
                                                                               var parent_id = $('#parent_id{$grid_name}').val();
                                                                               $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',st,'" . lang('BTN_EDIT') . " ['+" . $show_label . "+']');
                                                                               $(st,\"#{$tab_id}tabs\").load('{$url_put}/get_form/{$set_param}edit/'+select_id+'/'+parent_id+'/{$grid_name}/{$tab_id}', function(){ $.unblockUI();});
                                                                         }

                                                 ";
                            }

                            $form_function .="
                                                                  }

                                                           return false;
                                                      });
                                       ";

                            break;

                        case 'EXPORT' :
                            $form_function.="
                                            $('#{$btn_id}').click(function() {
                                                
                                                var url = '{$url_put}/export_data/{$parent_id}';
                                                if($('#form_search{$grid_name}').is('form')){
                                                    var tabletogrid_id = '';
                                                    var tabletogrid_value = '';

                                                    $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                        tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                        tabletogrid_value = $('#table_to_grid_'+tabletogrid_id).jqGrid('getGridParam','selarrrow').join(',');
                                                        $('#span_'+tabletogrid_id).html('');
                                                        $('<input/>').attr({type: 'hidden',id: tabletogrid_id+'{$grid_name}',name: tabletogrid_id, value: tabletogrid_value}).appendTo('#span_'+tabletogrid_id);
                                                    });     

                                                    document.form_search{$grid_name}.action = url;
                                                    document.form_search{$grid_name}.target = '_blank';
                                                    document.form_search{$grid_name}.method = 'POST';
                                                    document.form_search{$grid_name}.submit();
                                                    $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                        tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                        $('#span_'+tabletogrid_id).html('');
                                                    });
                                                }else{
                                                    window.open(url);
                                                }
                                                
                                                return false;
                                                 
                                            });
                                       ";

                            break;

                        case 'NEW_PAGE' :
                            $form_function.="
                                            $('#{$btn_id}').click(function() {
                                                      var st='';

                                                      var tabletogrid_id = '';
                                                      var tabletogrid_value = '';

                                                      $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                          tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                          tabletogrid_value = $('#table_to_grid_'+tabletogrid_id).jqGrid('getGridParam','selarrrow').join(',');
                                                          $('#span_'+tabletogrid_id).html('');
                                                          $('<input/>').attr({type: 'hidden',id: tabletogrid_id+'{$grid_name}',name: tabletogrid_id, value: tabletogrid_value}).appendTo('#span_'+tabletogrid_id);
                                                      });

                                            ";
                            if ($multiselect != 'false') {
                                $form_function .="

                                                                          if(bt){
                                                                               var select_id = Array();
                                                                               select_id[0]=bt;
                                                                               bt=0;";

                                if (!empty($select_field)) {

                                    $form_function .="
                                                                               var data = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                                                if(data.{$select_field}!=''){
                                                                                     select_id[0] = data.{$select_field};
                                                                                }";
                                }
                                $form_function .="

                                                                          }else{
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');

                                                                          }
                                                                          $.each(select_id, function(i) {
                                                                                st +=select_id[i]+',';
                                                                          });


                                                 ";
                            } else {
                                $form_function .="
                                                                          if(bt){
                                                                               select_id=bt;
                                                                               bt=0;";

                                if (!empty($select_field)) {

                                    $form_function .="                               var data = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                                                if(data.{$select_field}!=''){
                                                                                     select_id = data.{$select_field};
                                                                                }";
                                }
                                $form_function .="
                                                                          }else{
                                                                                var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                                          }
                                                                          st +=select_id;

                                                 ";
                            }

                            if ($submitForm == '') {
                                $submitForm = 'form_search';
                            }
                            $form_function .="
                                                           var arr_st = st.split(',');
                                                           st = '';
                                                           arr_st.forEach(function(entry) {
                                                                if(entry !== ''){
                                                                    if(st !== ''){
                                                                        st+=',';
                                                                    }
                                                                    st+=entry;
                                                                }
                                                            }); 
                                                           $('#{$submitForm}{$grid_name} #{$select_id}').val(st); 
                                                           if(st.indexOf(',') > -1){
                                                                st = '';
                                                           }    
                                                           document.{$submitForm}{$grid_name}.action = base_url+'{$url}'+st;
                                                           document.{$submitForm}{$grid_name}.target = '_blank';
                                                           document.{$submitForm}{$grid_name}.method = 'POST';
                                                           document.{$submitForm}{$grid_name}.submit();
                                                           $('#{$submitForm}{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                               tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                               $('#span_'+tabletogrid_id).html('');
                                                           });
                                                           return false;
                                                      });
                                       ";

                            break;

                        //start new page select
                        case 'NEW_PAGE_SELECTED' :
                            $form_function.="   //alert($select_field);
                                            $('#{$btn_id}').click(function() {
                                                      var st='';

                                                      var tabletogrid_id = '';
                                                      var tabletogrid_value = '';

                                                      $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                          tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                          tabletogrid_value = $('#table_to_grid_'+tabletogrid_id).jqGrid('getGridParam','selarrrow').join(',');
                                                          $('#span_'+tabletogrid_id).html('');
                                                          $('<input/>').attr({type: 'hidden',id: tabletogrid_id+'{$grid_name}',name: tabletogrid_id, value: tabletogrid_value}).appendTo('#span_'+tabletogrid_id);
                                                      });

                                            ";
                            if ($multiselect != 'false') {
                                $form_function .="  

                                                                          if(bt){
                                                                               var select_id = Array();
                                                                               select_id[0]=bt;
                                                                               bt=0;";

                                if (!empty($select_field)) {

                                    $form_function .="
                                                                               var data = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                                                if(data.{$select_field}!=''){
                                                                                     select_id[0] = data.{$select_field};
                                                                                         alert(select_id[0]);
                                                                                }";
                                }
                                $form_function .="

                                                                          }else{
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');

                                                                          }
                                                                          $.each(select_id, function(i) {
                                                                                st +=select_id[i]+',';
                                                                          });


                                                 ";
                            } else {
                                $form_function .="
                                                                          if(bt){
                                                                               select_id=bt;
                                                                               bt=0;";

                                if (!empty($select_field)) {

                                    $form_function .="                               var data = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                                                if(data.{$select_field}!=''){
                                                                                     select_id = data.{$select_field};
                                                                                }";
                                }
                                $form_function .="
                                                                          }else{
                                                                                var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                                          }
                                                                          st +=select_id;

                                                 ";
                            }

                            if ($submitForm == '') {
                                $submitForm = 'form_search';
                            }
                            $form_function .="
                                                           $('#{$submitForm}{$grid_name} #{$select_id}').val(st);
                                                           document.{$submitForm}{$grid_name}.action = base_url+'{$url}'+st;
                                                           document.{$submitForm}{$grid_name}.target = '_blank';
                                                           document.{$submitForm}{$grid_name}.method = 'POST';
                                                           document.{$submitForm}{$grid_name}.submit();
                                                           $('#{$submitForm}{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                               tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                               $('#span_'+tabletogrid_id).html('');
                                                           });
                                                           return false;
                                                      });
                                                       
                                       ";

                            break;
                        //end new page select
                        case 'NEW_PAGE_ALL' :
                            $form_function.="
                                            $('#{$btn_id}').click(function() {
                                                      var st='';

                                                      var tabletogrid_id = '';
                                                      var tabletogrid_value = '';

                                                      $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                          tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                          tabletogrid_value = $('#table_to_grid_'+tabletogrid_id).jqGrid('getGridParam','selarrrow').join(',');
                                                          $('#span_'+tabletogrid_id).html('');
                                                          $('<input/>').attr({type: 'hidden',id: tabletogrid_id+'{$grid_name}',name: tabletogrid_id, value: tabletogrid_value}).appendTo('#span_'+tabletogrid_id);
                                                      });
                                            ";

                            $form_function .="
                                                           $('#{$select_id}').val(st);
                                                           document.forms[{$num_form}].action = base_url+'{$url}';
                                                           document.forms[{$num_form}].target = '_blank';
                                                           document.forms[{$num_form}].method = 'POST';
                                                           document.forms[{$num_form}].submit();
                                                           $('#form_search{$grid_name} table[id^=table_to_grid_]').each(function(){
                                                               tabletogrid_id = $(this).attr('id').replace('table_to_grid_', '');
                                                               $('#span_'+tabletogrid_id).html('');
                                                           });
                                                           return false;
                                                      });
                                       ";

                            break;

                        case 'DELETE' :
                            $form_function .="
                                              $('#{$btn_id}').click(function() {
                                                                     if(bt){
                                                                          sended=bt;
                                                                          bt=0;
                                                                     }else{
                                                           ";
                            if ($multiselect != 'false' || $multiselect === true) {
                                $form_function .="
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');
                                                                               sended = select_id.join(',');
                                                                               while(sended.charAt(0) == ',')
                                                                                    sended = sended.substr(1);

                                                      ";
                            } else {
                                $form_function .="
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                                               sended = select_id;
                                                      ";
                            }
                            $form_function .="

                                                                     }

                                                                     $('#dialog:ui-dialog').dialog('destroy');

                                                                     $('#dialog_confirm{$grid_name}').dialog({
                                                                          resizable: false,
                                                                          modal: true,
                                                                          buttons: {
                                                                               '" . lang('BTN_DELETE_ALL') . "': function() {
                                                                                   $(this).dialog('close');
                                                                                   $.blockUI({ message: '<br/>" . lang(MSG_PROCESSING) . "...<br/>" . image_asset('loading.gif') . " <br/><br/>' });
                                                                                   $.ajax({
                                                                                       type: 'POST',
                                                                                       url: '{$url_put}/set_data/{$set_param}delete/{$parent_id}',
                                                                                       data : 'selected='+sended ,
                                                                                       dataType : 'json',
                                                                                       error: function(response, textStatus, errorThrown){ 
                                                                                            $.unblockUI({
                                                                                                onUnblock: function(){ 
                                                                                                    switch (textStatus) {
                                                                                                        case 'error':
                                                                                                        case 'timeout':                                                                                                        
                                                                                                            alert(response.status +' '+ errorThrown);
                                                                                                            break; 
                                                                                                        case 'parsererror':
                                                                                                            //alert(strip_tags(response.responseText));
                                                                                                            alert(response.status +' Parser error');
                                                                                                            break; 
                                                                                                        default: 
                                                                                                            console.log('response=',response);
                                                                                                            console.log('textStatus=',textStatus);
                                                                                                            console.log('errorThrown=',errorThrown);
                                                                                                            break;
                                                                                                    } 
                                                                                                }
                                                                                            });
                                                                                       },
                                                                                       success: function(response){
                                                                                            var check_validate = true;
                                                                                            var type_validate = $.type(response);
                                                                                            if(type_validate === 'boolean'){
                                                                                                check_validate = response;
                                                                                            } 
                                                                                            
                                                                                            $.unblockUI({
                                                                                                onUnblock: function(){
                                                                                                    if(type_validate === 'object'){
                                                                                                        check_validate = response.PASS;
                                                                                                        if($.type(response.MSG) === 'string'){
                                                                                                            if(response.MSG != ''){
                                                                                                                alert(response.MSG);
                                                                                                            }
                                                                                                        }
                                                                                                        if($.type(response.SCRIPT) === 'string'){
                                                                                                            if(response.SCRIPT != ''){
                                                                                                                eval(response.SCRIPT);
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                }
                                                                                            });
                                                                                            {$after_success}
                                                                                            
                                                                                            if(check_validate === true){
                                                                                                $('#grid{$grid_name}').trigger('reloadGrid'); 
                                                                                            }
                                                                                        }
                                                                                    });
                                                                               },
                                                                               '" . lang('BTN_CANCEL') . "': function() {
                                                                                    $(this).dialog('close');
                                                                               }
                                                                          }
                                                                     });


                                                                return false;
                                                           });";

                            break;

                        case 'CONFIRM' :

                            if (!empty($append)) {
                                $append = "if(response.responseText!==''){ $('{$append}{$grid_name}').append(response.responseText);}";
                            }

                            $form_function .="
                                              $('#{$btn_id}').click(function() {
                                                  
                                                                     if(bt){
                                                                          sended=bt;
                                                                          bt=0;
                                                                     }else{
                                                           ";
                            if ($multiselect != 'false') {
                                $form_function .="
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');
                                                                               sended = select_id.join(',');
                                                                               while(sended.charAt(0) == ',')
                                                                                    sended = sended.substr(1);
                                                      ";
                            } else {
                                $form_function .="
                                                                               var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                                               sended = select_id;
                                                      ";
                            }

                            $form_function .="

                                                                     }

                                                                     $('#dialog:ui-dialog').dialog('destroy');
                                                                     var data = 'selected='+sended;
                                                                     var other_data = '';
                                                                        {$script_before_submit}
                                                                     if(other_data !== ''){
                                                                        data = other_data;
                                                                    }
                                                                     $('#dialog_{$temp_name}{$grid_name}').dialog({
                                                                          resizable: false,
                                                                          modal: true,
                                                                          buttons: {
                                                                               '" . (lang('BTN_' . $temp_name . '_ALL') == '' ? 'BTN_' . $temp_name . '_ALL' : lang('BTN_' . $temp_name . '_ALL')) . "': function() {
                                                                               $( this ).dialog('close');
                                                                                     $.blockUI({ message: '<br/>" . lang(MSG_PROCESSING) . "...<br/>" . image_asset('loading.gif') . " <br/><br/>' });

                                                                               $.ajax({
                                                                                                   type: 'POST',
                                                                                                   url: '{$url_put}/set_data/{$set_param}{$action}/{$parent_id}',
                                                                                                   data : data,
                                                                                                   dataType : 'json',
                                                                                                   success: function(response){ 
                                                                                                        $.unblockUI({
                                                                                                            onUnblock: function(){
                                                                                                                if($.type(response.MSG) === 'string'){
                                                                                                                    if(response.MSG != ''){
                                                                                                                        alert(response.MSG);
                                                                                                                    }
                                                                                                                }
                                                                                                                if($.type(response.SCRIPT) === 'string'){
                                                                                                                    if(response.SCRIPT != ''){
                                                                                                                        eval(response.SCRIPT);
                                                                                                                    }
                                                                                                                }
                                                                                                            }
                                                                                                        });
                                                                                                        
                                                                                                        if(response.PASS===true){
                                                                                                            $('#grid{$grid_name}').trigger('reloadGrid');
                                                                                                        } 
                                                                                                    },
                                                                                                    error : function(response){
                                                                                                        $.unblockUI({
                                                                                                            onUnblock: function(){ 
                                                                                                                if($.type(response) === 'object'){
                                                                                                                    if($.type(response.responseText) === 'string'){                                                                                                                         
                                                                                                                        {$success_function}
                                                                                                                        {$append}
                                                                                                                    }
                                                                                                                } 
                                                                                                            }
                                                                                                        });
                                                                                                    }

                                                                                  });
                                                                               },
                                                                               '" . lang('BTN_CANCEL') . "': function() {
                                                                                    $( this ).dialog('close');
                                                                               }
                                                                          }
                                                                     });


                                                                return false;
                                                           });";

                            break;

                        case 'NEW_TAB' :
                            $tab_label = '';
                            if ($selEnable == 'false' && $onGrid == 'false') {
                                $send_id = 'false';
                                $show_tab_name = FALSE;
                            }

                            if ($checkOpenForm !== TRUE) {
                                $checkOpenForm = 'false';
                            } else {
                                $checkOpenForm = 'true';
                            }

                            $tab_name = "{$tab_id}TAB_" . $name;
                            $form_function .="

                                                 $('#{$btn_id}').click(function() {

                                                           var st = '#{$tab_name}';
                                                           var check_select_tab = false;
                                                           var checkOpenForm = {$checkOpenForm};
                                                           
                                                           if($('div[id*=\"tab_add\"]').size() >0 && checkOpenForm == true) {
                                                                alert('" . label('MSG_CANNOT_SAVE_TAB') . "'); 
                                                                check_select_tab = false;
                                                                $('div[id*=\"tab_add\"]').each(function(){
                                                                     if(check_select_tab === false){
                                                                         select_tabs($(this));
                                                                         check_select_tab = true;
                                                                     }
                                                                });
                                                           }else if($('div[id*=\"tab_edit\"]').size() >0 && checkOpenForm == true) {
                                                                alert('" . label('MSG_CANNOT_SAVE_TAB') . "'); 
                                                                check_select_tab = false;
                                                                $('div[id*=\"tab_edit\"]').each(function(){
                                                                     if(check_select_tab === false){
                                                                         select_tabs($(this));
                                                                         check_select_tab = true;
                                                                     }
                                                                });
                                                           }else{
                                                                var parent_id = $('#parent_id{$grid_name}').val();
                                             ";
                            $show_label = '';

                            $temp_label = explode(',', $form_label);
                            foreach ($temp_label as $label) {
                                $label = trim($label);
                                $show_label.= ( empty($show_label) ? '' : "+' '+") . 'ret.' . build_select_field($label);
                            }

                            if ($send_id == 'true' && $show_tab_name === TRUE) {
                                $tab_label = ($form_label != '') ? "+ ' ['+" . $show_label . "+']' " : '';
                            }

                            $sparam = ($send_param != '') ? "{$send_param}/" : '';
                            if ($btn_name == '') {
                                $bt_label = lang('BTN_' . $name);
                                if (empty($bt_label)) {
                                    $bt_label = lang('GRID_' . $name);
                                }
                                if (empty($bt_label)) {
                                    $bt_label = $name;
                                }
                            } else {
                                $bt_label = $btn_name;
                            }
                            $url = !empty($url) ? $url : $loadURL;
                            if ($multiselect != 'false') {
                                if ($selEnable != 'false') {
                                    $form_function .="


                                                        if(bt){
                                                             var select_id = Array();
                                                             select_id[0]=bt;
                                                             bt=0;
                                                        }else{
                                                             var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selarrrow');
                                                        }

                                                        $.each(select_id, function(i) {
                                                                  tname=st;
                                                                  tname += '-' +select_id[i];
                                                                  if($(tname).size() >0) {
                                                                        $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',tname);
                                                                  }else{

                                                                       $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                                        var ret = $('#grid{$grid_name}').jqGrid('getRowData',select_id[i]);
                                                                        $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',tname,'{$bt_label}' {$tab_label} );
                                                                        $(tname,'#{$tab_id}tabs').load(base_url+'{$url}index/{$send_param}'" . ($send_id == 'true' ? '+' . (!empty($select_field) ? ($select_field == 'parent_id' ? 'parent_id' : 'ret.' . $select_field) : 'select_id[i]') : '') . "{$sparam}, function(){ $.unblockUI();});
                                                                 }
                                                        });

                                                 ";
                                } else {
                                    //$send_id = 'false';
                                    $form_function .="

                                                        if(bt){
                                                             select_id=bt;
                                                             bt=0;
                                                        }else{
                                                             var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                        }

                                                        tname=st+'{$grid_name}'; 
                                                        if(($(tname).size() >0) && !confirm('" . label(MSG_CONFIRM_LOAD_TAB) . "')) { 
                                                            $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',tname);
                                                        }else{
                                                            if($(tname).size() >0){
                                                                var length_tab = $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('length')-1; 
                                                                var selected = $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('option', 'selected');                                                                    
                                                                for (var i = length_tab; i > selected; i--) {
                                                                    $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('remove', i);
                                                                } 
                                                            } 
                                                            $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                            var ret = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                            $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',tname,'{$bt_label}' {$tab_label} );
                                                            $(tname,'#{$tab_id}tabs').load(base_url+'{$url}index/{$sparam}'" . ($send_id == 'true' ? '+' . (!empty($select_field) ? ($select_field == 'parent_id' ? 'parent_id' : 'ret.' . $select_field) : 'select_id') : '') . ", function(){ $.unblockUI();});
                                                        }

                                                 ";
                                }
                            } else {
                                $form_function .="

                                                        if(bt){
                                                             select_id=bt;
                                                             bt=0;
                                                        }else{
                                                             var select_id = $('#grid{$grid_name}').jqGrid('getGridParam','selrow');
                                                        }

                                                        tname=st+'{$grid_name}';
                                                        //tname += '-' +select_id;
                                                        if(($(tname).size() >0) && !confirm('" . label(MSG_CONFIRM_LOAD_TAB) . "')) {
                                                            //alert('" . label('MSG_CLOSE_TAB_BEFORE_SELECT') . "');
                                                            $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',tname);
                                                        }else{
                                                            if($(tname).size() >0){
                                                                var length_tab = $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('length')-1; 
                                                                var selected = $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('option', 'selected');                                                                    
                                                                for (var i = length_tab; i > selected; i--) {
                                                                    $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('remove', i);
                                                                }
                                                                
                                                                //$('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('remove',tname);
                                                            }
                                                            //$('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('select',tname);
                                                            $.blockUI({ message: '<br/>" . label(MSG_PROCESSING) . "<br/>" . image_asset('loading.gif') . "<br/><br/>' });
                                                            var ret = $('#grid{$grid_name}').jqGrid('getRowData',select_id);
                                                            $('#{$tab_id}tabs','#{$tab_id}main_tab').tabs('add',tname,'{$bt_label}' {$tab_label} );
                                                            $(tname,'#{$tab_id}tabs').load(base_url+'{$url}index/{$sparam}'" . ($send_id == 'true' ? '+' . (!empty($select_field) ? ($select_field == 'parent_id' ? 'parent_id' : 'ret.' . $select_field) : 'select_id') : '') . ", function(){ $.unblockUI();});
                                                        }

                                                 ";
                            }

                            $form_function.= "

                                                             }

                                                      return false;
                                                 });";

                            break;

                        case 'LINK' :

                            $form_function.="
                                            $('#{$btn_id}').click(function() {
                                                 window.open(base_url+'{$url}');
                                                 return false;
                                            });
                                       ";

                            break;

                        default :
                            //todo callback function ->clickFunction(grid_id)
                            $form_function .="
                                                 $('#{$btn_id}').click(function(){

                                                      {$clickFunction}('{$grid_name}');

                                                           return false;

                                                 });

                                            ";
                            break;
                    }
                }
            }

            $form_data .= "\");";

            $form_script = trim($form_script);
            $form_function = trim($form_function);

            if (empty($form_script) && empty($form_function)) {
                $form_data = '';
            } else {
                $form_data .= $form_script;
                $form_data .= $form_function;
            }
        }

        return $form_data;
    }

}

if (!function_exists('set_grid_button')) {

    /**
     * สร้างปุ่มบนตารางในรูปแบบของ html
     * @param string $grid_name <p>
     * ชื่อตาราง
     * </p>
     * @param array $button  <p>
     * ปุ่มที่ต้องการ
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_grid_button($grid_name, $button = array(), $permission_page = array()) {
        if (!empty($button)) {
            $form_data = "  var ids =$('#grid{$grid_name}').jqGrid('getDataIDs');

                                                  for(var i=0;i < ids.length;i++){
                                                       var cl = ids[i];
               ";

            foreach ($button as $params) {

                $red = 'false';
                $onTop = 'true';
                $onGrid = 'true';
                $selEnable = 'true';
                $line = 'false';
                $permission = '';
                $select_field = '';
                $show_label = FALSE;
                $icon = '';

                foreach ($params as $param => $paramval) {
                    $$param = $paramval;
                }

                if (empty($permission)) {
                    $permission = $name;
                }

                if (check_click_permission($permission, $permission_page)) {
                    if ($onGrid == 'true') {
                        $tmp_red = ".addClass('ui-state-error').hover(function(){
                                                           $(this).removeClass('ui-state-error').addClass('ui-state-hover');
                                                      }).mouseout(function(){
                                                            $(this).addClass('ui-state-error');
                                                      })";

                        $btn = 'GRID_' . $name;
                        $btn_id = 'GRID_' . $name . $grid_name;
                        $btn_call = 'BTN_' . $name . $grid_name;
                        $btn_var = 'bt' . $name;
                        $btn_name = ($label == '') ? lang('BTN_' . $name) : '';
                        if ($btn_name == '') {
                            $btn_name = ($label != '') ? lang('GRID_' . $label) : lang('GRID_' . $name);
                        }
                        $btn_name = ($btn_name == '') ? $name : $btn_name;
                        $form_data .= "{$btn_var} ='<button class=\"{$btn_id}\" onClick=\"grid_icon_click{$grid_name}(\''+cl+'\',\'{$btn_call}\');return false;\">{$btn_name}</button>';
                                                             if(ids[i]!='add') $(this).jqGrid('setRowData',ids[i],{{$btn}:{$btn_var}})
                             ";

                        if ($show_label === TRUE) {
                            $show_label = 'true';
                        } else {
                            $show_label = 'false';
                        }

                        $form_script .="$('.{$btn_id}').button({
                                                           icons: {primary: '{$icon}'},text: {$show_label}
                                                      })";
                        if ($red == 'true') {
                            $form_script .=$tmp_red;
                        }

                        $form_script .=";";
                    }
                }
            }

            $form_data .= "}";

            $form_data .= $form_script;
        }

        return $form_data;
    }

}

if (!function_exists('set_button_space')) {

    /**
     * สร้างพื้นที่ของปุ่มบนตารางในรูปแบบของ html
     * @param string $grid_name <p>
     * ชื่อตาราง
     * </p>
     * @param array $button  <p>
     * ปุ่มที่ต้องการ
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_button_space($grid_name, $button = array(), $permission_page = array()) {

        $return = array(
            'BTN' => '',
            'SHOW_TOOLBAR' => FALSE
        );
        if (!empty($button)) {
            foreach ($button as $params) {

                $red = 'false';
                $onTop = 'true';
                $onGrid = 'true';
                $selEnable = 'true';
                $line = 'false';
                $colWidth = 30;
                $label = '';
                $permission = '';
                $cellattr = '';
                $select_field = '';
                $name = '';

                foreach ($params as $param => $paramval) {
                    $$param = $paramval;
                }
                if ($onTop == 'true') {
                    $return['SHOW_TOOLBAR'] = TRUE;
                }

                if (empty($permission)) {
                    $permission = $name;
                }

                if (check_click_permission($permission, $permission_page)) {
                    if ($onGrid == 'true') {
                        $tmp_red = ".addClass('ui-state-error').hover(function(){
                                                           $(this).removeClass('ui-state-error').addClass('ui-state-hover');
                                                      }).mouseout(function(){
                                                            $(this).addClass('ui-state-error');
                                                      })";

                        $btn = 'GRID_' . $name;

                        $btn_id = 'GRID_' . $name . $grid_name;
                        $btn_call = 'BTN_' . $name . $grid_name;
                        $btn_var = 'bt' . $name;
                        $btn_name = ($label != '') ? lang('GRID_' . $label) : lang('GRID_' . $name);
                        $btn_name = ($btn_name != '') ? $btn_name : lang('GRID_' . $name);
                        $btn_name = ($label != '' && $btn_name == '') ? $label : $btn_name;
                        $btn_name = str_replace("'", "", $btn_name);
                        $cellattr = (!empty($cellattr) ? ',cellattr:' : '') . $cellattr;
                        $form_data .= ",{name:'{$btn}',index:'{$btn}',label:'{$btn_name}', align:'center' ,width:{$colWidth},sortable:false,viewable:false{$cellattr}}";
                    }
                }
            }
            $return['BTN'] = $form_data;
        }
        return $return;
    }

}

if (!function_exists('set_button_status')) {

    /**
     * ปรับสถานะของปุ่มที่เลือกบน toolbar
     * @param string $grid_name <p>
     * ชื่อตาราง
     * </p>
     * @param array $button  <p>
     * ปุ่มที่ต้องการ
     * </p>
     * @param string $status  <p>
     * สถานะ enable หรือ disable
     * </p>
     * @return string
     * ข้อมูล html ของฟอร์มที่ต้องการ
     *
     */
    function set_button_status($grid_name, $button = array(), $status = NULL, $permission_page = array()) {
        if ($button != '') {

            foreach ($button as $params) {
                $selEnable = 'true';
                $permission = '';
                foreach ($params as $param => $paramval) {
                    $$param = $paramval;
                }

                if (empty($permission)) {
                    $permission = $name;
                }

                if (check_click_permission($permission, $permission_page)) {
                    if ($selEnable == 'true') {
                        $btn_id = 'BTN_' . $name . $grid_name;
                        $btn_name = lang('BTN_' . $name);
                        $form_data .=" $(\"#{$btn_id}\").button(\"{$status}\");";
                    }
                }
            }
        }
        return $form_data;
    }

}

if (!function_exists('get_data_model')) {

    /**
     * เลือกข้อมูลจาก model
     * @param string $db_table <p>
     * ชื่อตาราง
     * </p>
     * @param int $value  <p>
     * ค่า id ที่ต้องการ
     * </p>
     * @param string $mode  <p>
     * โหมดการส่งค่า 'array' จะส่งเป็น array
     * </p>*
     * @return string
     * ถ้ามีการส่งค่า id มาจะส่งคืนเป็น text
     * ถ้าไม่มีการส่งค่า id มาจะส่งคืนเป็น array
     *
     */
    function get_data_model($db_table, $value = NULL, $mode = NULL, $filter = array()) {
        $CI = & get_instance();

        if (cache_exists($db_table)) {
            $tmp_dropdown = cache_fetch($db_table);

            if ($mode != 'array' && $mode != 'array_filter') {
                $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                $master = $tmp_dropdown[$value][$language];
            } else {
                $master = $tmp_dropdown;
            }
        } else {

            $dbtable = $CI->config->item($db_table); //echo '<pre>'; print_r($dbtable);

            $db_label = $dbtable['db_label'];
            $db_filter = '';
            if (!empty($dbtable['db_filter'])) {
                $db_filter = $dbtable['db_filter'];
            }
            $check_lang = FALSE;
            if (check_lang($dbtable['db_label'])) {
                $lang_data = suffix_lang($dbtable['db_label']);
                $db_label = implode(',', $lang_data);
                $check_lang = TRUE;
            }

            $db_label.= ( $db_filter != '') ? ',' . $db_filter : '';

            $table = ses_path($dbtable['db_model']);
            $CI->load->model($dbtable['db_model']);

            $check_extension_cache = cache_check_extension();
            if (($dbtable['db_cache'] == 'true' && $check_extension_cache == TRUE) || $mode == 'array' || $mode == 'array_filter') {
                $result = array();
                if (!empty($table)) {
                    if ($filter['PK_ID'] != '') {
                        $CI->adodb->SetFetchMode(1);
                        $result_temp = $CI->$table->select_by_id(array($dbtable['db_id'], $db_label), $filter['PK_ID']);

                        $CI->adodb->SetFetchMode(3);
                        array_push($result, $result_temp);
                    } else {
                        $order_label = $dbtable['db_order'];
                        if (check_lang($dbtable['db_order'])) {
                            $order_label = build_select_field(array($dbtable['db_order']));
                        }
                        $result = $CI->$table->cache_select(array($dbtable['db_id'], $db_label), $order_label, $filter);
                    }
                }
                $lang_data = $CI->config->item('app_lang');
                $db_filter = ($db_filter != '') ? explode(',', $db_filter) : '';

                if (is_array($result) && sizeof($result) > 0) {
                    foreach ($result as $drop_down => $index) {
                        $i = 1;
                        foreach ($lang_data as $item) {
                            $tmp_dropdown[$index[0]][$item] = ($check_lang === FALSE || ($check_lang === TRUE && empty($index[$i])) ? $index[1] : $index[$i]);
                            $i++;
                        }
                        if ($check_lang === FALSE) {
                            --$i;
                        }
                        if (!empty($db_filter) && is_array($db_filter)) {
                            foreach ($db_filter as $field) {
                                $tmp_dropdown[$index[0]]['filter'][$field] = $index[$i];
                                $i++;
                            }
                        }
                    }
                    if ($dbtable['db_cache'] == 'true') {
                        cache_store($db_table, $tmp_dropdown);
                    }
                }

//start for Dropdown array ที่มีการส่งค่า filter มาเลย  Noke

                if ($mode == 'array_filter' || (is_array($filter) && !empty($filter))) {
                    $i = 1;
                    if (!empty($tmp_dropdown) && is_array($tmp_dropdown) && !empty($db_filter) && is_array($db_filter)) {

                        foreach ($tmp_dropdown as $key => $rs) {
                            $add = TRUE;
                            foreach ($db_filter as $field) {
                                if (!empty($value) && $value != 'NULL') {
                                    if ($rs['filter'][$field] != $value) {
                                        $add = FALSE;
                                    }
                                } else if (is_array($filter) && !empty($filter[$field])) {
                                    $temp_filter = explode(',', $filter[$field]);
                                    if (!in_array($rs['filter'][$field], $temp_filter)) {
                                        $add = FALSE;
                                    }
                                } else {
                                    //$add = FALSE;
                                }
                            }
                            if ($add == TRUE) {
                                $new_tmp_dropdown[$key] = $rs;
                                $new_tmp_dropdown[$key]['filter'][$field] = $rs['filter'][$field];
                                $i++;
                            }
                        }
                        $tmp_dropdown = $new_tmp_dropdown;
                    }
                }
                //end Dropdown array 

                if ($mode != 'array' && $mode != 'array_filter') {
                    $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                    $master = $tmp_dropdown[$value][$language];
                } else {
                    $master = $tmp_dropdown;
                }
            } else {
                $CI->adodb->SetFetchMode(1);
                if (!empty($table)) {
                    $tmp_dropdown = $CI->$table->select_by_id(array($dbtable['db_id'], $db_label), $value);

                    $temp_label = explode(',', $db_label);
                    if ((count($temp_label) > 1) && $check_lang === TRUE) {
                        $language = (ses_data('lang') != '') ? strtolower(ses_data('lang')) : $CI->config->item('default_lang');
                        $lang_data = $CI->config->item('app_lang');

                        //var_dump($lang_data);
                        $language = array_search($language, $lang_data);

                        $CI->adodb->SetFetchMode(3);
                        $master = $tmp_dropdown[$language + 1];
                    } else {
                        $master = $tmp_dropdown[1]; //0 = id
                    }
                }
            }
        }
        $CI->adodb->SetFetchMode(2);
        return $master;
    }

}


/*
 * Noke start autocomplete with filter
 *  
 */
if (!function_exists('get_data_autocomplete_filter')) {

    /**
     * เลือกข้อมูลจาก autocomplete
     * @param string $model <p>
     * ชื่อตาราง
     * </p>
     * @param int $id  <p>
     * ค่า id ที่ต้องการ
     * </p>
     * @return Array
     * ค่าของ autocomplete
     *
     */
    function get_data_autocomplete_filter($model, $id = NULL, $filter = array()) {
        $CI = & get_instance();
        $CI->load->model($model);
        $table = ses_path($model);

        if (!empty($filter)) {
            $param_filter = explode('-----', $filter);
        }
        $filter_select = array();
        for ($i = 0; $i < count($param_filter); $i++) {
            $rs_filter = explode(':', $param_filter[$i]);
            $filter_select[trim($rs_filter[0])] = trim($rs_filter[1]);
        }

        $tb_model = $CI->$table->table_name;
        $chk_table = strstr($tb_model, "REF_");
        $check_ref_table = (!empty($chk_table) ? TRUE : FALSE);

        if (cache_exists($tb_model)) {
            $cache = cache_fetch($tb_model);

            //reset($cache);
            $language = strtolower(ses_data('lang'));

            $response = array();
            if ($id) {
                $name = $cache[$id][$language];
                if (!empty($name)) {
                    $title = ($check_ref_table == TRUE ? "{$id} {$name}" : $name);
                    $response[] = array(
                        'title' => $title,
                        'value' => $id
                    );
                }
            } else {
                $data = $CI->input->post('data');

                if (strlen($data) > 1) {
                    $filter['autocomplete_search'] = trim($data);
                    $response = array();
                    if (is_array($cache) && sizeof($cache) > 0) {

                        foreach ($cache as $key => $row) {

                            $data = ($check_ref_table == TRUE ? $key . ' ' : '') . $row[$language];
                            $check = strstr($data, $filter['autocomplete_search']);
                            if ($check) {
                                $response[] = array(
                                    'key' => $key,
                                    'value' => trim($data)
                                );
                            }
                        }
                    }
                }
            }
        } else {
            $CI->adodb->SetFetchMode(2);

            if (is_numeric($id) || empty($id)) {
                $filter_select = array_filter($filter_select);
                $response = $CI->$table->get_autocomplete_filter($id, $filter_select);
            } else {
                $response = json_decode($id);
            }
        }

        return $response;
    }

}
// end autocomplete with filter

if (!function_exists('get_data_autocomplete')) {

    /**
     * เลือกข้อมูลจาก autocomplete
     * @param string $model <p>
     * ชื่อตาราง
     * </p>
     * @param int $id  <p>
     * ค่า id ที่ต้องการ
     * </p>
     * @return Array
     * ค่าของ autocomplete
     *
     */
    function get_data_autocomplete($model, $id = NULL, $filter = array(), $config_table = NULL) {
        $CI = & get_instance();
        $CI->load->model($model);
        $table = ses_path($model);

        if (empty($config_table)) {
            $tb_model = $CI->$table->table_name;
        } else {
            $tb_model = $config_table;
        }
        $chk_table = strstr($tb_model, "REF_");
        $check_ref_table = (!empty($chk_table) ? TRUE : FALSE);

        if (cache_exists($tb_model)) {
            $cache = cache_fetch($tb_model);
            //reset($cache);
            $language = strtolower(ses_data('lang'));

            $response = array();
            if ($id) {
                $name = $cache[$id][$language];
                if (!empty($name)) {
                    $title = ($check_ref_table == TRUE ? "{$id} {$name}" : $name);
                    $response[] = array(
                        'title' => $title,
                        'value' => $id
                    );
                }
            } else {
                $data = $CI->input->post('data');

                if (strlen($data) > 1) {
                    $filter['autocomplete_search'] = trim($data);
                    $response = array();
                    if (is_array($cache) && sizeof($cache) > 0) {

                        foreach ($cache as $key => $row) {

                            $data = ($check_ref_table == TRUE ? $key . ' ' : '') . $row[$language];
                            $check = strstr($data, $filter['autocomplete_search']);
                            if ($check) {
                                $response[] = array(
                                    'key' => $key,
                                    'value' => trim($data)
                                );
                            }
                        }
                    }
                }
            }
        } else {
            $CI->adodb->SetFetchMode(2);
            $check_id = json_decode($id);
            if (!is_object($check_id) || empty($id)) {
                $response = array();
                if (is_callable(array($CI->$table, 'get_autocomplete'), FALSE)) {
                    $response = $CI->$table->get_autocomplete($id, $filter);
                } else {
                    $CI->adodb->SetFetchMode(3);
                    if (empty($config_table)) {
                        $dbtable = $CI->config->item($CI->$table->table_name);
                    } else {
                        $dbtable = $CI->config->item($config_table);
                    }
                    if (!empty($dbtable) && is_array($dbtable)) {
                        if (check_lang($dbtable['db_label'])) {
                            $lang_data = suffix_lang($dbtable['db_label']);
                            $db_label = implode(',', $lang_data);
                        } else {
                            $db_label = $dbtable['db_label'];
                        }

                        //$filter = array();
                        if (empty($id)) {
                            $key = 'key';
                            $filter['quick_search'] = $_POST['data'];
                        } else {
                            $key = 'title';
                            $filter['IN_ID'] = $id;
                        }
                        $result = $CI->$table->select_by_filter(array($dbtable['db_id'], $db_label), $filter, $dbtable['db_order']);
                        if (is_object($result)) {
                            while ($row = $result->FetchRow()) {
                                $response[] = array(
                                    $key => $row[1],
                                    'value' => $row[$dbtable['db_id']]
                                );
                            }
                        }
                    }
                    $CI->adodb->SetFetchMode(2);
                }
                if (!empty($response) && is_array($response)) {
                    $temp = array();

                    foreach ($response as $row) {
                        if (is_array($row)) {
                            $row['value'] = trim($row['value']);
                            if (!empty($row['title'])) {
                                $temp[$row['value']] = $row;
                            } else {
                                $temp[$row['value']] = array(
                                    'key' => $row['value'],
                                    'value' => trim($row['key'])
                                );
                            }
                        }
                    }

                    if (!empty($temp)) {
                        $response = array();
                        foreach ($temp as $row) {
                            $response[] = $row;
                        }
                    }
                }
            } else {
                $response = json_decode($id);
            }
        }

        return $response;
    }

}

if (!function_exists('get_data_dropdown')) {

    /**
     * เลือกข้อมูลจาก autocomplete
     * @param string $model <p>
     * ชื่อตาราง
     * </p>
     * @param int $id  <p>
     * ค่า id ที่ต้องการ
     * </p>
     * @return Array
     * ค่าของ autocomplete
     *
     */
    function get_data_dropdown($select_by) {
        $temp = array();
        $temp = lang($select_by);
        return $temp;
    }

}

if (!function_exists('get_data_lang')) {

    /**
     * เลือกข้อมูลจาก $lang
     * @param string $model <p>
     * ชื่อตาราง
     * </p>
     * @param int $id  <p>
     * ค่า id ที่ต้องการ
     * </p>
     * @return Array
     * ค่าของ $lang
     *
     */
    function get_data_lang($select_by, $value = NULL) {
        $temp = lang($select_by);
        if (!empty($value)) {
            if (!empty($temp) && is_array($temp)) {
                $temp = $temp[$value];
            } else {
                $temp = $value;
            }
        }
        return $temp;
    }

}

if (!function_exists('get_dropdown_model')) {

    /**
     * เลือกข้อมูลจาก autocomplete
     * @param string $db_table <p>
     * ชื่อตาราง
     * </p>
     * @param string $lang  <p>
     * ภาษาที่ต้องการ
     * @param array $option  <p>
     * ภาษาที่ต้องการ
     * </p>
     * @return Array
     * id และ name ที่ได้จาก $lang
     *
     */
    function get_dropdown_model($db_table, $default = 'กรุณาเลือก', $lang = 'th') {
        $array = get_data_model($db_table, NULL, 'array');
        if (is_array($array) && sizeof($array) > 0) {
            $option = array('' => $default);
            foreach ($array as $key => $val) {
                $option[$key] = $val[$lang];
            }
        }
        return $option;
    }

}

if (!function_exists('get_dropdown_lang')) {

    /**
     * เลือกข้อมูลจาก autocomplete
     * @param string $key <p>
     * ชื่อตาราง
     * </p>
     * @param string $lang  <p>
     * ภาษาที่ต้องการ
     * @param array $option  <p>
     * ภาษาที่ต้องการ
     * </p>
     * @return Array
     * id และ name ที่ได้จาก $lang
     *
     */
    function get_dropdown_lang($line, $default = 'กรุณาเลือก', $lang = 'th') {
        $array = lang($line);
        if (is_array($array) && sizeof($array) > 0) {
            $option = array('' => $default);
            foreach ($array as $key => $val) {
                if ($key != 'LABEL') {
                    $option[$key] = $val;
                }
            }
        }
        return $option;
    }

}

if (!function_exists('get_select_item')) {

    /**
     * ทำการกรอง id จาก datagrid ที่ทำการเลือก โดยการตัด spacebar และ undefined ออกจากการเลือกรายการ
     * @param string $select_item <p>
     * id ของ datagrid ที่เลือกจาก datagrid จากตัวแปร $_POST['selected']  
     * </p>
     * @return string
     * id ที่ได้จาก datagrid
     *
     */
    function get_select_item($select_item) {
        if (!empty($select_item)) {
            $select_item = str_replace(' ', '', $select_item);
            $select_item = str_replace('undefined', '', $select_item);
            $select_item = explode(',', $select_item);
            $select_item = array_filter($select_item);
            $select_item = implode(',', $select_item);
        } else {
            $select_item = '';
        }
        return $select_item;
    }

}
/* End of file My_form_helper.php */
/* Location: ./application/helper/My_form_helper.php */