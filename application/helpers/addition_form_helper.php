<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

if (!function_exists('get_form_id')) {

    function get_form_id($action = NULL, $form_id = NULL, $id = NULL) {

        if ($action == 'add') {
            $form_id = str_replace('null', '', $form_id);
        } else if ($action == 'edit') {
            $select_id_len = strlen($id);
            $form_id_len = strlen($form_id);
            $form_id = substr($form_id, 0, $form_id_len - $select_id_len);
        }
        return $form_id;
    }

}

