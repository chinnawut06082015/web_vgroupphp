<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of image_helper
 *
 * resize image
 * @author Administrator
 */
if (!function_exists('resize_image')) {

//put your code here
    function resize_image($newWidth, $originalFile) {

        if (empty($newWidth) || empty($originalFile)) {
            return FALSE;
        } else {
            $finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
            $f_info = finfo_file($finfo, $originalFile);
            finfo_close($finfo);
            $temp_info = explode('/', $f_info);
            if ($temp_info[0] == 'image') {
                list($width, $height) = getimagesize($originalFile);
                $newHeight = ($height / $width) * $newWidth;
                $tmp = imagecreatetruecolor($newWidth, $newHeight);

                if ($width > $newWidth) {
                    switch ($temp_info[1]) {
                        case 'jpeg' :
                            $src = imagecreatefromjpeg($originalFile);
                            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                            imagejpeg($tmp, $originalFile, 95);
                            imagedestroy($tmp);
                            break;

                        case 'png' :
                            imagealphablending($tmp, false);
                            imagesavealpha($tmp, true);
                            $src = imagecreatefrompng($originalFile);
                            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                            imagepng($tmp, $originalFile, 9);
                            imagedestroy($tmp);
                            break;
                        case 'gif' : break;
                        default : break;
                    }
                }
            }
        }
    }

}
if (!function_exists('profile_image')) {

    function profile_image($name, $size = '150') {

        $file = '';
        if (is_file($name)) {
            $file = $name;
        } else {
            $file = "./resources/images/blank.jpg";
        }

        echo "<img src = '{$file}'  width='{$size}' >";
    }
    function no_image($name, $size = '150') {

        $file = '';
        if (is_file($name)) {
            $file = $name;
        } else {
            $file = "./resources/images/no_image.jpeg";
        }

        echo "<img src = '{$file}'  width='{$size}' >";
    }


}
