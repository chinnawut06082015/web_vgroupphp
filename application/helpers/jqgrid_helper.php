<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');
/**
 * jqGrid CodeIgniter implementation
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   jqGrid CI
 * @author    Suranart Sriyeesun  (oomzabb@hotmail.com)
 * @version   0.1
 * Copyright (c) 2010 Suranart Sriyeesun  (http://www.facebook.com/TanOhmZ)
 */
if (!function_exists('build_grid_js')) {

  /**
   * Build Javascript to display grid
   *
   * @param	grid id, or the div id
   * @param	url to make the ajax call
   * @param	array with colModel info:
   * 			* 0 - display name
   * 	 		* 1 - width
   * 	 		* 2 - sortable
   * 			* 3 - align
   * 			* 4 - searchable (2 -> yes and default, 1 -> yes, 0 -> no.)
   * 			* 5 - hidden (TRUE or FALSE, default is FALSE. This index is optional.)
   * @param	array with button info:
   * 		 	* 0 - display name
   * 	 		* 1 - bclass
   * 	 		* 2 - onpress
   * @param	default sort column name
   * @param	default sort order
   * @param	array with aditional parameters
   * @return	string


   */
  function build_grid_js($grid_id, $url, $colModel, $sortname, $sortorder, $gridParams = NULL) {

      // get CI instance
    $CI =& get_instance();

    //Basic propreties
    $grid_js = '<script type="text/javascript">';
    $grid_js .= '$(function() {';
    $grid_js .= '$("#' . $grid_id . '").jqGrid({';
    $grid_js .= "url: '" . $url . "',";
    $grid_js .= "datatype: 'json',";
    $grid_js .= "sortname: '" . $sortname . "',";
    //$grid_js .= "sortorder: '" . $sortorder . "',";
    $grid_js .= "viewrecords: true,";

    // $grid_js .= "usepager: true,";
    // $grid_js .= "useRp: true,";
    //Other propreties
    if (is_array($gridParams)) {
      //String exceptions that dont have ' '. Must be lower case.
      $string_exceptions = array("rpoptions");

      //Print propreties
      foreach ($gridParams as $index => $value) {
        //Check and print with or without ' '
        if (is_numeric($value)) {
          $grid_js .= $index . ": " . $value . ",";
        } else {
          if (is_bool($value))
            if ($value == true)
              $grid_js .= $index . ": true,";
            else
              $grid_js .= $index . ": false,";
          else
          if (in_array(strtolower($index), $string_exceptions))
            $grid_js .= $index . ": " . $value . ",";
          else
            $grid_js .= $index . ": '" . $value . "',";
        }
      }
    }

    $col_name = "colNames:[";

    $grid_js .= "colModel : [";

    //Get colModel
    foreach ($colModel as $index => $value) {
      $grid_js .= "{name : '" . $index . "' ,index : '" . $index . "' , " . " width : " . $value[0] . ", align: '" . $value[2] . "'" . (isset($value[4]) && $value[4] ? ", hide : true" : "") . "},";
      $col_name .="'" . $CI->lang->line($index) . "',";
    }


    //Remove the last ","
    $col_name = substr($col_name, 0, -1) . ']';
    $grid_js = substr($grid_js, 0, -1) . '],';

    $grid_js .=$col_name;
    //Finalize
    $grid_js .= "});";
    $grid_js .= '$("#' . $grid_id . '").jqGrid("navGrid","#pager2",{edit:false,add:false,del:false});';
   
  $grid_js .= "}); </script>";

    return $grid_js;
  }

}