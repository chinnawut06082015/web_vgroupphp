<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Language Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Language CI
 * @author    Suranart Sriyeesun  (oomzabb@hotmail.com)
 * @version   0.1
 * Copyright (c) 2010 Suranart Sriyeesun  (http://www.facebook.com/TanOhmZ)
 */
if (!function_exists('suffix_lang')) {

    /**
     * เพิ่ม Tag ภาษาให้กับข้อมูลตาม _EN _TH
     * @param string $str <p>
     * ข้อมูลที่ต้องการ
     * </p>
     * @return Array
     * ข้อมูลที่ติด Tag ภาษา
     *
     * @param string $suffix [optional] <p>
     * suffix ที่ต่อท้ายชื่อฟิลด์ เช่น "_ML"
     * </p>
     * <b>Input</b><br>
     * suffix_lang("CAMPUS_NAME_ML")
     *
     * <b>Output</b><br>
     * array(CAMPUS_NAME_TH,CAMPUS_NAME_EN)
     *
     *
     */
    function suffix_lang($str, $suffix = NULL) {
        $CI = & get_instance();

        if (!$suffix) {
            $suffix = '_ML';
        }

        $str = str_replace($suffix, '', $str);

        $lang_data = $CI->config->item('app_lang');
        $result = array();
        foreach ($lang_data as $item) {
            //array_push($result, $str . '_' . strtoupper($item) . ' AS '.strtoupper($item));
            array_push($result, $str . '_' . strtoupper($item));
        }

        return $result;
    }

}

if (!function_exists('build_ml_field')) {

    /**
     * แยกฟิลด์ที่เป็น Multi Language ออกเป็นสองฟิลด์ และคืนค่าเป็นอาเรย์
     * @param Array $field_list <p>
     * ฟิลด์ที่ต้องการ
     * </p>
     * @return Array
     * ข้อมูลอาเรย์ของฟิลด์ที่แยกแล้ว
     *      
     */
    function build_ml_field($field_list = Array()) {
        $new_field_list = '';
        foreach ($field_list as $field) {
            if (!check_lang($field, '_AT')) {
                if (check_lang($field)) {
                    $lang_data = suffix_lang($field);
                    foreach ($lang_data as $item) {
                        $new_field_list .= trim($item) . '-';
                    }
                } else {
                    $new_field_list .= trim($field) . '-';
                }
            }
        }
        $new_field_list = substr($new_field_list, 0, strlen($new_field_list) - 1);
        $result = explode('-', $new_field_list);

        return $result;
    }

}


if (!function_exists('build_select_field')) {

    /**
     * แยกฟิลด์ที่เป็น Multi Language และส่งคืนเฉพาะภาษาที่เลือก
     * @param Array $field <p>
     * ฟิลด์ที่ต้องการ
     * </p>
     * @return String
     * ข้อมูลฟิลด์
     *
     */
    function build_select_field($field = Array()) {

        $result = '';
        if (is_array($field)) {
            $arr = array();
            foreach ($field as $row) {
                if (check_lang($row)) {
                    $lang_data = suffix_lang($row);
                    foreach ($lang_data as $item) {
                        $sel_lang = check_select_lang($item);
                        if ($sel_lang == lang('SUFFIX'))
                            $arr[] = trim($item);
                    }
                } else {
                    $arr[] = trim($row);
                }
            }
            $result = implode("','", $arr);
        } else {
            if (check_lang($field)) {
                $lang_data = suffix_lang($field);
                $chk = FALSE;
                foreach ($lang_data as $item) {
                    $sel_lang = check_select_lang($item);
                    if ($sel_lang == lang('SUFFIX')) {
                        $result .= trim($item);
                        $chk = TRUE;
                    }
                }
                if ($chk == FALSE) {
                    $field = trim($field);
                    $result .= substr($field, 0, strlen($field) - 2) . lang('SUFFIX');
                }
            } else {
                $result .= trim($field);
            }
        }


        return $result;
    }

}

if (!function_exists('check_lang')) {

    /**
     * ตรวจสอบชื่อฟิลด์ที่เป็น Multi Language จาก suffix ที่ต่อท้าย
     * @param string $str <p>
     * ข้อมูลที่ต้องการ
     * </p>
     * @param string $suffix [optional] <p>
     * suffix ที่ต่อท้ายชื่อฟิลด์ เช่น "_ML"
     * </p>
     * @return boolean
     * true false
     *
     * <b>Input</b><br>
     * check_lang( "CAMPUS_NAME_ML")
     *
     * <b>Output</b><br>
     * TRUE
     */
    function check_lang($str, $suffix = NULL) {

        if (!$suffix) {
            $suffix = '_ML';
        }
        if (substr($str, strlen($str) - 3, strlen($str)) == $suffix) {
            $result = TRUE;
        } else {
            $result = FALSE;
        }
        // $result = substr($str,strlen($str)-3,strlen($str));
        return $result;
    }

}

if (!function_exists('check_select_lang')) {

    /**
     * ตรวจสอบ Suffix ของภาษา
     * @param string $str <p>
     * ข้อมูลที่ต้องการ
     * </p>
     * @return $string
     * ข้อมูลภาษา
     *
     * <b>Input</b><br>
     * check_select_lang( "CAMPUS_NAME_TH")
     *
     * <b>Output</b><br>
     * th
     */
    function check_select_lang($str) {

        $result = strtolower(substr($str, strlen($str) - 2, strlen($str)));

        return $result;
    }

}
if (!function_exists('label')) {

    /**
     * ค้นหา label ของชื่อฟิลด์ จาก language ในระบบ
     * @param string $str <p>
     * ข้อมูลที่ต้องการ
     * </p>
     * @return $string
     * ชื่อ label
     *
     * <b>Input</b><br>
     * label( "ACAD_YEAR")
     *
     * <b>Output</b><br>
     * ปีการศึกษา
     */
    function label($str, $value = NULL) {
        $result = lang($str);
        if (is_array($result)) {
            if ($value != '') {
                $result = $result[$value];
            } else {
                $result = $result['LABEL'];
            }
        } else {
            $result = ($result == '') ? $str : $result;
        }
        return $result;
    }

}
