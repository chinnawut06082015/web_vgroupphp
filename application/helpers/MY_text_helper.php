<?php

/*
  แปลงค่า Money
 *
 * input .499
 * output 0.49
 */
if (!function_exists('FormatMoney')) {

    function FormatMoney($num = 0.00) {

        $num = floatval($num);
        $temp = explode('.', $num);
        $temp[1].= '000';
        $temp1 = $temp[0];
        $temp2 = substr($temp[1], '0', '2');
        return number_format($temp1) . '.' . $temp2;
    }

}

/*
  แปลงค่า RoundMoney
 *
 * input .499
 * output 0.49
 */
if (!function_exists('RoundMoney')) {

    function RoundMoney($num = 0.00) {

        $num = floatval($num);
        $temp = explode('.', $num);
        $temp[1].= '000';
        $temp1 = $temp[0];
        $temp2 = substr($temp[1], '0', '2');
        return $temp1 . '.' . $temp2;
    }

}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
  แปลงค่า GPA
 *
 * input .25
 * output 0.25
 */
if (!function_exists('FormatGPA')) {

    function FormatGPA($gpa = 0) {

        $gpa = floatval($gpa);
        $gpa.= "000";
        $temp1 = substr($gpa, "0", "1");
        $temp2 = substr($gpa, "2", "2");
        return $temp1 . "." . $temp2;
    }

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
  แปลงค่า GPA
 *
 * input .25
 * output 0.25
 */
if (!function_exists('CalGPA')) {

    function CalGPA($COMP_CR = 0, $CHK_CR = 0) {

        $comp = floatval($COMP_CR);
        $chk = floatval($CHK_CR);
        $gpa = ($chk > 0) ? $comp / $chk : 0;
        return FormatGPA($gpa);
    }

}

/*
 * To check encoding not UTF-8 and convert to UTF-8 form TIS-620.
 *
 * ตรวจสอบ encodeing และแปลงเป็น UTF-8 จาก TIS-620 (form .csv|.txt)
 *
 * input $string(UTF-8/TIS-620)
 * output $string (UTF-8)
 */
if (!function_exists('get_txt_encode_utf8')) {

    function get_txt_encode_utf8($string, $encoding = 'auto') {

        if ($encoding == 'auto') {
            if (!mb_detect_encoding($string, 'UTF-8', true)) {
                $string = iconv('TIS-620', 'UTF-8', $string);
            }
        } else {
            $string = iconv($encoding, 'UTF-8', $string);
        }
        return $string;
    }

}

/*
 * นับนับตัวอักษรภาษาไทย ไม่รวมวรรณยุกต์
 *
 * input $string(UTF-8/TIS-620)
 * output $number
 */
if (!function_exists('strlen_th')) {

    function strlen_th($string) {

        if (mb_detect_encoding($string, 'UTF-8', true)) {
            $string = iconv('UTF-8', 'TIS-620', $string);
        }
        $l = strlen($string);
        $t = '';
        $a = array(209, 212, 213, 214, 215, 216, 217);
        for ($i = 0; $i <= $l; $i++) {
            $c = ord($string{$i});
            if ($c > 0 && $c < 232 && !in_array($c, $a)) {
                $t.=$string{$i};
            }
        }
        return strlen($t);
    }

}

/*
 * กำหนดจำนวนตัวเลขที่แสดง
 * เช่น กำหนดให้เป็น 4 หลัก ดังนั้นเลขที่ 1 จะแสดงเป็น 0001
 * input $string
 * output $string
 * Noke 11/07/2015
 */
if (!function_exists('view_strlen_no')) {

    function view_strlen_no($string,$len=1) {
        $return_string = '0';
        if(!empty($string)){
            $len_current =  strlen($string);
            $left_len = $len - $len_current;
            $point='';
            for($i=0; $i<$left_len; $i++){
                $point .= '0';
            }
            $return_string=$point.$string;
        }
        return $return_string;

    }

}
