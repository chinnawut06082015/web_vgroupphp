<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of date_helper
 * 	สร้างวันที่ในรูปแบบไทย
 * @input 20060221
 * @output 21 ก.พ. 2549
 * @author Administrator
 */
if (!function_exists('view_date')) {

//put your code here
    function view_date($d, $full = false) {

        $d = trim($d);
        $month = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $monthFull = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $monthFullEn = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $weekFull = array("วันอาทิตย์ ที่", "วันจันทร์ ที่", "วันอังคาร ที่", "วันพุธ ที่", "วันพฤหัสบดี ที่", "วันศุกร์ ที่", "วันเสาร์ ที่");
        $tmp = explode(" ", $d);

        if (!empty($d)) {
            if (strpos($tmp[0], '/') > 0) {

                $date_array = explode("/", $tmp[0]);
                $fyear = $date_array[2];
                $fmonth = $date_array[1];
                $fday = $date_array[0];
            } else {

                $date_array = explode("-", $tmp[0]);
                if (strpos($tmp[0], '-') > 0) {
                    $fyear = $date_array[0];
                    $fmonth = $date_array[1];
                    $fday = $date_array[2];
                }
                if (sizeof($date_array) == 3) {
                    $fyear = $date_array[0];
                    $fmonth = $date_array[1];
                    $fday = $date_array[2];
                }
            }

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;

            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            if ($fyear < 2200) {
                $fyear = $fyear + $adjust_year;
            }

            switch ($full) {
                case 'emy':
                    $finish_date = $monthFullEn[(int) $fmonth - 1] . " " . ($fyear - $adjust_year);
                    break;
                case 'tmy':
                    $finish_date = $monthFull[(int) $fmonth - 1] . " " . $fyear;
                    break;
                case 'mm':
                    $finish_date = (int) $fday . " " . $monthFull[(int) $fmonth - 1] . " " . $fyear;
                    break;
                case 'me':
                    $finish_date = (int) $fday . " " . $monthFullEn[(int) $fmonth - 1] . " " . $fyear;
                    break;
                case 'm':
                    $finish_date = (int) $fday . " " . $month[(int) $fmonth - 1] . " " . $fyear;
                    break;
                case 'wf':
                    $w = date("w", mktime(0, 0, 0, $fmonth, $fday, ($fyear > 2200 ? $fyear - $adjust_year : $fyear)));
                    $finish_date = $weekFull[$w] . ' ' . $fday . ' ' . $monthFull[(int) $fmonth - 1] . ' ' . $fyear;
                    break;
                case 'sh':
                    $fyear = substr($fyear, '2', '2');
                    $finish_date = $fday . "/" . $fmonth . "/" . $fyear;
                    $finish_date = ($finish_date == "00/00/543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "0//543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "//543") ? "" : $finish_date;
                case 'en':

                    if ($lang == 'TH') {
                        $fyear -= 543;
                    }
                    $finish_date = $fday . "/" . $fmonth . "/" . ($fyear);
                    $finish_date = ($finish_date == "00/00/543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "0//543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "//543") ? "" : $finish_date;
                    break;
                case 'dt':
                    $finish_date = $fday . "/" . $fmonth . "/" . $fyear;
                    $finish_date = ($finish_date == "00/00/543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "0//543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "//543") ? "" : $finish_date;
                    $time = explode(".", $tmp[1]);
                    $finish_date .= " " . $time[0];
                    break;
                case 'sql':
                    $finish_date = $fyear - $adjust_year . $fmonth . $fday;
                    $finish_date = ($finish_date == "00/00/543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "0//543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "//543") ? "" : $finish_date;
                case 'sql1':
                    $finish_date = ($fyear - $adjust_year) .'-'. $fmonth .'-'. $fday;
                    break;
                case 'monthThaiFullOnly':
                    $finish_date = $monthFull[(int) $fmonth - 1];
                    break;
                case 'yearThaiOnly':
                    $finish_date = $fyear;
                    break;
                case 'short_sql_date':
                    $finish_date = $fyear . "-" . $fmonth . "-" . $fday;
                    // echo "finish_date1 = ".$finish_date;
                    break;
                case 'fulldate_en':
                    $finish_date = (int) $fday . " " . $monthFullEn[(int) $fmonth - 1] . " " . ($fyear - $adjust_year);
                    break;
                default:
                    $finish_date = $fday . "/" . $fmonth . "/" . $fyear;
                    $finish_date = ($finish_date == "00/00/543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "0//543") ? "" : $finish_date;
                    $finish_date = ($finish_date == "//543") ? "" : $finish_date;
                    break;
            }
            // echo "finish_date2 = " . $finish_date;
            return $finish_date;
        }
    }

}

/**
 * Description of date_helper
 * 	สร้างเวลา
 * @input 18:36:00.0000000
 * @output 18:36
 * @author Administrator
 */
if (!function_exists('view_time')) {

//put your code here
    function view_time($t, $full = false) {
        $t = trim($t);

        $tmp = explode(".", $t);
        if (!empty($t)) {
            $time_array = explode(":", $tmp[0]);
            $fhour = $time_array[0];
            $fmin = $time_array[1];
            $fsec = $time_array[2];

            switch ($full) {
                case 'h':
                    $result = (int) $fhour;
                    break;
                case 'm':
                    $result = (int) $fmin;
                    break;
                case 's':
                    $result = (int) $fsec;
                    break;
                case 'hm':
                    $result = $fhour . ":" . $fmin;
                    break;
                case 'hms':
                    $result = $fhour . ":" . $fmin . "/" . $fsec;
                    break;
                case 'ms':
                    $result = $fmin . "/" . $fsec;
                    break;
                default:
                    $result = $fhour . ":" . $fmin;
                    break;
            }
        }
        return $result;
    }

}

/**
 * Description of date_helper
 * 	สร้างเวลา
 * @input 18:36:00.0000000
 * @output 18:36
 * @author Administrator
 */
if (!function_exists('view_time_2')) {

//put your code here
    function view_time_2($t, $full = false) {
        $t = trim($t);

        $tmp = explode(".", $t);
        if (!empty($t)) {
            $time_array = explode(":", $tmp[0]);
            $fhour = $time_array[0];
            $fmin = $time_array[1];
            $fsec = $time_array[2];

            switch ($full) {
                case 'h':
                    $result = (int) $fhour;
                    break;
                case 'm':
                    $result = (int) $fmin;
                    break;
                case 's':
                    $result = (int) $fsec;
                    break;
                case 'hm':
                    $result = $fhour . ":" . $fmin;
                    break;
                case 'hms':
                    $result = $fhour . ":" . $fmin . "/" . $fsec;
                    break;
                case 'ms':
                    $result = $fmin . "/" . $fsec;
                    break;
                default:
                    $result = $fhour . ":" . $fmin;
                    break;
            }
        }
        return $result;
    }

}

/**
 * Description of date_helper
 * 	สร้างวันที่ในรูปแบบไทย
 * @input 20060221
 * @output 21 ก.พ. 2549
 * @author Administrator
 */
if (!function_exists('view_date_between')) {

//put your code here
    function view_date_between($d1, $d2) {
        $d1 = view_date(trim($d1));
        $d2 = view_date(trim($d2));
        $month = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $tmp1 = empty($d1) ? NULL : explode("/", $d1);
        $tmp2 = empty($d2) ? NULL : explode("/", $d2);
        if ($tmp1['2'] == $tmp2['2']) {
            if ($tmp1['1'] == $tmp2['1']) {
                if ($tmp1['0'] == $tmp2['0']) {
                    $finish_date = (int) $tmp1['0'] . ' ' . $month[(int) $tmp1['1'] - 1] . ' ' . substr($tmp1['2'], 2);
                } else {
                    $finish_date = (int) $tmp1['0'] . '-' . (int) $tmp2['0'] . ' ' . $month[(int) $tmp1['1'] - 1] . ' ' . substr($tmp1['2'], 2);
                }
            } else {
                $finish_date = (int) $tmp1['0'] . ' ' . $month[(int) $tmp1['1'] - 1] . '-' . (int) $tmp2['0'] . ' ' . $month[(int) $tmp2['1'] - 1] . ' ' . substr($tmp1['2'], 2);
            }
        } else {
            $finish_date = is_null($tmp1) ? '' : (int) $tmp1['0'] . ' ' . $month[(int) $tmp1['1'] - 1] . ' ' . substr($tmp1['2'], 2);
            $finish_date .= ' - ';
            $finish_date .= is_null($tmp2) ? '' : (int) $tmp2['0'] . ' ' . $month[(int) $tmp2['1'] - 1] . ' ' . substr($tmp2['2'], 2);
        }
        return $finish_date;
    }

}
/**
 * Description of date_helper
 * 	สร้างวันที่ในรูปแบบฐานข้อมูล type datetime
 * @input 21/02/2553
 * @output 20100221
 * @author Administrator
 */
if (!function_exists('sql_date')) {

//put your code here
    function sql_date($d) {
        $tmp = @explode("/", $d);

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        $tmp[2] = (integer) $tmp[2] - $adjust_year;
        $finish_date = "{$tmp[2]}{$tmp[1]}{$tmp[0]}";
        return $finish_date;
    }

}



/**
 * Description of date_helper
 * 	สร้างวันที่ในรูปแบบฐานข้อมูล type datetime
 * @input 21/02/2553
 * @output 2010-02-21
 * @author Administrator
 */
if (!function_exists('sql_search_date')) {

//put your code here
    function sql_search_date($d) {
        $tmp = @explode("/", $d);

        $CI = & get_instance();
        $lang = (ses_data('lang') == "" ) ? "TH" : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }
        $tmp[2] = $tmp[2] - $adjust_year;
        $finish_date = "{$tmp[2]}-{$tmp[1]}-{$tmp[0]}";
        return $finish_date;
    }

}

/*
  หาปีงบประมาณ
  input 2006-10-21 00:00:00
  output 2550
 */
if (!function_exists('get_year_budget')) {

    function get_year_budget($pdate = NULL) {



        if ($pdate == NULL) {
            $date = date("Y-m-d") . ' ' . date("H:i:s");

            $fyear = substr($date, "0", "4");
            $month = substr($date, "5", "2");

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
//            if ($lang == 'TH') {
//                $adjust_year = 543;
//            }

            $fyear = $fyear + $adjust_year;
        } else {
            $date = $pdate;
            $tmp = @explode("/", $pdate);

            $fyear = $tmp[2];
            $month = $tmp[1];
        }

        //ต.ค.
        if ($month >= 10) {
            $year_budget = $fyear + 1;
        } else {
            $year_budget = $fyear;
        }
        return $year_budget;
    }

}

/*
  หาปีการศึกษา
  input 2006-02-21 00:00:00
  output 2549
 */
if (!function_exists('get_year_std')) {

    function get_year_std($pdate = NULL) {

        if ($pdate == NULL) {
            $date = date("Y-m-d"); // . ' ' . date("H:i:s");
            //echo $date;

            $fyear = substr($date, "0", "4");
            $month = substr($date, "5", "2");

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            $fyear = $fyear + $adjust_year;
        } else {
            $date = $pdate;
            $tmp = @explode("/", $pdate);

            $fyear = $tmp[2];
            $month = $tmp[1];
        }

        //AEC
        if ($month <= 7) {
            $year_std = $fyear - 1;
        } else {
            $year_std = $fyear;
        }
        return $year_std;
    }

}
/*
  หาปีปัจจุบัน
  input 2006-02-21 00:00:00
  output 2549
 */
if (!function_exists('get_year')) {

    function get_year($pdate = NULL) {

        if ($pdate == NULL) {
            $date = date("Y-m-d") . ' ' . date("H:i:s");

            $fyear = substr($date, "0", "4");
            $month = substr($date, "5", "2");

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            $fyear = $fyear + $adjust_year;
        } else {
            $date = $pdate;
            $tmp = @explode("/", $pdate);

            $fyear = $tmp[2];
            $month = $tmp[1];
        }
        return $fyear;
    }

}
/*
  หาภาคเรียนของเดือนนั้นๆ
  input 2006-02-21 00:00:00
  output 1
 */
if (!function_exists('get_term')) {

    function get_term($pdate = NULL) {

        if ($pdate == NULL) {
            $date = date("Y-m-d") . ' ' . date("H:i:s");

            $fyear = substr($date, "0", "4");
            $month = substr($date, "5", "2");

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            $fyear = $fyear + $adjust_year;
        } else {
            $date = $pdate;
            $tmp = @explode("/", $pdate);

            $fyear = $tmp[2];
            $month = $tmp[1];
        }
        /*
          //มิ.ย. - ต.ค.
          if ($month >= 6 && $month <= 10) {
          $term = 1;
          }
          //พ.ย. - ธค.
          if ($month >= 11) {
          $term = 2;
          }
          //ม.ค. - ก.พ.
          if ($month >= 1 && $month <= 2) {
          $term = 2;
          }
          //มี.ค. - พ.ค.
          if ($month >= 3 && $month <= 5) {
          $term = 3;
          } */

        //AEC
        if ($month >= 8) {
            $term = 1;
        } else if ($month <= 5) {
            $term = 2;
        } else {
            $term = 3;
        }
        return $term;
    }

}

/*
  หาจำนวนนาที
 *   08.45 -> 09.10 = 25 min
 */
if (!function_exists('calMinute')) {

    function calMinute($time1, $time2 = '00.00') {
        $t1 = explode('.', ($time1 . '.'));
        $t2 = explode('.', ($time2 . '.'));

        $time1 = floatval($time1);
        $time2 = floatval($time2);

        if ($time1 > 24 || $time2 > 24) {
            return 0; //มากกว่า 24 ชั่วโมง
        } else {

            if ($t1[0] == $t2[0]) {
                $hour = 0;
            } else {
                $t1[0] = intval($t1[0]);
                $t2[0] = intval($t2[0]);

                $hour = abs($t1[0] - $t2[0]) * 0.4;
            }

            if (($time1 == $time2) || ($t1[1] > 59) || ($t2[1] > 59)) {
                return 0;
            } else {

                $tmp = ceil((abs($time1 - $time2) - $hour) * 100);

                if ($tmp % 5 != 0) {
                    $tmp--;
                }
                return $tmp;
            }
        }
    }

}

/*
  หาจำนวนชั่วโมง
 *   08.45 -> 09.10 = 25 min
 */
if (!function_exists('calHour')) {

    function calHour($minute) {
        if ($minute > 1440) {
            return '00.00'; //มากกว่า 24 ชั่วโมง
        } else {
            $minute = ceil($minute);
            $hour = intval($minute / 60);
            $min = $minute - ($hour * 60);
            return str_pad($hour, 2, '0', STR_PAD_LEFT) . '.' . str_pad($min, 2, '0', STR_PAD_LEFT);
        }
    }

}

/*
  หาวันที่ปัจจุบัน
  output 08/03/2554
 */
if (!function_exists('get_date')) {

    function get_date() {
        $date = date("Y-m-d");
        $temp = explode("-", $date);

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }
        return $temp[2] . '/' . $temp[1] . '/' . ($temp[0] + $adjust_year);
    }

}

/*
  หาวันที่ปัจจุบัน
  output 08/03/2554
 */
if (!function_exists('get_date_en')) {

    function get_date_en() {
        $date = date("Y-m-d");
        $temp = explode("-", $date);
        $fyear = $fyear;
        return $temp[2] . '/' . $temp[1] . '/' . $temp[0];
    }

}

/*
  input 20060221
  output 21/02/2549
 */
if (!function_exists('str_date')) {

    function str_date($d) {
        $finish_date = $d;
//        if (ereg("(^[1-2][0-9]{3})([0-1][0-9])([0-3][0-9])", $d, $regs)) {
        //            $finish_date = $regs[3] . "/" . $regs[2] . "/" . ($regs[1] + 543);
        //        }
        $dd = substr($d, 6, 2);
        $mm = substr($d, 4, 2);
        $yy = substr($d, 0, 4);

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        $finish_date = (integer) $dd . '/' . $mm . '/' . ($yy + $adjust_year);
        return $finish_date;
    }

}

/*
  สร้างวันที่ในรูปแบบไทย
  input 20060221
  output 21 ก.พ. 2549
 */
if (!function_exists('ViewThaiDate')) {

    function ViewThaiDate($d) {
        $thaimonth = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
//          if (ereg("(^[1-2][0-9]{3})([0-1][0-9])([0-3][0-9])", $d, $regs)) {
        //               $finish_date = (integer) $regs[3] . " " . $thaimonth[($regs[2] - 1)] . " " . ($regs[1] + 543);
        //          }
        $dd = substr($d, 6, 2);
        $mm = substr($d, 4, 2);
        $yy = substr($d, 0, 4);

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        $finish_date = (integer) $dd . ' ' . $thaimonth[($mm - 1)] . ' ' . ($yy + $adjust_year);
        return $finish_date;
    }

}

/*
  สร้างวันที่ในรูปแบบอังกฤษ
  input 2012-05-01
  mounthFullEn  1 ก.พ. 2012
  mounthFullTn  January 1, 2012
  mounthShortEn  May 1, 2555
  mounthShortTh  1 ก.พ. 2555
 */
if (!function_exists('view_date_format')) {

    function view_date_format($format = "", $d = null) {

        $d = empty($d) ? date("Y-m-d") : $d;
        $temp = explode("-", $d);

        $monthShortTh = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $monthFullTh = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        $monthShortEn = array("", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        $monthFullEn = array("", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        switch ($format) {
            case 'monthFullEn':
                $date = $monthFullEn[(int) $temp[1]] . ' ' . (int) $temp[2] . ', ' . $temp[0];
                break;

            case 'monthShortEn':
                $date = $monthShortEn[(int) $temp[1]] . ' ' . (int) $temp[2] . ', ' . $temp[0];
                break;

            case 'monthFullTh':

                $temp[0] = (int) $temp[0] + $adjust_year;
                $date = (int) $temp[2] . ' ' . $monthFullTh[(int) $temp[1]] . ' ' . $temp[0];
                break;

            case 'monthShortTh':
                $temp[0] = (int) $temp[0] + $adjust_year;
                $date = (int) $temp[2] . ' ' . $monthShortTh[(int) $temp[1]] . ' ' . $temp[0];
                break;
            case 'normal':
                $temp[0] = (int) $temp[0] + $adjust_year;
                $date = (int) $temp[2] . '/' . (int) $temp[1] . '/' . $temp[0];
                break;
            case 'dbEng':
                $temp[0] = (int) $temp[0];
                $date = (int) $temp[2] . '/' . (int) $temp[1] . '/' . $temp[0];
                break;

            default:
                $date = $d;
                break;
        }

        return $date;
    }

}

/*
  สร้างวันที่ในรูปแบบไทย
  input 20060221
  output 21 ก.พ. 2549
 */
if (!function_exists('get_thai_date')) {

    function get_thai_date($format = "full") {
        $d = view_date(date("Y-m-d"));
        $temp = explode("/", $d);
        $month = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $monthFull = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        if ($format == "full") {
            $date = view_thai_number($temp[0] . ' ' . $monthFull[(int) $temp[1]] . ' ' . $temp[2] . ' ' . date("H:i"));
        } else {
            $date = view_thai_number($temp[0] . ' ' . $month[(int) $temp[1]] . ' ' . $temp[2] . ' ' . date("H:i"));
        }
        return $date;
    }

}

/*
  คำนวณอายุจากวันเกิด
  input 20060221
  output 21 ก.พ. 2549
 */
if (!function_exists('get_age')) {

    function get_age($d) {

        if (!empty($d)) {
            $tmp = explode(" ", $d);
            if (strpos($tmp[0], '/') > 0) {
                $date = explode("/", $tmp[0]);
                $fyear = $date[2];
            } else {
                $date = explode("-", $tmp[0]);
                if (strpos($tmp[0], '-') > 0) {
                    $fyear = $date[0];
                }
                if (sizeof($date) == 3) {
                    $fyear = $date[0];
                }
            }
            $y = date("Y");
            $age = $y - $fyear;
        }
        return $age;
    }

}

/*
  สร้างวันที่ในรูปแบบไทย
  input $format
  output $finish_date
 */
if (!function_exists('gen_date')) {

    function gen_date($format) {

        $hour = 0; /* ปรับให้ตรงตามต้องการ */
        $min = 0; /* ปรับให้ตรงตามต้องกา */

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        $Year = date("Y") + $adjust_year;
        $Year_en = date("Y");

        $thaiweekFull = array("วันอาทิตย์ ที่", "วันจันทร์ ที่", "วันอังคาร ที่", "วันพุธ ที่", "วันพฤหัสบดี ที่", "วันศุกร์ ที่", "วันเสาร์ ที่");

        $thaimonthFull = array("มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");

        $thaimonth = array("ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");

        $monthFullEn = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

        /* $mdate หรือ $ThaiDate หรือ $ThaiDateFull */

        switch ($format) {
            /* วันศุกร์ที่ 3 สิงหาคม 2544 เวลา 12:36 น. */
            case 'fulldatetime':$finish_date = $thaiweekFull[date("w")] . date(" j ") . $thaimonthFull[date("m") - 1] . " " . $Year . date(" เวลา H:i น.", mktime(date("H") + $hour, date("i") + $min));
                break;
            /* 3 ส.ค. 2544 เวลา 12:36 น. */
            case 'shortdatetime':$finish_date = date("j ") . $thaimonth[date("m") - 1] . " " . $Year . date(" เวลา H:i น.", mktime(date("H") + $hour, date("i") + $min));
                break;
            /* 3 ส.ค. 2544 */
            case 'shortdate':$finish_date = date("j ", mktime(date("H") + $hour, date("i") + $min)) . $thaimonth[date("m") - 1] . " " . $Year;
                break;
            /* 3 สิงหาคม. 2544 */
            case 'fulldate':$finish_date = date("j ", mktime(date("H") + $hour, date("i") + $min)) . $thaimonthFull[date("m") - 1] . " " . $Year;
                break;
            /* ได้ค่าเป็น วินาที นับจากปี ค.ศ.1900 */
            case 'second':$finish_date = date("U", mktime(date("H") + $hour, date("i") + $min));
                break;
            /* 3 สิงหาคม. 2544 */
            case 'fulldate_en':$finish_date = date("j ", mktime(date("H") + $hour, date("i") + $min)) . $monthFullEn[date("m") - 1] . " " . $Year_en;
                break;
        }
        return $finish_date;
    }

}

/*
  คำนวณวันจันทร์ของสัปดาห์
 * Input 21/01/2554
 */
if (!function_exists('get_date_of_week')) {

    function get_date_of_week($d) {

        if (!empty($d)) {

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            $day = explode("/", $d);
            $fyear = ($day[2] > 2200 ? $day[2] - $adjust_year : $day[2]);
            $fmonth = $day[1];
            $fday = $day[0];
            $DAY_ID = date("w", mktime(0, 0, 0, $fmonth, $fday, $fyear)); //วันของสัปดาห์ อาทิตย์ = 0, จันทร์ = 1, ...

            $monday = new DateTime("{$fyear}-{$fmonth}-{$fday}");
            $sunday = new DateTime("{$fyear}-{$fmonth}-{$fday}");

            if ($DAY_ID == 0) {
//อ
                $monday->modify('-6 day');
            } else {
//จ-ส
                $m = $DAY_ID - 1;
                $s = 7 - $DAY_ID;
                if ($m > 0) {
                    $monday->modify('-' . $m . ' day');
                }
                $sunday->modify('+' . $s . ' day');
            }
            $t_d = date_format($monday, 'd/m/Y');
            $temp = explode('/', $t_d);
            $d_m = $temp[0] . '/' . $temp[1] . '/' . ($temp[2] + $adjust_year);

            $t_d = date_format($sunday, 'd/m/Y');
            $temp = explode('/', $t_d);
            $d_s = $temp[0] . '/' . $temp[1] . '/' . ($temp[2] + $adjust_year);

            $t = $d_m . ' - ' . $d_s;
        }
        return $t;
    }

}

if (!function_exists('ToSQLDate')) {

    function ToSQLDate($date_time, $lang_eng = FALSE, $last_time = FALSE) {
        $date_time = trim($date_time);
        if (empty($date_time)) {
            return 'NULL';
        } else {
            $temp = explode(' ', $date_time);

            $arrDate = explode('/', $temp[0]);

            if (($lang_eng == FALSE) && ($arrDate[2] > 2500)) {
                $CI = & get_instance();
                $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
                $adjust_year = 0;
                if ($lang == 'TH') {
                    $adjust_year = 543;
                }

                $yy = $arrDate[2] - $adjust_year;
            } else {
                $yy = $arrDate[2];
            }
            $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
            $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
            $date = $yy . '-' . $mm . '-' . $dd;

            if ($last_time == FALSE) {
                $time = '00:00:00';
            } else {
                $time = '23:59:59';
            }
            if (sizeof($temp) > 1) {
                $arrTime = explode(':', $temp[1]);
                $time = str_pad($arrTime[0], 2, '0', STR_PAD_LEFT) . ':' . str_pad($arrTime[1], 2, '0', STR_PAD_LEFT) . ':' . str_pad(floor($arrTime[2]), 2, '0', STR_PAD_LEFT);
            }
            return "CONVERT(datetime,'" . $date . " " . $time . "',120)";
        }
    }

}

if (!function_exists('DateTS')) {

    function DateTS($date_time, $lang_eng = FALSE) {
        $date_time = trim($date_time);
        if (empty($date_time)) {
            return 'NULL';
        } else {
            $temp = explode(' ', $date_time);

            $arrDate = explode('/', $temp[0]);
            if ($lang_eng == FALSE) {
                $CI = & get_instance();
                $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
                $adjust_year = 0;
                if ($lang == 'TH') {
                    $adjust_year = 543;
                }

                $yy = $arrDate[2] - $adjust_year;
            } else {
                $yy = $arrDate[2];
            }

            $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
            $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
            $date = $yy . '-' . $mm . '-' . $dd;

            $time = '00:00:00';
            if (sizeof($temp) > 1) {
                $arrTime = explode(':', $temp[1]);
                $time = str_pad($arrTime[0], 2, '0', STR_PAD_LEFT) . ':' . str_pad($arrTime[1], 2, '0', STR_PAD_LEFT) . ':' . str_pad(floor($arrTime[2]), 2, '0', STR_PAD_LEFT);
            }
            return strtotime($date . " " . $time);
        }
    }

}

if (!function_exists('returnBetweenArrayDate')) {

    function returnBetweenArrayDate($fromDate, $toDate) {
        
        //echo 'fromDate = '.$fromDate.br();
        //echo 'toDate = '.$toDate.br();

        $dateMonthYearArr = array();
        $fromDateTS = DateTS($fromDate);
        $toDateTS = DateTS($toDate);
        
        //echo "fromDateTS : {$fromDateTS} " . br();

        for ($currentDateTS = $fromDateTS; $currentDateTS <= $toDateTS; $currentDateTS += (60 * 60 * 24)) {
            // use date() and $currentDateTS to format the dates in between
            $dateMonthYearArr[] = $currentDateTS;
            //print $currentDateStr.”<br />”;
        }
        return $dateMonthYearArr;
    }

}

if (!function_exists('returnBetweenDate')) {

    function returnBetweenDate($field, $date_time, $lang_eng = FALSE) {
        $temp = explode(' ', $date_time);

        $arrDate = explode('/', $temp[0]);
        if ($lang_eng == FALSE) {
            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }
            $yy = $arrDate[2] - $adjust_year;
        } else {
            $yy = $arrDate[2];
        }

        $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
        $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
        $date = $yy . '-' . $mm . '-' . $dd;

        return "({$field} >= CONVERT(datetime,'" . $date . " 00:00:00',120)) AND ({$field} <= CONVERT(datetime,'" . $date . " 23:59:59',120))";
    }

}

//update 22/04/2013
if (!function_exists('returnBetweenDateFromTo')) {

    function returnBetweenDateFromTo($field, $date_from, $date_to, $lang_eng = FALSE, $convert = TRUE) {
        if ($convert === TRUE) {
            //date from
            $temp = explode(' ', $date_from);

            $CI = & get_instance();
            $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
            $adjust_year = 0;
            if ($lang == 'TH') {
                $adjust_year = 543;
            }

            $arrDate = explode('/', $temp[0]);
            if ($lang_eng == FALSE) {
                $yy = $arrDate[2] - $adjust_year;
            } else {
                $yy = $arrDate[2];
            }

            $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
            $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
            $date_from = $yy . '-' . $mm . '-' . $dd;

            //date to
            $temp = explode(' ', $date_to);

            $arrDate = explode('/', $temp[0]);
            if ($lang_eng == FALSE) {
                $yy = $arrDate[2] - $adjust_year;
            } else {
                $yy = $arrDate[2];
            }

            $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
            $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
            $date_to = $yy . '-' . $mm . '-' . $dd;
        }
        return "({$field} >= CONVERT(datetime,'" . $date_from . " 00:00:00',120)) AND ({$field} <= CONVERT(datetime,'" . $date_to . " 23:59:59',120))";
    }

}

//update 17/07/2014
if (!function_exists('returnBetweenDateFromToEng')) {

    function returnBetweenDateFromToEng($field, $date_from, $date_to, $lang_eng = FALSE) {
        //date from
        $temp = explode(' ', $date_from);

        $arrDate = explode('/', $temp[0]);

        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        // if ($lang_eng == FALSE)
        $yy = $arrDate[2] - $adjust_year;
        // else
        //     $yy = $arrDate[2];
        $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
        $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
        $date_from = $yy . '-' . $mm . '-' . $dd;

        //date to
        $temp = explode(' ', $date_to);

        $arrDate = explode('/', $temp[0]);
        // if ($lang_eng == FALSE)
        $yy = $arrDate[2] - $adjust_year;
        // else
        //     $yy = $arrDate[2];
        $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
        $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
        $date_to = $yy . '-' . $mm . '-' . $dd;

        return "({$field} >= CONVERT(datetime,'" . $date_from . " 00:00:00',120)) AND ({$field} <= CONVERT(datetime,'" . $date_to . " 23:59:59',120))";
    }

}

if (!function_exists('check_in_date_range')) {

    function check_in_date_range($start_date, $end_date, $date_from_user) {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);

        // Check that user date is between start & end
        return (($start_ts <= $user_ts) && ($user_ts <= $end_ts));
    }

}

if (!function_exists('add_date')) {

    function add_date($date, $y = 0, $m = 0, $d = 0, $type = 'default') {
        $CI = & get_instance();
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }

        switch ($type) {
            case 'datepicker':
                $day = explode("/", $date);
                $finish_date = date("Y-m-d", mktime(0, 0, 0, $day[1] + $m, $day[0] + $d, ($day[2] - $adjust_year) + $y));
                break;

            default:
                if (strpos($date, '/') > 0) {
                    $day = explode("/", $date);
                } else {
                    $day = explode("-", $date);
                }
                $finish_date = date("Y-m-d", mktime(0, 0, 0, $day[1] + $m, $day[0] + $d, $day[2] + $y));

                break;
        }
        return view_date($finish_date);
    }

}

if (!function_exists('import_timeatendance_check_date_format')) {

    function import_timeatendance_check_date_format($date = NULL) {
        $error = true;
        if (!empty($date)) {
            $temp = explode('/', $date);
            if (count($temp) == 3) {
                $error = false;
                if (intval($temp[0]) == 0 or intval($temp[0]) > 31 or intval($temp[1]) == 0 or intval($temp[1]) > 12) {
                    $error = true;
                } else {
                    if (strlen($temp[2]) == 4) {
                        $y = date('Y');
                        if ($temp[2] > $y) {
                            $error = true;
                        }
                    } else {
                        $error = true;
                    }
                }
            }
        }
        return $error;
    }

}

if (!function_exists('get_month_return')) {

    function get_month_return($month = NULL, $format = "full") {
        $monthShortTh = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $monthFullTh = array("", "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม");
        if ($format == "full") {
            $mm = $monthFullTh[$month];
        } else {
            $mm = $monthShortTh[$month];
        }
        return $mm;
    }

}

/*
  สร้างวันที่ในรูปแบบไทย
  input $format date  = 2006-02-11 00:00:00
  output $new date format +10 days =2006-02-21 00:00:00
 */
if (!function_exists('get_due_date')) {

    function get_due_date($currentDate = NULL, $format = "full") {
        $due_date = 10;
        $date = '';
        $temp = explode(' ', $currentDate);
        $arrDate = explode('/', $temp[0]);
        $yy = $arrDate[2];
        $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
        $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
        if ($dd > $due_date) {
            if ($mm == 12) {
                $new_mm = 1;
                $new_yy = $yy + 1;
            } else {
                $new_mm = $mm + 1;
                $new_yy = $yy;
            }
        } else {
            $new_mm = $mm;
            $new_yy = $yy;
        }
        $date = $due_date . '/' . $new_mm . '/' . $new_yy;
        return $date;
    }

}

//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
if (!function_exists('dateDifference')) {

    function dateDifference($date_1, $date_2, $differenceFormat = '%a') {
        if (!empty($date_1) && !empty($date_2)) {
            $datetime1 = date_create($date_1);
            $datetime2 = date_create($date_2);
            $interval = date_diff($datetime1, $datetime2);
        }
        return $interval->format($differenceFormat);
    }

}

/*
  สร้างวันที่ในรูปแบบไทย
  input 11/02/2006
  output 2006-02-11 00:00:00
 */
if (!function_exists('view_date_output_format')) {

    function view_date_output_format($date_time, $lang_eng = FALSE) {
        $date_time = trim($date_time);
        if (empty($date_time)) {
            return 'NULL';
        } else {
            $temp = explode(' ', $date_time);

            $arrDate = explode('/', $temp[0]);

            if (($lang_eng == FALSE) && ($arrDate[2] > 2500)) {
                $CI = & get_instance();
                $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
                $adjust_year = 0;
                if ($lang == 'TH') {
                    $adjust_year = 543;
                }
                $yy = $arrDate[2] - $adjust_year;
            } else {
                $yy = $arrDate[2];
            }
            $mm = str_pad($arrDate[1], 2, '0', STR_PAD_LEFT);
            $dd = str_pad($arrDate[0], 2, '0', STR_PAD_LEFT);
            $date = $yy . '-' . $mm . '-' . $dd;

            $time = '00:00:00';
            if (sizeof($temp) > 1) {
                $arrTime = explode(':', $temp[1]);
                $time = str_pad($arrTime[0], 2, '0', STR_PAD_LEFT) . ':' . str_pad($arrTime[1], 2, '0', STR_PAD_LEFT) . ':' . str_pad(floor($arrTime[2]), 2, '0', STR_PAD_LEFT);
            }
            return $date . " " . $time;
        }
    }

}

// 'Date Difference retrun days
// Input $date_1  10/11/2557
// Input $date_2  10/12/2557
// Output  8 (days)
//////////////////////////////////////////////////////////////////////
if (!function_exists('dateDifferenceDays')) {

    function dateDifferenceDays($date_1, $date_2, $differenceFormat = '%a') {
        // $date_1 = '2014-12-1';
        // $date_2 = '2014-12-10';
        if (!empty($date_1) && !empty($date_2)) {
            $start = new DateTime($date_1);
            $end = new DateTime($date_2);
            $days = round(($end->format('U') - $start->format('U')) / (60 * 60 * 24));
        }
        return $days;
    }

}


/*
  สร้างวันที่ในรูปแบบไทย
  input 11/02/2006
  output 2006-02-11 00:00:00
 */
if (!function_exists('colorDay')) {

    function colorDay() {
        $colorDay = array("", "จันทร์,#FFFF95", "อังคาร,#FF93FF", "พุธ,#75FF75", "พฤหัสบดี,#FFDF5E", "ศุกร์,#79CAFF", "เสาร์,#CB7DFF", "อาทิตย์,#FF4848");
        return $colorDay;
    }

}

/*
  แสดงข้อมูลวันตัวย่อ
  input 1
  output จ.
 */
if (!function_exists('view_day')) {

    function view_day($day = NULL, $view = 'full') {
        $fullDay = array("", "จันทร์", "อังคาร", "พุธ", "พฤหัสบดี", "ศุกร์", "เสาร์", "อาทิตย์");
        $shortDay = array("", "จ", "อ", "พ", "พฤ", "ศ", "ส", "อา");
        if ($view == 'full') {
            $thisday = $fullDay[$day];
        } else {
            $thisday = $shortDay[$day];
        }

        // echo "<pre>";
        //  echo "day=".$day;
        // print_r($shortDay);
        // echo "thisday=".$thisday;
        return $thisday;
    }

}

/*
  แสดงข้อมูลวันตัวย่อ
  input 3
  output ฤดูร้อน
 */
if (!function_exists('get_semester_name')) {

    function get_semester_name($semester_id, $acad_year = NULL, $lang = 'th') {

        if ($acad_year != 2556) {
            $arrYear = array(
                'en' => array('', 'First', 'Second', 'Summer'),
                'th' => array('', 'ที่ 1', 'ที่ 2', 'ฤดูร้อน')
            );
        } else {
            $arrYear = array(
                'en' => array('', 'First', 'Second', 'First Summer', 'Second Summer'),
                'th' => array('', 'ที่ 1', 'ที่ 2', 'ฤดูร้อนที่ 1', 'ฤดูร้อนที่ 2')
            );
        }
        return $arrYear[$lang][$semester_id];
    }

}

/*
  แสดงข้อมูลชั้นปีปัจจุบัน
  input ปีที่เข้าเรียน, ปีที่พิมพ์เอกสาร, หลักสูตรปกติ=1 หรือต่อเนื่อง=3
  output ฤดูร้อน
 */
if (!function_exists('get_student_year')) {

    function get_student_year($admit_year = NULL, $current_year = NULL, $curr_type_id = NULL) {

        if (!empty($admit_year) && !empty($current_year)) {
            if ($curr_type_id == '3') {
                $student_year = ($current_year - $admit_year) + 3;
                $student_year = ($student_year > 0 ? $student_year : 3);
            } else {
                $student_year = ($current_year - $admit_year) + 1;
                $student_year = ($student_year > 0 ? $student_year : 1);
            }
        }
        return $student_year;
    }

}


// รูปแบบของเวลาที่ใช้คำนวณ แบบ   
// อยู่ในรูปแบบ 00:00:00 ถึง 23:59:59  
if (!function_exists('diff2time')) {

    function diff2time($time_a, $time_b) {
        $now_time1 = strtotime(date("Y-m-d " . $time_a));
        $now_time2 = strtotime(date("Y-m-d " . $time_b));
        $time_diff = abs($now_time2 - $now_time1);
        $time_diff_h = floor($time_diff / 3600); // จำนวนชั่วโมงที่ต่างกัน  
        $time_diff_m = floor(($time_diff % 3600) / 60); // จำวนวนนาทีที่ต่างกัน  
        $time_diff_s = ($time_diff % 3600) % 60; // จำนวนวินาทีที่ต่างกัน  
        return $time_diff_h . " ชั่วโมง " . $time_diff_m . " นาที " . $time_diff_s . " วินาที";
    }

}

//  
// input 2017
// output 2560
if (!function_exists('view_year')) {

    function view_year($year) {
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }
        return $year + $adjust_year;
    }

}

// input 2560
// output 2017
if (!function_exists('form_year')) {

    function form_year($year) {
        $lang = ses_data('lang') === '' ? $CI->config->item('default_lang') : ses_data('lang');
        $adjust_year = 0;
        if ($lang == 'TH') {
            $adjust_year = 543;
        }
        return $year - $adjust_year;
    }

}