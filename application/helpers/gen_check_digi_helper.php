<?php

if (!function_exists('gen_2_digi')) {

    function gen_2_digi() {
        $m = microtime();
        $mt = intval(substr($m, 2, 2));
        $string = strval(abs(crc32(md5(sha1($_SERVER['HTTP_COOKIE'] . '@' . $_SERVER['HTTP_USER_AGENT'] . '#' . $_SERVER['REMOTE_ADDR'] . '$' . $m)))));
        $len = strlen($string);
        $sum = 0;
        for ($i = 0; $i < $len; $i++) {
            $sum += intval($string{$i});
        }
        if ($sum < $mt) {
            return str_pad(rand(0, $sum), 2, '0', STR_PAD_LEFT);
        } else {
            return str_pad(rand($sum, 99), 2, '0', STR_PAD_LEFT);
        }
    }

}

if (!function_exists('gen_cs_check_digi')) {//counter service

    function gen_cs_check_digi($data = NULL, $arr_mod = array(6, 4)) {
        if (!empty($data)) {
            //$arr_mod = array(6, 4);
            $data = strval($data);
            $l_data = strlen($data);
            $c_arr = count($arr_mod);
            $total = 0;
            for ($i = 0; $i < $l_data; $i++) {
                $c = $i % $c_arr;
                $total += $data{$i} * $arr_mod[$c];
            }

            $return = str_pad(strval($total), 2, '0', STR_PAD_LEFT);
            return substr($return, -2);
        } else {
            return '00';
        }
    }

}