<?php

if (!defined('BASEPATH'))
     exit('No direct script access allowed');
/**
 * Cache Helper
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   Language CI
 * @author    Suranart Sriyeesun  (oomzabb@hotmail.com)
 * @version   0.1
 * Copyright (c) 2010 Suranart Sriyeesun  (http://www.facebook.com/TanOhmZ)
 */
if (!function_exists('cache_master')) {

     /**
      * นำข้อมูลมาสเตอร์ที่ต้องการเข้าสู่แคช
      * @param string $model <p>
      * พาทของโมเดล
      * </p>
      * @param string $id <p>
      * id ของมาสเตอร์
      * </p>
      * @return string
      * ข้อมูล cache ที่ต้องการ
      *
      */
     function cache_master($model, $id, $value, $order, $cache_name='') {
          $CI = & get_instance();
          $cache_name = trim($cache_name);
          $cache_name = (!empty($cache_name) ? trim($cache_name) : trim($id));

          if (check_lang($value)) {

               $lang_data = suffix_lang($value);
               $value = implode(',', $lang_data);
          }

          $table = ses_path($model);
          $CI->load->model($model);
          $result = $CI->$table->cache_select(array($id, $value), $order);

          $lang_data = $CI->config->item('app_lang');

          //print_r($result);
          if (is_array($result) && sizeof($result) > 0) {
               foreach ($result as $drop_down => $index) {

                    $i = 1;
                    foreach ($lang_data as $item) {
                         $temp[$index[0]][$item] = ($index[$i] == '' ? $index[1] : $index[$i]);
                         $i++;
                    }
               }
               cache_store($cache_name, $temp);
          }
     }

}


if (!function_exists('cache_exists')) {

     /**
      * ตรวจสอบว่ามีแคชชื่อที่ส่งมาหรือไม่
      * @param string $cache_name <p>
      * ชื่อของแคช
      * </p>
      * @return boolean
      *
      */
     function cache_exists($cache_name) {

          if (function_exists('wincache_ucache_exists')) {
               return wincache_ucache_exists($cache_name);
          } else if (function_exists('apc_exists')) {
               return apc_exists($cache_name);
          }
     }

}


if (!function_exists('cache_fetch')) {

     /**
      * เรียกใช้ค่าจากแคช
      * @param string $cache_name <p>
      * ชื่อของแคช
      * </p>
      * @return Array
      *
      */
     function cache_fetch($cache_name) {

          if (function_exists('wincache_ucache_get')) { 
               return wincache_ucache_get($cache_name);
          } else if (function_exists('apc_fetch')) {
               return apc_fetch($cache_name);
          }
     }

}

if (!function_exists('cache_store')) {

     /**
      * เก็บค่าเข้าแคช
      * @param string $cache_name <p>
      * ชื่อของแคช
      * </p>
      * @return Array
      *
      */
     function cache_store($db_table, $tmp_dropdown) {

          if (function_exists('wincache_ucache_add')) {
               return wincache_ucache_add($db_table, $tmp_dropdown);
          } else if (function_exists('apc_store')) {
               return apc_store($db_table, $tmp_dropdown);
          }
     }

}

if (!function_exists('cache_check_extension')) {

     /**
      * ตรวจสอบการเปิดใช้งานแคช
      * @return TRUE, FALSE
      *
      */
     function cache_check_extension() {

          if (extension_loaded('wincache')) {
               return TRUE;
          } else if (extension_loaded('apc')) {
               return TRUE;
          } else {
               return FALSE;
          }
     }

}
