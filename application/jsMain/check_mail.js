function check_mail() {
            var re = /[A-Z0-9._%+-]+@[A-Z]/igm;
            if (re.test(document.getElementById('user').value)) {
                swal({
                    title: "ใส่เฉพาะ Username เท่านั้น ไม่ต้องใส่ @tu.ac.th หรือ @domain ของท่าน",
                    icon: "warning",
                    buttons: {
                        confirm: {
                            text: "Close",
                            value: true,
                            visible: true,
                            className: "btn btn-danger",
                            closeModal: true
                        }
                    }
                });
            }
        }