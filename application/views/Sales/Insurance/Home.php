<?php
$this->load->view('Sales/Insurance/header/top_menu');
?>

<!-- Main Content -->
<div class="adminx-content">
    <div class="adminx-main-content">
        <div class="container-fluid">
            <form id="search<?= $form ?>" name="search<?= $form ?>" method="post" data-toggle="validator"
                onsubmit="return false">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow mb-grid">
                            <div class="form-row card-body text-center">
                                <div class="col-md-12 mb-12">
                                    <p class="navbar-brand mb-0 h1 d-none d-md-block weight-400">
                                        <!--<img src="./resources/img/logo.png" class="navbar-brand-image d-inline-block align-top mr-2" alt="">-->
                                        สวัสดี คุณ <?= $_SESSION['vgroup']['info']['Name'] ?>
                                        <?= $_SESSION['vgroup']['info']['Surname'] ?>
                                    </p>
                                </div>
                                <div class="col-md-12 mb-12">
                                    <p class="navbar-brand mb-0 h1 d-none d-md-block weight-400">
                                        <!--<img src="./resources/img/logo.png" class="navbar-brand-image d-inline-block align-top mr-2" alt="">-->
                                        ยินดีต้อนรับเข้าสู่ VInsurance ครับ
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
function Refresh() {
    location.reload();
}
</script>
<?php
$this->load->view('Sales/Insurance/header/buttom_menu');
?>