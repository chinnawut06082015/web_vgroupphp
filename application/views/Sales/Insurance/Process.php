<?php
$this->load->view('Sales/Insurance/header/top_menu');
?>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script> -->
<!-- Main Content -->
<style type='text/css'>
.modal-dialog-full-width {
    width: 100% !important;
    height: 100% !important;
    margin: 0 !important;
    padding: 0 !important;
    max-width: none !important;

}

.modal-content-full-width {
    height: auto !important;
    min-height: 100% !important;
    border-radius: 0 !important;
    background-color: #ececec !important
}

.modal-header-full-width {
    border-bottom: 1px solid #9ea2a2 !important;
}

.modal-footer-full-width {
    border-top: 1px solid #9ea2a2 !important;
}
</style>
<div class="adminx-content">
    <div class="container-fluid">

        <!-- Search -->
        <br>
            <!-- <button id="modalActivate" type="button" class="btn btn-danger" data-toggle="modal"
                data-target="#exampleModalPreview">
                Launch demo modal
            </button> -->
            <!-- Modal -->
            <div class="modal fade right" id="exampleModalPreview" tabindex="-1" role="dialog"
                aria-labelledby="exampleModalPreviewLabel" aria-hidden="true">
                <div class="modal-dialog-full-width modal-dialog momodel modal-fluid" role="document">
                    <div class="modal-content-full-width modal-content ">
                        <div class=" modal-header-full-width   modal-header text-center">
                            <h5 class="modal-title w-100" id="exampleModalPreviewLabel">Material Design Full Screen
                                Modal</h5>
                            <button type="button" class="close " data-dismiss="modal" aria-label="Close">
                                <span style="font-size: 1.3em;" aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <h1 class="section-heading text-center wow fadeIn my-5 pt-3"> Not for money, but for
                                humanity</h1>
                        </div>
                        <div class="modal-footer-full-width  modal-footer">
                            <button type="button" class="btn btn-danger btn-md btn-rounded"
                                data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary btn-md btn-rounded">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-lg-12 text-right">
                <p>
                    <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#collapseExample"
                        aria-expanded="false" aria-controls="collapseExample">
                        <i data-feather="zoom-in"></i> Search
                    </button>
                </p>
            </div>
        </div>
        <div class="collapse" id="collapseExample">
            <form id="search<?= $form ?>" name="search<?= $form ?>" method="post" data-toggle="validator"
                onsubmit="return false">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow mb-grid">
                            <div class="form-row card-body">
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="BR_CD">สาขา :</label>
                                    <select class="form-control" id="BR_CD" name="BR_CD">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="0112">สาขาแจ้งวัฒนะ</option>
                                        <option value="0113">สาขาหนองแขม</option>
                                        <option value="0115">สาขาบางกอกน้อย</option>
                                        <option value="0116">สาขาปากเกร็ด - เมืองทอง</option>
                                        <option value="333">สำนักงานใหญ่</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="MTO_NM">รุ่นรถยนต์ :</label>
                                    <select class="form-control" id="MTO_NM" name="MTO_NM">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="AMAZE">AMAZE</option>
                                        <option value="BRIO">BRIO</option>
                                        <option value="CITY">CITY</option>
                                        <option value="JAZZ">JAZZ</option>
                                        <option value="FREED">FREED</option>
                                        <option value="HR-V">HR-V</option>
                                        <option value="BR-V">BR-V</option>
                                        <option value="CR-V">CR-V</option>
                                        <option value="ACCORD">ACCORD</option>
                                        <option value="RX">RX</option>
                                        <option value="STREAM">STREAM</option>
                                        <option value="STEPWGN">STEPWGN</option>
                                        <option value="ODYSSEY">ODYSSEY</option>
                                        <option value="MOBILIO">MOBILIO</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="LECENCE">ทะเบียนรถยนต์ :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons"
                                        id="LECENCE">
                                        <input class="form-control" type="text" name="LECENCE" id="LECENCE"
                                            autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="FRAME_NO">หมายเลขตัวถัง :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons"
                                        id="FRAME_NO">
                                        <input class="form-control" type="text" name="FRAME_NO" id="FRAME_NO"
                                            autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="START_DATE2">เดือน/ปี กรมธรรม์ :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons"
                                        id="START_DATE2">
                                        <input class="form-control" type="text" name="START_DATE2" id="datepicker2"
                                            autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="INS_NM">บริษัทประกันภัย :</label>
                                    <select class="form-control" id="INS_NM" name="INS_NM">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="AMAZE">AMAZE</option>
                                        <option value="BRIO">BRIO</option>
                                        <option value="CITY">CITY</option>
                                        <option value="JAZZ">JAZZ</option>
                                        <option value="FREED">FREED</option>
                                        <option value="HR-V">HR-V</option>
                                        <option value="BR-V">BR-V</option>
                                        <option value="CR-V">CR-V</option>
                                        <option value="ACCORD">ACCORD</option>
                                        <option value="RX">RX</option>
                                        <option value="STREAM">STREAM</option>
                                        <option value="STEPWGN">STEPWGN</option>
                                        <option value="ODYSSEY">ODYSSEY</option>
                                        <option value="MOBILIO">MOBILIO</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="INS_NO">เลขที่กรมธรรม์ :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons"
                                        id="INS_NO">
                                        <input class="form-control" type="text" name="INS_NO" id="INS_NO"
                                            autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <label class="form-label" for="CUSTOMER_NM">ชื่อลูกค้า :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons"
                                        id="CUSTOMER_NM">
                                        <input class="form-control" type="text" name="CUSTOMER_NM" id="CUSTOMER_NM"
                                            autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <button type='submit' name="searchData" id="searchData"
                                        onclick="searchBTN_<?= $form ?>()" class="btn btn-primary">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Database</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="example" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th scope="col">Actions</th>
                                <th scope="col">สาขา</th>
                                <th scope="col">วันเริ่มคุ้มครอง</th>
                                <th scope="col">เลขที่กรมธรรม์</th>
                                <th scope="col">บริษัทประกัน</th>
                                <th scope="col">ทุนประกัน</th>
                                <th scope="col">เบี้ยประกัน</th>
                                <th scope="col">ที่ปรึกษาการขาย</th>
                                <th scope="col">รุ่นรถยนต์</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                                if (!empty($user)) {
                                    foreach ($user as $value) {
                                        ?>
                            <tr>
                                <td>
                                    <button id="<?= $value['User_NM']; ?>" onclick="edit_<?= $form ?>(this)"
                                        type="button" class="btn btn-sm btn-primary">แก้ไข</button>
                                    <!-- <button id="<?= $value['User_NM']; ?>"  onclick="delete_<?= $form ?>(this)" type="button" class="btn btn-sm btn-danger">ลบ</button> -->
                                </td>
                                <td><?= $value['BR_CD'] ?></td>
                                <td><?= view_date($value['START_DATE2']) ?></td>
                                <td><?= $value['INS_NO'] ?></td>
                                <td><?= $value['INS_NM'] ?></td>
                                <td><?= $value['insCapital'] ?></td>
                                <td><?= $value['insPremium'] ?></td>
                                <td><?= $value['SALE_NM'] ?></td>
                                <td><?= $value['MTO_NM'] ?> <?= $value['COLOR_NM'] ?></td>
                            </tr>
                            <?php
                                    }
                                } else {
                                    ?>
                            <tr>
                                <td colspan="9" style="text-align: center;">ไม่พบข้อมูล</td>
                            </tr>
                            <?php }
                                ?>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- // Main Content -->

<form id="add<?= $form ?>" name="add<?= $form ?>" method="post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="add_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">เพิ่ม</h4>
                </div>
                <div class="modal-body mx-5">
                    <div class="md-form mb-5">
                        <label>
                            <font color="#FF0000">*</font>IDNumem :
                        </label>
                        <input type="text" class="form-control" id="IDNumem" name="IDNumem" placeholder="IDNumem"
                            value="" required maxlength="20" autocomplete="Off">
                    </div>
                    <div class="md-form mb-5">
                        <label>
                            <font color="#FF0000">*</font>IDNumpro :
                        </label>
                        <input type="text" class="form-control" id="IDNumpro" name="IDNumpro" placeholder="IDNumpro"
                            value="" required maxlength="20" autocomplete="Off">
                    </div>
                    <div class="md-form mb-5">
                        <label>
                            <font color="#FF0000">*</font>Username :
                        </label>
                        <input type="text" class="form-control" id="User_NM" name="User_NM" placeholder="Username"
                            value="" required maxlength="20" autocomplete="Off">
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <input type="hidden" name="action" value="add">
                        <button type='submit' onclick="AddItem_<?= $form ?>()" name="add" id="add" tabindex="4"
                            class="btn btn-primary">เพิ่ม <i class="fa fa-plus ml-1"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="edit<?= $form ?>" name="edit<?= $form ?>" method="post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="edit_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">แก้ไข</h4>
                </div>
                <div class="modal-body mx-6">
                    <div id="load_edit_<?= $form ?>" name="load_edit_<?= $form ?>" style="display: none;">
                        <center>
                            <div class="loader"></div>
                            <div>Loading..</div>
                        </center>
                    </div>
                    <div id="form_edit_<?= $form ?>" name="form_edit_<?= $form ?>">
                        <div class="md-form mb-5">
                            <label>
                                <font color="#FF0000">*</font>IDNumem :
                            </label>
                            <input type="text" class="form-control" id="IDNumem" name="IDNumem" placeholder="IDNumem"
                                value="" required maxlength="20" autocomplete="Off">
                        </div>
                        <div class="md-form mb-5">
                            <label>
                                <font color="#FF0000">*</font>IDNumpro :
                            </label>
                            <input type="text" class="form-control" id="IDNumpro" name="IDNumpro" placeholder="IDNumpro"
                                value="" required maxlength="20" autocomplete="Off">
                        </div>
                        <div class="md-form mb-5">
                            <label>
                                <font color="#FF0000">*</font>Username :
                            </label>
                            <input type="text" class="form-control" id="User_NM" name="User_NM"
                                placeholder="ชื่อหัวข้อปัญหา" value="" required maxlength="20">
                        </div>
                        <br>
                        <div class="modal-footer d-flex justify-content-center">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" id="IDNumem" name="IDNumem">
                            <button type='submit' onclick="EditItem_<?= $form ?>()" name="edit" id="edit" tabindex="4"
                                class="btn btn-primary">ยืมยันแก้ไข <span
                                    class="glyphicon glyphicon-wrench"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="delete<?= $form ?>" name="delete<?= $form ?>" method="post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="delete_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">ลบข้อมูล</h4>
                </div>
                <div class="modal-body mx-6">
                    <div id="load_delete_<?= $form ?>" name="load_delete_<?= $form ?>" style="display: none;">
                        <center>
                            <div class="loader"></div>
                            <div>Loading..</div>
                        </center>
                    </div>
                    <div id="form_edit_<?= $form ?>" name="form_edit_<?= $form ?>">
                        <div class="md-form mb-6">
                            <label>Username :</label>
                            <input type="text" class="form-control" id="User_NM" name="User_NM" value="" required=""
                                maxlength="50" disabled="">
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" id="IDNumem" name="IDNumem">
                            <button type='submit' onclick="DelItem_<?= $form ?>()" name="delete" id="delete"
                                tabindex="4" class="btn btn-danger ">ยืมยันการลบข้อมูล <i
                                    class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
});

$(function() {
    $("#datepicker").datepicker({
        changeMonth: true,
        changeYear: true
    });

    $("#datepicker2").datepicker({
        changeMonth: true,
        changeYear: true
    });
});
</script>
<script type="text/javascript">
function Refresh() {
    location.reload();
}
////////////////// Start Add Item ////////////////// 
function add_<?= $form ?>() {
    $('#add_<?= $form ?>').modal('show');
}

function AddItem_<?= $form ?>() {
    var myData = $('#add<?= $form ?>').serialize();
    $.ajax({
        type: "POST",
        url: "./User/set_data/",
        data: myData,
        success: function(response) {

            var dt = JSON.parse(response);
            if (dt.status === true) {
                swal(
                    dt.message,
                    '',
                    'success'
                ).then(() => {
                    location.reload();
                });
            } else {
                $("#load_add_<?= $form ?>").hide();
                $("#form_add_<?= $form ?>").show();
                swal(dt.message, '', 'error')
                    .then(() => {});
            }
        }
    });
}

////////////////// End Add Item ////////////////// 

////////////////// Start Edit Item ///////////////
function edit_<?= $form ?>(event) {
    $.ajax({
        url: "./User/get_data_edit/" + event.id,
        type: "POST",
        success: function(data) {
            var dt = JSON.parse(data);
            $('#edit_<?= $form ?> #IDNumem').val(dt.IDNumem);
            $('#edit_<?= $form ?> #IDNumpro').val(dt.IDNumpro);
            $('#edit_<?= $form ?> #User_NM').val(dt.User_NM);
            $('#edit_<?= $form ?>').modal('show');
        }
    });
}

function EditItem_<?= $form ?>() {
    var myData = $('#edit<?= $form ?>').serialize();
    document.getElementById("edit").disabled = true;
    $.ajax({
        type: "POST",
        url: "./User/set_data/",
        data: myData,
        success: function(response) {
            var dt = JSON.parse(response);
            if (dt.status === true) {
                swal(dt.message, '', 'success')
                    .then(() => {
                        location.reload();
                    });
            } else {
                $("#load_add_<?= $form ?>").hide();
                $("#form_add_<?= $form ?>").show();
                swal(dt.message, '', 'error')
                    .then(() => {
                        location.reload();
                    });
            }
        }
    });
}
////////////////// End Edit Item ///////////////


////////////////// Start delete Item ///////////////
function delete_<?= $form ?>(event) {
    $.ajax({
        url: "./User/get_data_edit/" + event.id,
        type: "POST",
        success: function(data) {
            var dt = JSON.parse(data);
            $('#delete_<?= $form ?> #IDNumem').val(dt.IDNumem);
            $('#delete_<?= $form ?> #User_NM').val(dt.User_NM);
            $('#delete_<?= $form ?>').modal('show');
        }
    });
}

function DelItem_<?= $form ?>() {
    var myData = $('#delete<?= $form ?>').serialize();
    document.getElementById("delete").disabled = true;
    $.ajax({
        type: "POST",
        url: "./User/set_data/",
        data: myData,
        success: function(response) {
            var dt = JSON.parse(response);
            if (dt.status === true) {
                swal(dt.message, '', 'success')
                    .then(() => {
                        location.reload();
                    });
            } else {
                swal('ข้อมูลไม่ถูกต้อง', '', 'error')
                    .then(() => {
                        location.reload();
                    });
            }
        }
    });
}

function searchBTN_<?= $form ?>() {

    $("#Table").hide();
    $("#load").show();

    var myData = $('#search<?= $form ?>').serialize();
    document.getElementById("searchData").disabled = true;
    $.ajax({
        url: "./Process/search_data/",
        // url: "./User/search_data/",
        type: "POST",
        data: myData,
        success: function(response) {
            var dt = JSON.parse(response);
            if (dt.status === true) {
                // location.reload();

            } else {
                // location.reload();
            }
        }
    });
}
</script>
<?php
$this->load->view('Sales/Insurance/header/buttom_menu');
?>