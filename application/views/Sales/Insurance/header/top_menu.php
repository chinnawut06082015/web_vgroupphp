<!DOCTYPE html>
<html lang="en">

<head>
    <title>VInsurance</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="/web_vgroupphp/resources/dist/css/adminx.css" media="screen" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

        <link rel="stylesheet" href="//apps.bdimg.com/libs/jqueryui/1.10.4/css/jquery-ui.min.css">
        <script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css"> -->
</head>

<body>
    <div class="adminx-container">
        <!-- Header -->
        <nav class="navbar navbar-expand justify-content-between fixed-top">
            <p class="navbar-brand mb-0 h1 d-none d-md-block weight-400">
                <img src="/web_vgroupphp/resources/images/logo_vgroup.png"
                    class="navbar-brand-image d-inline-block align-top mr-2" alt="">
                <?= $_SESSION['vgroup']['info']['Name'] ?> <?= $_SESSION['vgroup']['info']['Surname'] ?>
            </p>
            <div class="d-flex flex-1 d-block d-md-none">
                <a href="#" class="sidebar-toggle ml-3">
                    <i data-feather="menu"></i>
                </a>
            </div>
        </nav>
        <!-- // Header -->

        <!-- expand-hover push -->
        <div class="adminx-sidebar expand-hover push" id="sidebar">
            <ul class="sidebar-nav">
                <li class="sidebar-nav-item">
                    <a href="Home" class="sidebar-nav-link <?php echo ($_SESSION['page'] == 'Home' ? "active" : "") ?>">
                        <span class="sidebar-nav-icon">
                            <i data-feather="home"></i>
                        </span>
                        <span class="sidebar-nav-name">
                            Home
                        </span>
                    </a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="./Process"
                        class="sidebar-nav-link <?php echo ($_SESSION['page'] == 'Process' ? "active" : "") ?>">
                        <span class="sidebar-nav-icon">
                            <i data-feather="folder"></i>
                        </span>
                        <span class="sidebar-nav-name">
                            Insurance
                        </span>
                    </a>
                </li>
                <li class="sidebar-nav-item">
                    <a href="./Login" class="sidebar-nav-link">
                        <span class="sidebar-nav-icon">
                            <i data-feather="power"></i>
                        </span>
                        <span class="sidebar-nav-name">
                            Sign Out
                        </span>
                    </a>
                </li>

            </ul>
        </div>