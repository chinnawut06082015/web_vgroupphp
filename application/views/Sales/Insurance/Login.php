<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <title>CORE</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name='author' content='thepnatee phojan' />

    <script src="https://restapi.tu.ac.th/tuapi/resources/assets/js/plugin/webfont/webfont.min.js"></script>
    <link rel="stylesheet" href="https://restapi.tu.ac.th/tuapi/resources/assets/css/azzara.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>

<script type="text/javascript">
window.onload = function(e) {
    document.getElementById('user').value = '';
    document.getElementById('pass').value = '';

};

function send() {
    var myData = $('#login-form').serialize();
    var user = document.getElementById('user').value;
    var pass = document.getElementById('pass').value;

    if (user === '' || pass === '') {
        swal({
            title: "ข้อผิดพลาด",
            text: "กรุณากรอกข้อมูลของท่านให้ครบถ้วน",
            icon: "error",
            buttons: {
                confirm: {
                    text: "Close",
                    value: true,
                    visible: true,
                    className: "btn btn-danger",
                    closeModal: true
                }
            }
        });
    } else {
        document.getElementById("btn_login").disabled = true;
        $("#form_username").hide();
        $("#load").show();

        $.ajax({
            type: "POST",
            url: "./Login/get_data/",
            data: myData,
            success: function(response) {
                var dt = JSON.parse(response);
                if (dt.status === true) {
                    window.location = "./Home";
                } else {
                    swal({
                        title: dt.message_header,
                        text: dt.message_error,
                        icon: "error",
                        buttons: {
                            confirm: {
                                text: "Close",
                                value: true,
                                visible: true,
                                className: "btn btn-danger",
                                closeModal: true
                            }
                        }
                    }).then(function(isConfirm) {
                        if (isConfirm) {
                            $("#form_username").show();
                            $("#load").hide();
                            document.getElementById("btn_login").disabled = false;
                        }
                    });

                }
            }
        });
    }
}
</script>

<body class="login">
    <div class="wrapper wrapper-login">
        <div class="container container-login animated fadeIn">
            <form id="login-form" name="login-form" method="post" onsubmit="return false">
                <div id="load" name="load" style="display: none;">
                    <center>
                        <div class="loader"></div>
                        <div>Loading..</div>
                    </center>
                </div>

                <div id="form_username" name="form_username" class="login-form">
                    <!-- <h3 class="text-center"> <IMG SRC="./resources/images/logo_.png" ALT="Thammasat APIs Developer" WIDTH=80 HEIGHT=80></h3> -->
                    <div class="text-center">
                        <img src="/web_vgroupphp/resources/images/logo_vgroup.png" class="img-rounded" alt=""
                            width="304" height="236">
                    </div>
                    <h3 class="text-center">Insurance System</h3>
                    <div class="form-group form-floating-label">
                        <input id="user" name="user" type="text" autocomplete="off"
                            class="form-control input-border-bottom" required>
                        <label for="user" class="placeholder">Username</label>
                    </div>
                    <div class="form-group form-floating-label">
                        <input id="pass" name="pass" type="password" autocomplete="off" c
                            class="form-control input-border-bottom" required>
                        <label for="pass" class="placeholder">Password</label>
                    </div>

                    <div class="form-action mb-3">
                        <button onclick="send()" name="btn_login" id="btn_login"
                            class="btn btn-primary btn-rounded btn-login">Sign In</button>
                    </div>
                </div>
            </form>
        </div>

    </div>


    <script src="../../../../../web_vgroupphp/resources/assets/js/plugin/sweetalert/sweetalert.min.js"></script>
    <script src="https://restapi.tu.ac.th/tuapi/resources/assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js">
    </script>
    <script src="https://restapi.tu.ac.th/tuapi/resources/assets/js/core/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</body>

</html>