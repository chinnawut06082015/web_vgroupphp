<?php
$this->load->view('header/top_menu');
?>

<!-- Main Content -->
<div class="adminx-content">
    <div class="adminx-main-content">
        <div class="container-fluid">
            <form id="search<?= $form ?>" name="search<?= $form ?>"method = "post" data-toggle="validator" onsubmit="return false">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card shadow mb-grid">
                            <div class="form-row card-body">
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="userName">userName :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons" id="userName">
                                        <input class="form-control" type="text" name="userName" id="userName" autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="userEmail">Email :</label>
                                    <div class="btn-group btn-group-toggle btn-group-sm" data-toggle="buttons" id="userEmail">
                                        <input class="form-control" type="text" name="userEmail" id="userEmail" autocomplete="off" size="125">
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <label class="form-label" for="userSysType">ประเภทผู้ใช้งาน :</label>
                                    <select class="form-control" id="userSysType" name="userSysType">
                                        <option value="">กรุณาเลือก</option>
                                        <option value="A">Administrators</option>
                                        <option value="U">ผู้ใช้งานทั่วไป</option>
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <button type='submit' onclick="add_<?= $form ?>()" class="btn btn-primary">เพิ่ม User</button>
                                    <button type='submit' name="searchData" id="searchData" onclick="searchBTN_<?= $form ?>()" class="btn btn-primary">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            <div class="row">
                <div class="col">
                    <div class="card shadow mb-grid">
                        <div class="card-header d-flex justify-content-between align-items-center">
                            <div class="card-header-title">รายการ User</div>
                        </div>
                        <div class="table-responsive-md" id="load" name="load" style="display: none;">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">Username</th>
                                        <th scope="col">อีเมล</th>
                                        <th scope="col">ประเภทผู้ใช้งาน</th>
                                        <th scope="col">วันที่แก้ไขล่าสุด</th>
                                        <th scope="col">ผู้แก้ไขล่าสุด</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <div>
                                <center>
                                    <img src="https://tu.ac.th/templates/site/resx/loader.svg" alt="Loading..." />
                                    <div >Loading..</div>
                                </center>    
                            </div>
                        </div>
                        <div class="table-responsive-md" id="Table" name="Table">
                            <table class="table table-actions table-striped table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th scope="col">Username</th>
                                        <th scope="col">อีเมล</th>
                                        <th scope="col">ประเภทผู้ใช้งาน</th>
                                        <th scope="col">วันที่แก้ไขล่าสุด</th>
                                        <th scope="col">ผู้แก้ไขล่าสุด</th>
                                        <th scope="col">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($user)) {
                                        foreach ($user as $value) {
                                            if ($value['userSysType'] == 'A') {
                                                $type = 'Administrator';
                                            } else {
                                                $type = 'ผู้ใช้งานทั่วไป';
                                            }
                                            ?>
                                            <tr>
                                                <td><?= $value['userName'] ?></td>
                                                <td><?= $value['userEmail'] ?></td>
                                                <td><?= $type ?></td>
                                                <td><?= view_date($value['LAST_DATE']) ?></td>
                                                <td><?= $value['LAST_USER'] ?></td>
                                                <td>
                                                    <button id="<?= $value['userId']; ?>"  onclick="edit_<?= $form ?>(this)" type="button" class="btn btn-sm btn-primary">แก้ไข</button>
                                                    <button id="<?= $value['userId']; ?>"  onclick="delete_<?= $form ?>(this)" type="button" class="btn btn-sm btn-danger">ลบ</button>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7" style="text-align: center;">ไม่พบข้อมูล</td>
                                        </tr>
                                    <?php }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- // Main Content -->

<form id="add<?= $form ?>" name="add<?= $form ?>"method = "post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="add_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">เพิ่ม</h4>
                </div>
                <div class="modal-body mx-5">
                    <div class="md-form mb-5">
                        <label><font color="#FF0000">*</font>Username :</label>
                        <input type="text" class="form-control" id="userName" name="userName" placeholder="Username" value="" required maxlength="20" autocomplete="Off">
                        <label><font color="#FF0000">*</font>ประเภทผู้ใช้งาน</label>
                        <select class="form-control" id="userSysType" name="userSysType">
                            <option value="A">Admin</option>
                            <option value="U">ผู้ใช้งานทั่วไป</option>
                        </select>
                    </div>
                    <div class="modal-footer d-flex justify-content-center">
                        <input type="hidden" name="action" value="add">
                        <button type='submit' onclick="AddItem_<?= $form ?>()"  name="add" id="add" tabindex="4" class="btn btn-primary">เพิ่ม <i class="fa fa-plus ml-1"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="edit<?= $form ?>" name="edit<?= $form ?>"  method = "post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="edit_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">แก้ไข</h4>
                </div>
                <div class="modal-body mx-6">
                    <div id="load_edit_<?= $form ?>" name="load_edit_<?= $form ?>" style="display: none;">
                        <center>
                            <div class="loader"></div>
                            <div >Loading..</div>
                        </center>    
                    </div>
                    <div id="form_edit_<?= $form ?>" name="form_edit_<?= $form ?>">
                        <div class="md-form mb-5">
                            <label><font color="#FF0000">*</font>Username :</label>
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="ชื่อหัวข้อปัญหา" value="" required maxlength="20" readonly="">
                            <label><font color="#FF0000">*</font>ประเภทผู้ใช้งาน</label>
                            <select class="form-control" id="userSysType" name="userSysType">
                                <option value="A">Admin</option>
                                <option value="U">ผู้ใช้งานทั่วไป</option>
                            </select>
                        </div>
                        <br>
                        <div class="modal-footer d-flex justify-content-center">
                            <input type="hidden" name="action" value="edit">
                            <input type="hidden" id="userId" name="userId">
                            <button type='submit' onclick="EditItem_<?= $form ?>()"  name="edit" id="edit" tabindex="4" class="btn btn-primary">ยืมยันแก้ไข <span class="glyphicon glyphicon-wrench"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="delete<?= $form ?>" name="delete<?= $form ?>"  method = "post" data-toggle="validator" onsubmit="return false">
    <div class="modal fade" id="delete_<?= $form ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title w-100 font-weight-bold">ลบข้อมูล</h4>
                </div>
                <div class="modal-body mx-6">
                    <div id="load_delete_<?= $form ?>" name="load_delete_<?= $form ?>" style="display: none;">
                        <center>
                            <div class="loader"></div>
                            <div >Loading..</div>
                        </center>    
                    </div>
                    <div id="form_edit_<?= $form ?>" name="form_edit_<?= $form ?>">
                        <div class="md-form mb-6">
                            <label>Username :</label>
                            <input type="text" class="form-control" id="userName"name="userName" value=""required="" maxlength="50" disabled="">
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <input type="hidden" name="action" value="delete">
                            <input type="hidden" id="userId" name="userId">
                            <button type='submit' onclick="DelItem_<?= $form ?>()"  name="delete" id="delete" tabindex="4" class="btn btn-danger ">ยืมยันการลบข้อมูล <i class="fa fa-trash"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script type="text/javascript">
    function Refresh() {
        location.reload();
    }
    ////////////////// Start Add Item ////////////////// 
    function add_<?= $form ?>() {
        $('#add_<?= $form ?>').modal('show');
    }
    function AddItem_<?= $form ?>() {
        var myData = $('#add<?= $form ?>').serialize();
        $.ajax({
            type: "POST",
            url: "./User/set_data/",
            data: myData,
            success: function (response) {

                var dt = JSON.parse(response);
                if (dt.status === true) {
                    swal(dt.message, '', 'success')
                            .then(() => {
                                location.reload();
                            });
                } else {
                    $("#load_add_<?= $form ?>").hide();
                    $("#form_add_<?= $form ?>").show();
                    swal(dt.message, '', 'error')
                            .then(() => {
                            });
                }
            }
        });
    }

    ////////////////// End Add Item ////////////////// 

    ////////////////// Start Edit Item ///////////////
    function edit_<?= $form ?>(event) {
        $.ajax({
            url: "./User/get_data_edit/" + event.id,
            type: "POST",
            success: function (data) {
                var dt = JSON.parse(data);
                $('#edit_<?= $form ?> #userId').val(dt.userId);
                $('#edit_<?= $form ?> #userName').val(dt.userName);
                $('#edit_<?= $form ?> #userDetail').val(dt.userDetail);
                $('#edit_<?= $form ?>').modal('show');
            }
        });
    }
    function EditItem_<?= $form ?>() {
        var myData = $('#edit<?= $form ?>').serialize();
        document.getElementById("edit").disabled = true;
        $.ajax({
            type: "POST",
            url: "./User/set_data/",
            data: myData,
            success: function (response) {
                var dt = JSON.parse(response);
                if (dt.status === true) {
                    swal(dt.message, '', 'success')
                            .then(() => {
                                location.reload();
                            });
                } else {
                    $("#load_add_<?= $form ?>").hide();
                    $("#form_add_<?= $form ?>").show();
                    swal(dt.message, '', 'error')
                            .then(() => {
                                location.reload();
                            });
                }
            }
        });
    }
    ////////////////// End Edit Item ///////////////


    ////////////////// Start delete Item ///////////////
    function delete_<?= $form ?>(event) {
        $.ajax({
            url: "./User/get_data_edit/" + event.id,
            type: "POST",
            success: function (data) {
                var dt = JSON.parse(data);
                $('#delete_<?= $form ?> #userId').val(dt.userId);
                $('#delete_<?= $form ?> #userName').val(dt.userName);
                $('#delete_<?= $form ?> #userDetail').val(dt.userDetail);
                $('#delete_<?= $form ?>').modal('show');
            }
        });
    }
    function DelItem_<?= $form ?>() {
        var myData = $('#delete<?= $form ?>').serialize();
        document.getElementById("delete").disabled = true;
        $.ajax({
            type: "POST",
            url: "./User/set_data/",
            data: myData,
            success: function (response) {
                var dt = JSON.parse(response);
                if (dt.status === true) {
                    swal(dt.message, '', 'success')
                            .then(() => {
                                location.reload();
                            });
                } else {
                    swal('ข้อมูลไม่ถูกต้อง', '', 'error')
                            .then(() => {
                                location.reload();
                            });
                }
            }
        });
    }

    function searchBTN_<?= $form ?>() {

        $("#Table").hide();
        $("#load").show();

        var myData = $('#search<?= $form ?>').serialize();
        document.getElementById("searchData").disabled = true;
        $.ajax({
            url: "./User/search_data/",
            type: "POST",
            data: myData,
            success: function (response) {
                var dt = JSON.parse(response);
                if (dt.status === true) {
                    location.reload();

                } else {
                    location.reload();
                }
            }
        });
    }
</script>
<?php
$this->load->view('header/buttom_menu');
?>